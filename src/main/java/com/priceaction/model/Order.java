package com.priceaction.model;

import java.math.BigDecimal;

import com.priceaction.testclnt.TransactionType;

public class Order {
  public TransactionType type;
  public BigDecimal entryPrice;
  public BigDecimal stopLoss;
  public BigDecimal entryPercent;
  public int qty;
  public String symbol;
  
  
  //then if Open for the day is between the entry and stoploss 
  //then place order immediately with the stoploss calculation
  
  public boolean isOpenDayInEntryStopLossRange(final BigDecimal openForDay) {
    return isBetweenNotOrdered(openForDay, entryPrice, stopLoss);
    /*
    if (type == TransactionType.BUY) {
      //openForDay < entryPrice && openForDay > stopLoss
      return openForDay.compareTo(entryPrice) < 0 
          && openForDay.compareTo(stopLoss) > 0;
    }    
    return openForDay.compareTo(entryPrice) < 0 
    && openForDay.compareTo(stopLoss) > 0;
    */
  }
  
  public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

  public static BigDecimal percentage(BigDecimal base, BigDecimal pct){
      return base.multiply(pct).divide(ONE_HUNDRED);
  }
  
  public static <T extends Comparable<T>> boolean isBetween(T value, T start, T end) {
    return value.compareTo(start) >= 0 && value.compareTo(end) <= 0;
  }
  
  public static <T extends Comparable<T>> boolean isBetweenNotOrdered(T value, T b1, T b2) {
    final boolean parametersOrderd = b1.compareTo(b2) >= 0;
    final T start = parametersOrderd?b1:b2;
    final T end = parametersOrderd?b2:b1;
    return value.compareTo(start) >= 0 && value.compareTo(end) <= 0;
  }
  
  //Apart from it, we do have other rule that LTP (last trader price) is close to Entry%, 
  
  public boolean isLastTraderPriceCloseToEntry(final BigDecimal lastTraderPrice) {
    
    final BigDecimal entPctVal = percentage(entryPrice, entryPercent);
    
    final BigDecimal start = entryPrice.subtract(entPctVal);
    
    final BigDecimal end = entryPrice.add(entPctVal);
    
    return isBetween(lastTraderPrice, start, end);
  }
  
}
