package com.priceaction.model.arrow;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginRequestRq {
  
  public String getApiKey() {
    return api_key;
  }

  public void setApiKey(String api_key) {
    this.api_key = api_key;
  }

  @JsonProperty("api_key")
  private String api_key;
}

