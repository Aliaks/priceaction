package com.priceaction.model.arrow;

import java.util.function.Consumer;

public class ArrowRespResponseRs<D, E> {

  public ArrowRespResponseRs(ResStatResp<D> data, ResStatResp<E> error) {
    super();
    this.data = data;
    this.error = error;
  }

  private ResStatResp<D> data;
  
  private ResStatResp<E> error;
  
  public boolean isError() {
    return !data.isSuccess();
  }

  public ResStatResp<D> getData() {
    return data;
  }
  public ResStatResp<E> getError() {
    return error;
  }

  public void onSuccess(Consumer<ResStatResp<D>> successAction) {
    if (!isError()) {
      successAction.accept(getData());
    }
  }

  public void onError(Consumer<ResStatResp<E>> errorAction) {
    if (!isError()) {
      errorAction.accept(getError());
    }
  }

}
