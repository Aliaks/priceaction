package com.priceaction.model.arrow;

import java.util.function.Consumer;

public class ArrowResponseRs<D extends ResStat, E> implements ArrowResponse<D, E> {

  public ArrowResponseRs(D data, E error) {
    super();
    this.data = data;
    this.error = error;
  }

  private D data;
  
  private E error;
  
  @Override
  public boolean isError() {
    return !data.isSuccess();
  }

  @Override
  public D getData() {
    return data;
  }
  @Override
  public E getError() {
    return error;
  }

  public void onSuccess(Consumer<D> successAction) {
    if (!isError()) {
      successAction.accept(getData());
    }
  }

  @Override
  public void onError(Consumer<E> errorAction) {
    if (!isError()) {
      errorAction.accept(getError());
    }
  }

}
