package com.priceaction.model.arrow;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MWLError extends ResStat {
  
// {"status":"E","response":{"error_no":"1000","error_msg":"Invalid Session","error_type":"E"}}
  
  @JsonProperty("error_no")
  public String error_no;
  
  @JsonProperty("error_msg")
  public String error_msg;
  
  @JsonProperty("error_type")
  public String error_type;
  
  /*1.Session Expired
     - Redirect to login page
  2. Not able to Retrieve MWList
  3. No_MarketWatch
  */
}
