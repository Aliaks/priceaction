package com.priceaction.model.arrow;

import java.lang.reflect.ParameterizedType;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResStatResp <D> {
  
  private final Class<ResStatResp<D>> type = (Class<ResStatResp<D>>) ((ParameterizedType) getClass()
      .getGenericSuperclass()).getActualTypeArguments()[0];

  public Class<ResStatResp<D>> getType() {
    return type;
  }
  public static final String OKSTAT = "Ok"; 
  
  @JsonProperty("status")
  public String status;
  
  @JsonProperty("response")
  public D response;
  
  public boolean isSuccess() {
    return OKSTAT.equalsIgnoreCase(status);
  }
}
