package com.priceaction.model.arrow;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResStat {
  public static final String OKSTAT = "Ok"; 
  
  @JsonProperty("status")
  public String status;
  
  public boolean isSuccess() {
    return OKSTAT.equalsIgnoreCase(status);
  }
}
