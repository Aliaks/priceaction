package com.priceaction.model.arrow;

import java.util.function.Consumer;

public interface ArrowResponse<D extends ResStat, E> {
  boolean isError();
  
  D getData();
  
  E getError();
  
  void onSuccess(final Consumer<D> successAction);
  
  void onError(final Consumer<E> errorAction);

}
