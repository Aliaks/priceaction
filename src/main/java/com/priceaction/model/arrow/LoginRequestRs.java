package com.priceaction.model.arrow;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "priceSpecification", "journey", "page" })
public class LoginRequestRs {
  
  private static final String OK_STATUS = "S";
  
  private static final String ERR_STATUS = "E";

  @JsonProperty("status")
  private String status;
  
  @JsonProperty("response")
  //private String response;
  private ResponseOk response;
  
  public boolean isResponseOk() {
    return OK_STATUS.equalsIgnoreCase(status);
  }
  
  public String toString() {
    return "status: " + status + " response: " + response;
  }

  public ResponseOk getResponse() {
    return response;
  }
  
  
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class ResponseOk {
    @JsonProperty("app_name")
    private String appName;
    
    @JsonProperty("long_url")
    private String longUrl;
    
    @JsonProperty("sess_id")
    private String sessId;
    
    public String getLongUrl() {
      return longUrl;
    }

    public String getSessId() {
      return sessId;
    }

    public String toString() {
      return "app_name: " + appName + " long_url: " + longUrl + " sess_id: " + sessId;
    }
  }
  
  public static class ResponseError {
    @JsonProperty("error_no")
    private String errNo;
    
    @JsonProperty("error_msg")
    private String errMsg;
    
    @JsonProperty("error_type")
    private String errType;
    
    public String toString() {
      return "error_no: " + errNo + " error_msg: " + errMsg + " error_type: " + errType;
      
    }
  }
}
