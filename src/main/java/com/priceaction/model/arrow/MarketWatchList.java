package com.priceaction.model.arrow;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

//public class MarketWatchList extends ResStat {
public class MarketWatchList {
  @JsonProperty("values")
  public List<String> values;
  
  @JsonProperty("logindefaulttmw")
  public String logindefaulttmw;
  
}
