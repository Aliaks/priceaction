package com.priceaction.model.arrow;

public class ArrowOrder {
  
  /*
   
1 uid User id Yes
2 act_id  Account id  Yes
3 s_prdt_ali  Product alias Yes
4 Tsym  Trading Symbol  Yes
5 exch  Exchange name Yes
6 Ttranstype  Transaction type  Yes
7 Ret Retention type (from LoadRetentionType rest api)  Yes
8 prctyp  Price type (from DefaultLogin/ NormalLogin rest api)  Yes
9 qty Quantity  Yes
10  discqty Disclosed quantity  Yes ( Send as 0 for default)
11  MktPro  Market Protection Yes (Should be sent as NA by default)
12  Price Price Yes
13  TrigPrice Trigger price Yes( Send as 0 for default)
14  Pcode Product Code  Yes
15  DateDays  Date Days Yes ( Send as NA for default )
16  AMO After market order flag (YES/NO)
   YES -> it is a AMO order
   NO -> it is a normal order Yes
17  PosSquareFlg  Y -> Position square off  No
18  MinQty  Minimum Quantity(available only for CSE)  Yes( Send as 0 for default)
19  BrokerClient  Specifies the Broker-Client relationship
   1- Local-local
   2- Local-foreign
   3- Foreign-foreign No
20  naicCode  Vendor code Send only if inhouse approved
21  orderSource Order source  No
22  userTag It will have below two values    Web    Mob No
23  Exch_Algo_Id  Algo ID No
24  Exch_Algo_Category  Algo Category No
25  remarks User Remarks  No
    
    
   
   */

}
