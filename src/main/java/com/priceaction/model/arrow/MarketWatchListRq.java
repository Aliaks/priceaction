package com.priceaction.model.arrow;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MarketWatchListRq {
  /*
   *      "{\"user_apikey\":\"" + appSettings.getApiKey() + "\"," + 
         "\"session_id\":\"" + appSettings.getSessId() + "\"," +
         "\"access_token\":\"" + appSettings.getRequestToken() + "\"," +
         "\"data\": {\"acctId\":\"" + appSettings.getClientId() + "\"}" +
        "}";
   
   */
  @JsonProperty("user_apikey")
  public String user_apikey;
  @JsonProperty("session_id")
  public String session_id;

  @JsonProperty("access_token")
  public String access_token;
  @JsonProperty("data")
  public Data data;
  
  public static class Data {
    @JsonProperty("acctId")
    public String acctId;
  }
}
