package com.priceaction.model;

import java.math.BigDecimal;
import java.time.LocalTime;

public interface CommonSegmentModel {

  LocalTime getStartTrading();

  LocalTime getCancelPendingOrders();

  Integer getMaxOpenPositions();

  BigDecimal getMaxMtmLoss();

  Boolean getIntraday();

  Integer getNomOfTargets();

  BigDecimal getEntryPercent();

  BigDecimal getCancelPercent();

  Boolean getQuantityFromExel();

}