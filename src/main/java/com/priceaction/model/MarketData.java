package com.priceaction.model;

import java.math.BigDecimal;

public class MarketData {
  public String symbol;
  public BigDecimal openMarketPrice;
  public BigDecimal lastTraderPrice;
}
