package com.priceaction.model;

import java.math.BigDecimal;
import java.time.LocalTime;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CashSettingsModel implements CommonSegmentModel {
  @JsonProperty
  private LocalTime startTrading = LocalTime.now().plusMinutes(1);
  
  @JsonProperty
  private LocalTime cancelPendingOrders = LocalTime.now().plusHours(5);
  
  @JsonProperty
  private Integer maxOpenPositions = 100;
  
  @JsonProperty
  private BigDecimal maxMtmLoss  = BigDecimal.TEN;
  
  @JsonProperty
  private BigDecimal riskPerTrade  = BigDecimal.TEN;
  
  @JsonProperty
  private Boolean Intraday = true;
  
  @JsonProperty
  private Integer nomOfTargets = 4;
  
  @JsonProperty
  private BigDecimal entryPercent = BigDecimal.TEN;
  
  @JsonProperty
  private BigDecimal cancelPercent  = BigDecimal.TEN;
  
  @JsonProperty
  private Boolean quantityFromExel = true;
  
  
  /* (non-Javadoc)
   * @see com.priceaction.model.CommonSegmentModel#getStartTrading()
   */
  @Override
  public LocalTime getStartTrading() {
    return startTrading;
  }
  public void setStartTrading(LocalTime startTrading) {
    this.startTrading = startTrading;
  }
  /* (non-Javadoc)
   * @see com.priceaction.model.CommonSegmentModel#getCancelPendingOrders()
   */
  @Override
  public LocalTime getCancelPendingOrders() {
    return cancelPendingOrders;
  }
  public void setCancelPendingOrders(LocalTime cancelPendingOrders) {
    this.cancelPendingOrders = cancelPendingOrders;
  }
  /* (non-Javadoc)
   * @see com.priceaction.model.CommonSegmentModel#getMaxOpenPositions()
   */
  @Override
  public Integer getMaxOpenPositions() {
    return maxOpenPositions;
  }
  public void setMaxOpenPositions(Integer maxOpenPositions) {
    this.maxOpenPositions = maxOpenPositions;
  }
  /* (non-Javadoc)
   * @see com.priceaction.model.CommonSegmentModel#getMaxMtmLoss()
   */
  @Override
  public BigDecimal getMaxMtmLoss() {
    return maxMtmLoss;
  }
  public void setMaxMtmLoss(BigDecimal maxMtmLoss) {
    this.maxMtmLoss = maxMtmLoss;
  }
  public BigDecimal getRiskPerTrade() {
    return riskPerTrade;
  }
  public void setRiskPerTrade(BigDecimal riskPerTrade) {
    this.riskPerTrade = riskPerTrade;
  }
  /* (non-Javadoc)
   * @see com.priceaction.model.CommonSegmentModel#getIntraday()
   */
  @Override
  public Boolean getIntraday() {
    return Intraday;
  }
  public void setIntraday(Boolean intraday) {
    Intraday = intraday;
  }
  /* (non-Javadoc)
   * @see com.priceaction.model.CommonSegmentModel#getNomOfTargets()
   */
  @Override
  public Integer getNomOfTargets() {
    return nomOfTargets;
  }
  public void setNomOfTargets(Integer nomOfTargets) {
    this.nomOfTargets = nomOfTargets;
  }
  /* (non-Javadoc)
   * @see com.priceaction.model.CommonSegmentModel#getEntryPercent()
   */
  @Override
  public BigDecimal getEntryPercent() {
    return entryPercent;
  }
  public void setEntryPercent(BigDecimal entryPercent) {
    this.entryPercent = entryPercent;
  }
  /* (non-Javadoc)
   * @see com.priceaction.model.CommonSegmentModel#getCancelPercent()
   */
  @Override
  public BigDecimal getCancelPercent() {
    return cancelPercent;
  }
  public void setCancelPercent(BigDecimal cancelPercent) {
    this.cancelPercent = cancelPercent;
  }
  /* (non-Javadoc)
   * @see com.priceaction.model.CommonSegmentModel#getQuantityFromExel()
   */
  @Override
  public Boolean getQuantityFromExel() {
    return quantityFromExel;
  }
  public void setQuantityFromExel(Boolean quantityFromExel) {
    this.quantityFromExel = quantityFromExel;
  }

}
