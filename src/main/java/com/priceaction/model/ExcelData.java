package com.priceaction.model;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.priceaction.testclnt.TransactionType;

public class ExcelData {
  
  public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
  
  public String symbol;
  public TransactionType transactionType;
  public String exchangeSegment;
  public int exchangeInstrumentID;
  
  public BigDecimal entryPrice;
  public BigDecimal stoplossPrice;
  public BigDecimal target1Price;
  public BigDecimal target2Price;
  public BigDecimal target3Price;
  public BigDecimal target4Price;
  public int qty1;
  public int qty2;
  public int qty3;
  public int qty4;
  
  public String entryAppOrderID = null;
  
  public String stoplossAppOrderID = null;
  
  public List<String> exitAppOrderIDs = new ArrayList<String>();
  
  public boolean isEntryOrderPlaced() {
    return isNotEmpty(entryAppOrderID);
  }
  
  public boolean isExitOrdersPlaced() {
    return !exitAppOrderIDs.isEmpty();
  }
  
  public int getTargetQauntity(int target) {
    if (target == 0) {
      return qty1;
    }
    if (target == 1) {
      return qty2;
    }
    if (target == 2) {
      return qty3;
    }
    if (target == 3) {
      return qty4;
    }
    throw new RuntimeException("Incorrect Target Number.");
  }
  
  public BigDecimal getTargetPrice(int target) {
    if (target == 0) {
      return target1Price;
    }
    if (target == 1) {
      return target2Price;
    }
    if (target == 2) {
      return target3Price;
    }
    if (target == 3) {
      return target4Price;
    }
    throw new RuntimeException("Incorrect Target Number.");
  }
  
  public Double getTargetDoublePrice(int target) {
    final BigDecimal targetPrice = getTargetPrice(target);
    return targetPrice.doubleValue();
  }
  
  /**
so we will have up to 4 orders per 1 Excel data entry

We will have 4 different orders. 

Yes only exit orders, not entry orders 
Entry orders will always be one.
   */
  
  
  /*
   
so, for each Excel buy entry(1000 qtty): we create:
1 buy entry order on 1000
1 sell stop loss order 1000
4 (target number) sell exit orders
total 1000
then publish entry order and stoploss order
waiting entry order filled and once it is filled we publish exit orders (simplified case, there may be partial filling)
is this correct?
this is optimistic scenario, no stop loss hit

We publish only buy order first, only on realization of the order is filled completly, 
we place stoploss and target orders 
Stoploss and target orders both are exit orders, so both to be placed only 
if buy order is filled or partially filled, 
partially filled is another concept 
we need to exactly place stoploss order based on filled quantity only 
   */
  
  public Order entryOrder;
  public List<Order> exitOrders;
  
  public Order stoplossOrder;
  
  
  
  public String getKey() {
    return String.join("|", exchangeSegment, symbol);
  }
  
  
  //then if Open for the day is between the entry and stoploss 
  //then place order immediately with the stoploss calculation
  
  public boolean isOpenDayInEntryStopLossRange(final BigDecimal openForDay) {
    return (openForDay.doubleValue() > 0.0) && isBetweenNotOrdered(openForDay, entryPrice, stoplossPrice);
  }
  
  public static BigDecimal percentage(final BigDecimal base, final BigDecimal pct){
      return base.multiply(pct).divide(ONE_HUNDRED);
  }
  
  public static <T extends Comparable<T>> boolean isBetween(T value, T start, T end) {
    return value.compareTo(start) >= 0 && value.compareTo(end) <= 0;
  }
  
  public static <T extends Comparable<T>> boolean isBetweenNotOrdered(T value, T b1, T b2) {
    final boolean parametersOrderd = b1.compareTo(b2) >= 0;
    final T start = parametersOrderd?b1:b2;
    final T end = parametersOrderd?b2:b1;
    return value.compareTo(start) >= 0 && value.compareTo(end) <= 0;
  }
  
  //Apart from it, we do have other rule that LTP (last trader price) is close to Entry%, 
  
  public boolean isLastTraderPriceCloseToEntry(final BigDecimal lastTraderPrice, 
      final BigDecimal entryPercent) {
    
    final BigDecimal entPctVal = percentage(entryPrice, entryPercent);
    
    final BigDecimal start = entryPrice.subtract(entPctVal);
    
    final BigDecimal end = entryPrice.add(entPctVal);
    
    return isBetween(lastTraderPrice, start, end);
  }

  
  
  
  
}
