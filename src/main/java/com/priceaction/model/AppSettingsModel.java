package com.priceaction.model;

import java.math.BigDecimal;
import java.time.LocalTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AppSettingsModel implements CommonSegmentModel {

  public enum Segment {
    cash, futures
  }

  
  @JsonProperty
  public CashSettingsModel cashSettingsModel;
  
  @JsonProperty
  public FuturesSettingsModel futuresSettingsModel; 
  
  @JsonProperty
  public String importedFile = "";
  
  @JsonProperty
  public Segment currentSegment = Segment.cash;
  

  @JsonIgnore
  public boolean isSegmentCash() {
    return currentSegment == Segment.cash;
  }
  
  @JsonIgnore
  @Override
  public LocalTime getStartTrading() {
    return isSegmentCash()
        ?cashSettingsModel.getStartTrading()
            :futuresSettingsModel.getStartTrading();
  }

  @JsonIgnore
  @Override
  public LocalTime getCancelPendingOrders() {
    return isSegmentCash()
        ?cashSettingsModel.getCancelPendingOrders()
            :futuresSettingsModel.getCancelPendingOrders();
  }

  @JsonIgnore
  @Override
  public BigDecimal getEntryPercent() {
    return isSegmentCash()
        ?cashSettingsModel.getEntryPercent()
            :futuresSettingsModel.getEntryPercent();
  }
  
  @JsonIgnore
  @Override
  public BigDecimal getCancelPercent() {
    return isSegmentCash()
        ?cashSettingsModel.getCancelPercent()
            :futuresSettingsModel.getCancelPercent();
  }

  @JsonIgnore
  @Override
  public Integer getMaxOpenPositions() {
    return isSegmentCash()
    ?cashSettingsModel.getMaxOpenPositions()
        :futuresSettingsModel.getMaxOpenPositions(); 
    }

  @JsonIgnore
  @Override
  public BigDecimal getMaxMtmLoss() {
    return isSegmentCash()
        ?cashSettingsModel.getCancelPercent()
            :futuresSettingsModel.getCancelPercent();
  }

  @JsonIgnore
  @Override
  public Boolean getIntraday() {
    return isSegmentCash()
        ?cashSettingsModel.getIntraday()
            :futuresSettingsModel.getIntraday();
  }

  @JsonIgnore
  @Override
  public Integer getNomOfTargets() {
    return isSegmentCash()
        ?cashSettingsModel.getNomOfTargets()
            :futuresSettingsModel.getNomOfTargets();
  }

  @JsonIgnore
  @Override
  public Boolean getQuantityFromExel() {
    return isSegmentCash()
        ?cashSettingsModel.getQuantityFromExel()
            :futuresSettingsModel.getQuantityFromExel();
  }

}
