package com.priceaction.model.xts;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "appOrderID", "productType", "orderType", "modifiedOrderQuantity", "modifiedDisclosedQuantity",
    "modifiedLimitPrice", "modifiedStopPrice", "timeInForce", "orderUniqueIdentifier" })
public class ModifyOrderRq {

  @JsonProperty("appOrderID")
  public String appOrderID;
  @JsonProperty("productType")
  public String productType;
  @JsonProperty("orderType")
  public String orderType;
  @JsonProperty("modifiedOrderQuantity")
  public Integer modifiedOrderQuantity;
  @JsonProperty("modifiedDisclosedQuantity")
  public Integer modifiedDisclosedQuantity;
  @JsonProperty("modifiedLimitPrice")
  public Double modifiedLimitPrice;
  @JsonProperty("modifiedStopPrice")
  public Double modifiedStopPrice;
  @JsonProperty("timeInForce")
  public String timeInForce = "DAY";
  @JsonProperty("orderUniqueIdentifier")
  public String orderUniqueIdentifier = "jhgjhg";

}