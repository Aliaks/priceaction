
package com.priceaction.model.xts;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "MessageCode", "MessageVersion", "ApplicationType", "TokenID", "ExchangeSegment",
    "ExchangeInstrumentID", "ExchangeTimeStamp", "Bids", "Asks", "Touchline", "BookType", "XMarketType" })
public class Quote {

  @JsonProperty("MessageCode")
  public Integer messageCode;
  @JsonProperty("MessageVersion")
  public Integer messageVersion;
  @JsonProperty("ApplicationType")
  public Integer applicationType;
  @JsonProperty("TokenID")
  public Long tokenID;
  @JsonProperty("ExchangeSegment")
  public Integer exchangeSegment;
  @JsonProperty("ExchangeInstrumentID")
  public Integer exchangeInstrumentID;
  @JsonProperty("ExchangeTimeStamp")
  public Long exchangeTimeStamp;
  @JsonProperty("Bids")
  public List<Bid> bids = null;
  @JsonProperty("Asks")
  public List<Ask> asks = null;
  @JsonProperty("Touchline")
  public Touchline touchline;
  @JsonProperty("BookType")
  public Integer bookType;
  @JsonProperty("XMarketType")
  public Integer xMarketType;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "Size", "Price", "TotalOrders", "BuyBackMarketMaker" })
  public static class Ask {

    @JsonProperty("Size")
    public Long size;
    @JsonProperty("Price")
    public Double price;
    @JsonProperty("TotalOrders")
    public Long totalOrders;
    @JsonProperty("BuyBackMarketMaker")
    public Long buyBackMarketMaker;

  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "Size", "Price", "TotalOrders", "BuyBackMarketMaker" })
  public static class AskInfo {

    @JsonProperty("Size")
    public Long size;
    @JsonProperty("Price")
    public Double price;
    @JsonProperty("TotalOrders")
    public Long totalOrders;
    @JsonProperty("BuyBackMarketMaker")
    public Long buyBackMarketMaker;

  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "Size", "Price", "TotalOrders", "BuyBackMarketMaker" })
  public static class Bid {

    @JsonProperty("Size")
    public Long size;
    @JsonProperty("Price")
    public Long price;
    @JsonProperty("TotalOrders")
    public Long totalOrders;
    @JsonProperty("BuyBackMarketMaker")
    public Long buyBackMarketMaker;

  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "Size", "Price", "TotalOrders", "BuyBackMarketMaker" })
  public static class BidInfo {

    @JsonProperty("Size")
    public Long size;
    @JsonProperty("Price")
    public Double price;
    @JsonProperty("TotalOrders")
    public Long totalOrders;
    @JsonProperty("BuyBackMarketMaker")
    public Long buyBackMarketMaker;

  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "BidInfo", "AskInfo", "LastTradedPrice", "LastTradedQunatity", "TotalBuyQuantity",
      "TotalSellQuantity", "TotalTradedQuantity", "AverageTradedPrice", "LastTradedTime", "LastUpdateTime",
      "PercentChange", "Open", "High", "Low", "Close", "TotalValueTraded", "BuyBackTotalBuy", "BuyBackTotalSell" })
  public static class Touchline {

    @JsonProperty("BidInfo")
    public BidInfo bidInfo;
    @JsonProperty("AskInfo")
    public AskInfo askInfo;
    @JsonProperty("LastTradedPrice")
    public Double lastTradedPrice;
    @JsonProperty("LastTradedQunatity")
    public Long lastTradedQunatity;
    @JsonProperty("TotalBuyQuantity")
    public Long totalBuyQuantity;
    @JsonProperty("TotalSellQuantity")
    public Long totalSellQuantity;
    @JsonProperty("TotalTradedQuantity")
    public Long totalTradedQuantity;
    @JsonProperty("AverageTradedPrice")
    public Double averageTradedPrice;
    @JsonProperty("LastTradedTime")
    public Long lastTradedTime;
    @JsonProperty("LastUpdateTime")
    public Long lastUpdateTime;
    @JsonProperty("PercentChange")
    public Double percentChange;
    @JsonProperty("Open")
    public Double open;
    @JsonProperty("High")
    public Double high;
    @JsonProperty("Low")
    public Double low;
    @JsonProperty("Close")
    public Double close;
    @JsonProperty("TotalValueTraded")
    public Long totalValueTraded;
    @JsonProperty("BuyBackTotalBuy")
    public Long buyBackTotalBuy;
    @JsonProperty("BuyBackTotalSell")
    public Long buyBackTotalSell;

  }

}
