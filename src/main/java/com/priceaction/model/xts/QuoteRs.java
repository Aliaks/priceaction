package com.priceaction.model.xts;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "code", "description", "result" })
public class QuoteRs {

  @JsonProperty("type")
  public String type;
  @JsonProperty("code")
  public String code;
  @JsonProperty("description")
  public String description;
  @JsonProperty("result")
  public Result result;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "Size", "Price", "TotalOrders", "BuyBackMarketMaker" })
  public static class AskInfo {

    @JsonProperty("Size")
    public Integer size;
    @JsonProperty("Price")
    public Double price;
    @JsonProperty("TotalOrders")
    public Integer totalOrders;
    @JsonProperty("BuyBackMarketMaker")
    public Integer buyBackMarketMaker;

  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "Size", "Price", "TotalOrders", "BuyBackMarketMaker" })
  public static class Asks {

    @JsonProperty("Size")
    public Integer size;
    @JsonProperty("Price")
    public Double price;
    @JsonProperty("TotalOrders")
    public Integer totalOrders;
    @JsonProperty("BuyBackMarketMaker")
    public Integer buyBackMarketMaker;

  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "Size", "Price", "TotalOrders", "BuyBackMarketMaker" })
  public static class BidInfo {

    @JsonProperty("Size")
    public Integer size;
    @JsonProperty("Price")
    public Integer price;
    @JsonProperty("TotalOrders")
    public Integer totalOrders;
    @JsonProperty("BuyBackMarketMaker")
    public Integer buyBackMarketMaker;

  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "Size", "Price", "TotalOrders", "BuyBackMarketMaker" })
  public static class Bids {

    @JsonProperty("Size")
    public Integer size;
    @JsonProperty("Price")
    public Integer price;
    @JsonProperty("TotalOrders")
    public Integer totalOrders;
    @JsonProperty("BuyBackMarketMaker")
    public Integer buyBackMarketMaker;

  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "MessageCode", "MessageVersion", "ApplicationType", "TokenID", "ExchangeSegment",
      "ExchangeInstrumentID", "ExchangeTimeStamp", "Bids", "Asks", "Touchline", "BookType", "XMarketType" })
  public static class ListQuotes {

    @JsonProperty("MessageCode")
    public Integer messageCode;
    @JsonProperty("MessageVersion")
    public Integer messageVersion;
    @JsonProperty("ApplicationType")
    public Integer applicationType;
    @JsonProperty("TokenID")
    public Integer tokenID;
    @JsonProperty("ExchangeSegment")
    public Integer exchangeSegment;
    @JsonProperty("ExchangeInstrumentID")
    public Integer exchangeInstrumentID;
    @JsonProperty("ExchangeTimeStamp")
    public Integer exchangeTimeStamp;
    @JsonProperty("Bids")
    public Bids bids;
    @JsonProperty("Asks")
    public Asks asks;
    @JsonProperty("Touchline")
    public Touchline touchline;
    @JsonProperty("BookType")
    public Integer bookType;
    @JsonProperty("XMarketType")
    public Integer xMarketType;

  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "exchangeSegment", "exchangeInstrumentID" })
  public static class Quote {

    @JsonProperty("exchangeSegment")
    public Integer exchangeSegment;
    @JsonProperty("exchangeInstrumentID")
    public Integer exchangeInstrumentID;

  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "mdp", "quoteList", "listQuotes" })
  public static class Result {

    @JsonProperty("mdp")
    public Integer mdp;
    @JsonProperty("quoteList")
    public List<Quote> quoteList;
    
    @JsonProperty("listQuotes")
    public List<String> listQuotes = null;  
    
    /*
    @JsonProperty("listQuotes")
   // public ListQuotes listQuotes;
    public String listQuotes;
*/
  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "BidInfo", "AskInfo", "LastTradedPrice", "LastTradedQunatity", "TotalBuyQuantity",
      "TotalSellQuantity", "TotalTradedQuantity", "AverageTradedPrice", "LastTradedTime", "LastUpdateTime",
      "PercentChange", "Open", "High", "Low", "Close", "TotalValueTraded", "BuyBackTotalBuy", "BuyBackTotalSell" })
  public static class Touchline {

    @JsonProperty("BidInfo")
    public BidInfo bidInfo;
    @JsonProperty("AskInfo")
    public AskInfo askInfo;
    @JsonProperty("LastTradedPrice")
    public Double lastTradedPrice;
    @JsonProperty("LastTradedQunatity")
    public Integer lastTradedQunatity;
    @JsonProperty("TotalBuyQuantity")
    public Integer totalBuyQuantity;
    @JsonProperty("TotalSellQuantity")
    public Integer totalSellQuantity;
    @JsonProperty("TotalTradedQuantity")
    public Integer totalTradedQuantity;
    @JsonProperty("AverageTradedPrice")
    public Double averageTradedPrice;
    @JsonProperty("LastTradedTime")
    public Integer lastTradedTime;
    @JsonProperty("LastUpdateTime")
    public Integer lastUpdateTime;
    @JsonProperty("PercentChange")
    public Integer percentChange;
    @JsonProperty("Open")
    public Double open;
    @JsonProperty("High")
    public Double high;
    @JsonProperty("Low")
    public Double low;
    @JsonProperty("Close")
    public Double close;
    @JsonProperty("TotalValueTraded")
    public Integer totalValueTraded;
    @JsonProperty("BuyBackTotalBuy")
    public Integer buyBackTotalBuy;
    @JsonProperty("BuyBackTotalSell")
    public Integer buyBackTotalSell;

  }

}