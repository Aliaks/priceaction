
package com.priceaction.model.xts;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "code", "description", "result" })
public class ModifyOrderRs {

  @JsonProperty("type")
  public String type;
  @JsonProperty("code")
  public String code;
  @JsonProperty("description")
  public String description;
  @JsonProperty("result")
  public Result result;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "AppOrderID" })
  public static class Result {

    @JsonProperty("AppOrderID")
    public String appOrderID;

  }

}
