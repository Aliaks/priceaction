package com.priceaction.model.xts;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "exchangeSegmentList" })
public class SymbolsMarketDataRq {

  @JsonProperty("exchangeSegmentList")
  public List<String> exchangeSegmentList = null;

}