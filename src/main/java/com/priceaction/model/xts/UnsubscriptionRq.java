
package com.priceaction.model.xts;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "userID", "clientID", "source", "instruments", "marketDataPort"})
  public class UnsubscriptionRq {

    @JsonProperty("userID")
    public String userID;
    @JsonProperty("clientID")
    public String clientID;
    @JsonProperty("source")
    public String source;
    @JsonProperty("instruments")
    public List<Instrument> instruments;
    
    @JsonProperty("marketDataPort")
    public Integer  marketDataPort; 
 //   @JsonProperty("publishFormat")
 //   public String publishFormat;
}