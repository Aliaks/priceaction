package com.priceaction.model.xts;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
{
"userID": "SYMP",
"password": "Sft@1111",
"publicKey": "3064470037e8c577",
"source": "WebApi"
}
 
 */
public class LoginRq {
  
  @JsonProperty("userID")
  public String userID;
  
  @JsonProperty("password")
  public String password;
  
  @JsonProperty("publicKey")
  public String publicKey;
    
  @JsonProperty("source")
  public String source = "WebApi";
  
  
  
  //  "userID": "SYMP",
 //   "password": "Sft@1111",
 //   "publicKey": "3064470037e8c577",
 //   "source": "WebApi"
 //   }

}
