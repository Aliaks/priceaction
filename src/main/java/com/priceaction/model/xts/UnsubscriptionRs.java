
package com.priceaction.model.xts;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "code", "description", "result" })
public class UnsubscriptionRs {

  @JsonProperty("type")
  public String type;
  @JsonProperty("code")
  public String code;
  @JsonProperty("description")
  public String description;
  @JsonProperty("result")
  public Result result;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "exchangeSegment", "exchangeInstrumentID" })
  public static class UnsubList {

    @JsonProperty("exchangeSegment")
    public Integer exchangeSegment;
    @JsonProperty("exchangeInstrumentID")
    public Integer exchangeInstrumentID;

  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "mdp", "quotesList", "listQuotes" })
  public static class Result {

    @JsonProperty("marketDataPort")
    public Integer marketDataPort;
    @JsonProperty("unsubList")
    public List<UnsubList> unsubList = null;
  }

}