package com.priceaction.model.xts;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "type", "code", "description", "result" })
public class OrderBookRs implements Serializable {

  @JsonProperty("type")
  public String type;
  @JsonProperty("code")
  public String code;
  @JsonProperty("description")
  public String description;
  @JsonProperty("result")
  public List<OrderBook> result;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "LoginID", "ClientID", "AppOrderID", "OrderReferenceID", "GeneratedBy", "ExchangeOrderID",
      "OrderCategoryType", "ExchangeSegment", "ExchangeInstrumentID", "OrderSide", "OrderType", "ProductType",
      "TimeInForce", "OrderPrice", "OrderQuantity", "OrderStopPrice", "OrderStatus", "OrderAverageTradedPrice",
      "LeavesQuantity", "CumulativeQuantity", "OrderDisclosedQuantity", "OrderGeneratedDateTime",
      "ExchangeTransactTime", "LastUpdateDateTime", "OrderExpiryDate", "CancelRejectReason", "OrderUniqueIdentifier",
      "OrderLegStatus", "MessageCode", "MessageVersion", "TokenID", "ApplicationType", "SequenceNumber" })

  public static class OrderBook {

    @JsonProperty("LoginID")
    public String loginID;
    @JsonProperty("ClientID")
    public String clientID;
    @JsonProperty("AppOrderID")
    public String appOrderID;
    @JsonProperty("OrderReferenceID")
    public String orderReferenceID;
    @JsonProperty("GeneratedBy")
    public String generatedBy;
    @JsonProperty("ExchangeOrderID")
    public String exchangeOrderID;
    @JsonProperty("OrderCategoryType")
    public String orderCategoryType;
    @JsonProperty("ExchangeSegment")
    public String exchangeSegment;
    @JsonProperty("ExchangeInstrumentID")
    public String exchangeInstrumentID;
    @JsonProperty("OrderSide")
    public String orderSide;
    @JsonProperty("OrderType")
    public String orderType;
    @JsonProperty("ProductType")
    public String productType;
    @JsonProperty("TimeInForce")
    public String timeInForce;
    @JsonProperty("OrderPrice")
    public double orderPrice;
    @JsonProperty("OrderQuantity")
    public long orderQuantity;
    @JsonProperty("OrderStopPrice")
    public double orderStopPrice;
    @JsonProperty("OrderStatus")
    public String orderStatus;
    @JsonProperty("OrderAverageTradedPrice")
    public String orderAverageTradedPrice;
    @JsonProperty("LeavesQuantity")
    public int leavesQuantity;
    @JsonProperty("CumulativeQuantity")
    public int cumulativeQuantity;
    @JsonProperty("OrderDisclosedQuantity")
    public int orderDisclosedQuantity;
    @JsonProperty("OrderGeneratedDateTime")
    public String orderGeneratedDateTime;
    @JsonProperty("ExchangeTransactTime")
    public String exchangeTransactTime;
    @JsonProperty("LastUpdateDateTime")
    public String lastUpdateDateTime;
    @JsonProperty("OrderExpiryDate")
    public String orderExpiryDate;
    @JsonProperty("CancelRejectReason")
    public String cancelRejectReason;
    @JsonProperty("OrderUniqueIdentifier")
    public String orderUniqueIdentifier;
    @JsonProperty("OrderLegStatus")
    public String orderLegStatus;
    @JsonProperty("MessageCode")
    public long messageCode;
    @JsonProperty("MessageVersion")
    public long messageVersion;
    @JsonProperty("TokenID")
    public long tokenID;
    @JsonProperty("ApplicationType")
    public long applicationType;
    @JsonProperty("SequenceNumber")
    public long sequenceNumber;

  }
}
