package com.priceaction.model.xts;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.priceaction.client.xts.XtsRequestProvider.Segment;

public class Instrument {
  @JsonProperty("exchangeSegment")
  private Integer exchangeSegment;
  @JsonProperty("exchangeInstrumentID")
  private Integer exchangeInstrumentID;

  public Instrument() {
  }

  public Integer getExchangeSegment() {
    return exchangeSegment;
  }
  

  @JsonIgnore
  public void setExchangeSegment(final String exchangeSegment) {
    this.exchangeSegment = Segment.getByNamy(exchangeSegment).id;
  }

  public void setExchangeSegment(Integer exchangeSegment) {
    this.exchangeSegment = exchangeSegment;
  }

  public Integer getExchangeInstrumentID() {
    return exchangeInstrumentID;
  }

  public void setExchangeInstrumentID(Integer exchangeInstrumentID) {
    this.exchangeInstrumentID = exchangeInstrumentID;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((exchangeInstrumentID == null) ? 0 : exchangeInstrumentID.hashCode());
    result = prime * result + ((exchangeSegment == null) ? 0 : exchangeSegment.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Instrument other = (Instrument) obj;
    if (exchangeInstrumentID == null) {
      if (other.exchangeInstrumentID != null)
        return false;
    } else if (!exchangeInstrumentID.equals(other.exchangeInstrumentID))
      return false;
    if (exchangeSegment == null) {
      if (other.exchangeSegment != null)
        return false;
    } else if (!exchangeSegment.equals(other.exchangeSegment))
      return false;
    return true;
  }
}