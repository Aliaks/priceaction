
package com.priceaction.model.xts;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "code", "description", "result" })
public class SubscriptionRs {

  @JsonProperty("type")
  public String type;
  @JsonProperty("code")
  public String code;
  @JsonProperty("description")
  public String description;
  @JsonProperty("result")
  public Result result;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "exchangeSegment", "exchangeInstrumentID" })
  public static class QuotesList {

    @JsonProperty("exchangeSegment")
    public Integer exchangeSegment;
    @JsonProperty("exchangeInstrumentID")
    public Integer exchangeInstrumentID;

  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "mdp", "quotesList", "listQuotes" })
  public static class Result {

    @JsonProperty("mdp")
    public Integer mdp;
    @JsonProperty("quotesList")
    public List<QuotesList> quotesList = null;
    @JsonProperty("listQuotes")
    public List<String> listQuotes = null;

  }

}