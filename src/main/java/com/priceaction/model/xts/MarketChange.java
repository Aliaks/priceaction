package com.priceaction.model.xts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.priceaction.model.xts.OrderBookRs.OrderBook;

/**
 * 
 * This class will include market changes/event per one main circle 
 * connected to particular instrument
 * corresponding BookOrderRs(s), LTP, openPrice
 */
public class MarketChange {
  public Instrument instrument;
  
  public Quote quote;
  
  public List<OrderBook> orderBooks = new ArrayList<>();
  
  public Double getLtp() {
    
    return quote.touchline.lastTradedPrice;
  }
  
   public BigDecimal getLtpBigDec() {
    if (quote.touchline.lastTradedPrice == null) {
      return null;
    }
    return BigDecimal.valueOf(quote.touchline.lastTradedPrice);
  }
  
  public Double getOpenPrice() {
    return quote.touchline.open; 
  }

  public BigDecimal getOpenPriceBigDec() {
    return BigDecimal.valueOf(quote.touchline.open); 
  }
}
