package com.priceaction.model.xts;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
//import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "code", "description", "result" })
public class LoginRs {

  @JsonProperty("type")
  public String type;
  @JsonProperty("code")
  public String code;
  @JsonProperty("description")
  public String description;
  @JsonProperty("result")
  public Result result;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

  /*
   * @Override public String toString() { return new
   * ToStringBuilder(this).append("type", type).append("code",
   * code).append("description", description).append("result",
   * result).append("additionalProperties", additionalProperties).toString(); }
   */

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({ "token" })
  public static class Result {

    @JsonProperty("token")
    public String token;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
      return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
      this.additionalProperties.put(name, value);
    }

    /*
     * @Override public String toString() { return new
     * ToStringBuilder(this).append("token", token).append("additionalProperties",
     * additionalProperties).toString(); }
     */

  }
}
/*
 * Use this tool offline:Maven pluginGradle pluginAnt taskCLIJava API Reference
 * properties
 */
