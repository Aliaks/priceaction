package com.priceaction.model.xts;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
//import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "exchangeSegment", "exchangeInstrumentID", "productType", "orderType", "orderSide", "timeInForce",
    "disclosedQuantity", "orderQuantity", "limitPrice", "stopPrice", "orderUniqueIdentifier" })
public class PlaceOrderRq {

  @JsonProperty("exchangeSegment")
  public String exchangeSegment;
  @JsonProperty("exchangeInstrumentID")
  public Integer exchangeInstrumentID;
  @JsonProperty("productType")
  public String productType;
  @JsonProperty("orderType")
  public String orderType;
  @JsonProperty("orderSide")
  public String orderSide;
  @JsonProperty("timeInForce")
  public String timeInForce = "DAY";
  @JsonProperty("disclosedQuantity")
  public Integer disclosedQuantity;
  @JsonProperty("orderQuantity")
  public Integer orderQuantity;
  @JsonProperty("limitPrice")
  public Double limitPrice;
  @JsonProperty("stopPrice")
  public Double stopPrice;
  @JsonProperty("orderUniqueIdentifier")
  public String orderUniqueIdentifier = "jhgjhg";

}
