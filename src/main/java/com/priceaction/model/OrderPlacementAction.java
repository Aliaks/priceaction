package com.priceaction.model;

import java.util.ArrayList;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;

import com.priceaction.model.xts.PlaceOrderRq;
import com.priceaction.model.xts.PlaceOrderRs;
import com.priceaction.client.xts.XtsClient;
import com.priceaction.client.xts.XtsRequestProvider;
import com.priceaction.model.xts.ModifyOrderRq;

public class OrderPlacementAction {
  
  public enum ActionType {
    PLACE, MODIFY, CANCEL
  }
  
  public enum ActionTarget {
    ENTRY, TAEGET, STOPLOSS
  }
  
  private List<Integer> newQttys; // for modifucation action, tbi
  
  private List<Integer> taegetIds;   //list??
  
  private Integer filledQuantity;
  
  private XtsClient xtsClient;
  
  private XtsRequestProvider xtsRequestProvider;
  
  private ActionType type;
  
  private ActionTarget target;
  
  public ExcelData excelDataEntry;
  
  public List<PlaceOrderRq> ordersToPlace;
  
  public List<String>  appOrderIdsToCancel;
  
  public List<String>  orderUniqueIdentifiersToCancel;
  
  public List<ModifyOrderRq> ordersToModify;

  
  public OrderPlacementAction(final XtsClient xtsClient, 
      final XtsRequestProvider xtsRequestProvider, final ExcelData excelDataEntry) {
    super();
    this.xtsClient = xtsClient;
    this.xtsRequestProvider = xtsRequestProvider;
    this.excelDataEntry = excelDataEntry;
    this.excelDataEntry = excelDataEntry;
    newQttys = new ArrayList<>();
    taegetIds = new ArrayList<>();

  }

  public OrderPlacementAction withType(ActionType type) {
    this.type = type;
    return this;
  }
  
  public OrderPlacementAction withNewQtties(final Integer ...newQtt) {
    for(Integer qt: newQtt) {
      newQttys.add(qt);
    }
  //  this.newQttys = newQtt;
    return this;
  }
  
  public OrderPlacementAction withTarget(ActionTarget target) {
    this.target = target;
    return this;
  }
  
  public OrderPlacementAction withTargetIds(final Integer ...tgtIds) {
    for(Integer tid: tgtIds) {
      taegetIds.add(tid);
    }
      return this;
    }
  public OrderPlacementAction withFilledQuantity(final Integer filledQuantity) {
    this.filledQuantity = filledQuantity;
    return this;
  }

  public static OrderPlacementAction newEmpty() {
    OrderPlacementAction opd = new OrderPlacementAction(null, null, null);
    opd.ordersToPlace = new ArrayList<>();
    opd.appOrderIdsToCancel = new ArrayList<>();
    opd.ordersToModify = new ArrayList<>();
    opd.appOrderIdsToCancel = new ArrayList<>();
    
    return opd;
  }
  
  private Void executeEntryPlacement() {
    final PlaceOrderRq placeEntryOrder = xtsRequestProvider.placeEntryOrder(excelDataEntry);
    try {
      final PlaceOrderRs placeOrder = xtsClient.placeOrder(placeEntryOrder);
      excelDataEntry.entryAppOrderID = placeOrder.result.appOrderID;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  private boolean paceAllTargets() {
    return taegetIds.isEmpty(); 
  }

  private List<PlaceOrderRq> placeTargetsExitOrders() {
    return taegetIds.stream()
    .map(trg -> xtsRequestProvider.placeExitOrder(excelDataEntry, trg))
    .collect(Collectors.toList());
  }

  
  private Void executeExitPlacement() {
    final List<PlaceOrderRq> placeTargetsOrders = paceAllTargets()?
        xtsRequestProvider.placeExitOrders(excelDataEntry):
          placeTargetsExitOrders();
        
      placeTargetsOrders.forEach(rq -> {
        try {
        final PlaceOrderRs placeOrder = xtsClient.placeOrder(rq);
        excelDataEntry.exitAppOrderIDs.add(placeOrder.result.appOrderID);
        } catch (Exception e) {
          //todo add rqlog and log error
          e.printStackTrace();
        }        
      });
    return null;
  }
  
  private Void executeStoplossPlacement() {
    final Integer qtty = paceAllTargets()?
        xtsRequestProvider.getTotalQuantity(excelDataEntry):
          newQttys.stream().reduce(0, (i, j) -> i + j);
    final PlaceOrderRq placeStopLossOrder = xtsRequestProvider.placeStopLossOrder(excelDataEntry, qtty);
    try {
      final PlaceOrderRs placeOrder = xtsClient.placeOrder(placeStopLossOrder);
      excelDataEntry.stoplossAppOrderID = placeOrder.result.appOrderID;
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    return null;
  }

  
  private void on(final ActionType type, 
      final ActionTarget target, final Supplier<Void> exec) {
    if (this.target == target && this.type == type) {
      exec.get();
    }
  }
  
  public void execute() {
    on(ActionType.PLACE, ActionTarget.ENTRY, this::executeEntryPlacement);
    on(ActionType.PLACE, ActionTarget.TAEGET, this::executeExitPlacement);
    on(ActionType.PLACE, ActionTarget.STOPLOSS, this::executeStoplossPlacement);
  }

}
