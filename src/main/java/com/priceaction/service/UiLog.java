package com.priceaction.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

import javax.annotation.PostConstruct;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.priceaction.model.AppSettingsModel;
import com.priceaction.model.xts.QuoteRs;
//import com.priceaction.model.arrow.ArrowRespResponseRs;
//import com.priceaction.model.arrow.ArrowResponse;
import com.priceaction.ui.PriceActionUiMain;


public abstract class UiLog {
  protected JTextArea output;
  
  @Autowired
  protected PriceActionUiMain priceActionUiMain;
 
  protected abstract void initOutputPane();
  
  protected Path logFile;
  
  public void writeToFile(final String txt) {
    try {
     // Path path = Paths.get(AppConfig.APP_MODEL_FILE);
      Files.write(logFile, (txt + "\n").getBytes(), StandardOpenOption.APPEND);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  protected void initLogFile() {
    try {
       Files.write(logFile, "Starting".getBytes());
     } catch (IOException e) {
       e.printStackTrace();
     }
  }
  
  public  void info(final String txt) {
    final String str = LocalDateTime.now() + ": " + txt + "\n";
    SwingUtilities.invokeLater(()->output.append(str));
    writeToFile(str);
  }
  
  public  void error(final String txt) {
    final String str = "ERROR: " + LocalDateTime.now() + ": " + txt + "\n";
    SwingUtilities.invokeLater(()->
      {
        output.append(str);
      });
    writeToFile(str);
  }
  
  
  public  void error(final Throwable e) {
    final String str = "ERROR: " + LocalDateTime.now() + ": " + e.getMessage() 
    + "\n";
    SwingUtilities.invokeLater(()->
      {
        output.append(str);
      });
    writeToFile(str);
  }
  
  
  /*
  public  void jsonInfo(final T data) {
    convertToJson(data);
    output.append(LocalDateTime.now() + ": " + txt + "\n");
  }
  */
  
  protected static <T> String convertToJson(final T data) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return  mapper.writeValueAsString(data);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      return "error";
    }
  }
  
  
  /*
  protected static <T> T convertToObject(final String data, final Class<T> c) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.readValue(data, c);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }
  
  */
 
  
  
  
  @Service(value = "uiAppLog")
  public static class AppUiLog extends UiLog {

    @Override
    @PostConstruct
    protected void initOutputPane() {
      output = priceActionUiMain.getTxaAppLog();
      logFile = Paths.get("all.log");
      initLogFile();
    }
    
    public void info(final AppSettingsModel appSettingsModel) {
      final String str = convertToJson(appSettingsModel);
      info(str);
    }
    
  }
  
  @Service(value = "rqRsUiLog")
  public static class RqRsUiLog extends UiLog {

    @Override
    @PostConstruct
    protected void initOutputPane() {
      output = priceActionUiMain.getTxaRqRs();
      logFile = Paths.get("rqrs.log");
      initLogFile();
    }
    
    
    /*
    public  void logResp(final ArrowResponse rs) {
      rs.onError(e -> error(convertToJson(e)));
      rs.onSuccess(d -> info(convertToJson(d)));
    }
    
    public  void logRespRs(final ArrowRespResponseRs rs) {
      rs.onError(e -> error(convertToJson(e)));
      rs.onSuccess(d -> info(convertToJson(d)));
    }
    */
    
    public void dumpQuoteRs(final QuoteRs rs) {
      final String qrs = convertToJson(rs);
      writeToFile(qrs + "/n");
     }

  @Service(value = "uiExelLog")
  public static class ExelUiLog extends UiLog {

    @Override
    @PostConstruct
    protected void initOutputPane() {
      output = priceActionUiMain.getTxaExelLogs();
      logFile = Paths.get("excelimp.log");
      initLogFile();
    }
  }
  }
}
