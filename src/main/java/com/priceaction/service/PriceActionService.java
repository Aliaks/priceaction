package com.priceaction.service;

import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.priceaction.app.AppConfig;
import com.priceaction.app.PriceActionConfig;
import com.priceaction.client.arrow.ArrowClient;
import com.priceaction.client.xts.MarketSymboleParser.SymbolData;
import com.priceaction.client.xts.XtsClient;
import com.priceaction.model.AppSettingsModel;
import com.priceaction.model.CashSettingsModel;
import com.priceaction.model.ExcelData;
import com.priceaction.model.FuturesSettingsModel;
import com.priceaction.model.xts.LoginRs;
import com.priceaction.service.UiLog.AppUiLog;
import com.priceaction.ui.SeqTokenInputDlg;
import com.priceaction.utils.SignatureHelper;



@Service
public class PriceActionService {
  
  // -- deprecated Arrow client ----
  @Autowired
  @Qualifier("arrowClient") ArrowClient arrowClient;
  
  @Autowired SignatureHelper signatureHelper;
  
  ArrowClientHelper arrowClientHelper;
  // -------------------------------
  
  
  
  @Autowired
  @Qualifier("xtsClient")
  private XtsClient xtsClient;
  
  @Autowired AppUiLog uiAppLog;  
  
  
  @Autowired AppConfig appSettings;
  
  @Autowired
  private ExcelImportService excelImportService;
  
  @Autowired
  private ObjectMapper objectMapper;
  
  @Autowired
  private TradingService tradingService;
  
  @Autowired StateService stateService;
  
  
  @Autowired SeqTokenInputDlg seqTokenInputDlg;

  private List<ExcelData> exDataImport;
  
  private AppSettingsModel appSettingsModel;
  
  public AppSettingsModel getAppSettingsModel() {
    return appSettingsModel;
  }

  @PostConstruct
  public void init() {
    appSettingsModel = new AppSettingsModel();
    appSettingsModel.cashSettingsModel = new CashSettingsModel();
    appSettingsModel.futuresSettingsModel = new FuturesSettingsModel();
    readAppSettings();
    stateService.fireTranzition(StateService.State.NOT_CONNECTED);
    if (isNotEmpty(appSettingsModel.importedFile)) {
      importExData(appSettingsModel.importedFile);
    } else {
      stateService.fireReTranzition();
    }
    
  }
  
  public void importExData(final String path) {
    exDataImport = excelImportService.importEcelData(path);
    appSettingsModel.importedFile = path;
    stateService.fireReTranzition();
  }
  
  public List<ExcelData> getExDataImport() {
    return exDataImport;
  }

  public void setFuturesSettings (final FuturesSettingsModel cashSettings) {
    appSettingsModel.futuresSettingsModel = cashSettings;
  }
  
  public FuturesSettingsModel getFuturesSettings() {
    return appSettingsModel.futuresSettingsModel;
  }
  
  public void setCashSettings (final CashSettingsModel cashSettings) {
    appSettingsModel.cashSettingsModel = cashSettings;
  }
  
  public CashSettingsModel getCashSettings() {
    return appSettingsModel.cashSettingsModel;
  }
  
  public void saveAppSettings() {
    try {
      objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
      StringWriter strModel = new StringWriter();
      objectMapper.writeValue(strModel, appSettingsModel);
      Files.write(Paths.get(AppConfig.APP_MODEL_FILE), strModel.toString().getBytes());
    } catch (IOException e) {
      uiAppLog.error(e);
      e.printStackTrace();
    }
  }

  public void readAppSettings() {
    if (!Files.exists(Paths.get(AppConfig.APP_MODEL_FILE))) {
      uiAppLog.info("Settings file does not exists, using default values");
      return;
    }
    byte[] jsonData;
    try {
      jsonData = Files.readAllBytes(Paths.get(AppConfig.APP_MODEL_FILE));
      appSettingsModel = objectMapper.readValue(jsonData, AppSettingsModel.class);
      uiAppLog.info(appSettingsModel);
    } catch (IOException e) {
      uiAppLog.error(e);
      e.printStackTrace();
    }
  }
  
 public void postMessage(final String msg) {
   uiAppLog.info(msg);
 }
 
 public void onLoginFail() {
   uiAppLog.info("onLoginFail");
    final StateService.Error error = new StateService.Error();
    error.msg = "Login error.";
    stateService.fireTranzition(StateService.State.NOT_CONNECTED, error);
  }
  
  public void doLogin() {
    uiAppLog.info("Connecting...");
    stateService.fireTranzition(StateService.State.CONNECTING);
    
    CompletableFuture
    .runAsync(() -> {
      uiAppLog.info("start xst interactive login");
      final LoginRs interactiveLoginRs = xtsClient.doUserInteractiveLogin();
      uiAppLog.info("interactive login -- success code: " + interactiveLoginRs.code);
      appSettings.setXtsInteractiveSeaaionToken(interactiveLoginRs.result.token);
    }).thenRunAsync(() -> {
      //to do market login
      uiAppLog.info("start xst market login");
      final LoginRs marketLoginRs = xtsClient.doUserMarktLogin();
      uiAppLog.info("market login -- success code: " + marketLoginRs.code);
      appSettings.setXtsMarketSeaaionToken(marketLoginRs.result.token);
    }).thenRunAsync(() -> {
      if (appSettings.getSymbolsData() == null) {
        uiAppLog.info("starting symbole data fetch");
        final Map<String, SymbolData> marketSymbols = xtsClient.doFetchMarketSymbols();
        uiAppLog.info("found symbols: " + marketSymbols.size());
        appSettings.setSymbolsData(marketSymbols);        
      }
    }).thenRun(()->{
      uiAppLog.info("success login");
    //  updateExcelData(appSettings.getSymbolsData(), getExDataImport());
      stateService.fireTranzition(StateService.State.CONNECTED);
    })
    .exceptionally(t -> {
      uiAppLog.error(t.getMessage());
      t.printStackTrace();
      onLoginFail();
       return null; 
    });
  }
  
  public void doLogout() {
    uiAppLog.info("Disonnecting...");
    stateService.fireTranzition(StateService.State.NOT_CONNECTED);
  }
  
  public boolean isExcelDataImported() {
    return exDataImport != null;
  }
  
  
  public void doStartTrading() {
    final String securitySubQuery = appSettings.getSecuritySubQuery();
    uiAppLog.info("Starting with seq subquery: " + securitySubQuery);
    tradingService.startTrading();
//    final ArrowRespResponseRs<MarketWatchList, MWLError> getMWListRs = arrowClient.doGetMWList();
  }
  
  public void doStopTrading() {
    tradingService.stopTrading();
  //  stateService.fireTranzition(StateService.State.CONNECTED);
    
  }
  
  public void setSegment(AppSettingsModel.Segment segment) {
    appSettingsModel.currentSegment = segment;
    stateService.fireReTranzition();
  }

  public void updateExcelData() {
    updateExcelData(appSettings.getSymbolsData(), getExDataImport());  
  }
  
  public void updateExcelData(final Map<String, SymbolData> marketSymbols, final List<ExcelData> excelData) {
    for (SymbolData symData: marketSymbols.values()) {
      for (ExcelData excEntry: excelData) {
        updateExcelEntry(symData, excEntry);
      }
    }
  }
  
  /*
   * 
   * 
NSECM|SKFINDIA|3186|

NSECM|BHARATWIRE|16123|

NSECM|RETFMID150|8506|

SKFINDIA,NSECM,Buy,100,200,120,130,140,150,10,20,30,40

   */
  private void updateExcelEntry(final SymbolData symData, final ExcelData exEntry) {
    if (symData.exchangeSegment.equalsIgnoreCase(exEntry.exchangeSegment) 
        && symData.name.equalsIgnoreCase(exEntry.symbol)) {
      exEntry.exchangeInstrumentID = symData.exchangeInstrumentID;
    }
  }
  
}
