package com.priceaction.service;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.xml.crypto.dsig.spec.XSLTTransformParameterSpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.priceaction.client.arrow.ArrowClient;
import com.priceaction.client.xts.XtsClient;
import com.priceaction.client.xts.XtsRequestProvider;
import com.priceaction.model.ExcelData;
import com.priceaction.model.MarketData;
import com.priceaction.model.Order;
import com.priceaction.model.OrderPlacementAction;
import com.priceaction.model.TradingCurrentData;
import com.priceaction.model.xts.Instrument;
import com.priceaction.model.xts.MarketChange;
import com.priceaction.model.xts.OrderBookRs;
import com.priceaction.model.xts.OrderBookRs.OrderBook;
import com.priceaction.model.xts.Quote;
import com.priceaction.model.xts.QuoteRs;
import com.priceaction.model.xts.SubscriptionRs;
import com.priceaction.model.xts.UnsubscriptionRs;

@Service
public class TradingService {
  
  @Autowired
  private PriceActionService priceActionService;

  @Autowired
  private StateService stateService;
  
  @Autowired
  @Qualifier("orderPlacementService")
  private OrderPlacementService orderPlacementService;
  
  @Autowired
  @Qualifier("xtsClient")
  private XtsClient xtsClient;
  
  @Autowired
  @Qualifier("xtsRequestProvider")
  private XtsRequestProvider xtsRequestProvider;
  
  private boolean stopTradingFlg;
  
  private boolean squareOffInitiated = false; 
  
  private boolean testMode = true;

  private int tradesCompleted;

  private int openPosittionsNum;

  private ScheduledExecutorService scheduler;
  
  private boolean starting;
  
  public void startSquareOff() {
    squareOffInitiated = true;
  }
  
  public void startTrading() {
    
    starting = false;
    if (cancelTradingTimeCondition()) {
      priceActionService.postMessage("Cancel Pending Orders Time already passed.");
      return;
    }
  
    long initalDelay = getDurationSecondsToStart();
    
    scheduler = Executors.newScheduledThreadPool(1);            
    scheduler.scheduleAtFixedRate(()->doTrading(),
        initalDelay,
        TimeUnit.DAYS.toSeconds(1),
        TimeUnit.SECONDS);
    stateService.fireTranzition(StateService.State.PRE_TRADING);
    
    if (initalDelay<3) {
      priceActionService.postMessage("Trading starts now.");
    } else {
      final LocalTime startTrading = priceActionService
          .getAppSettingsModel().getStartTrading();
       priceActionService.postMessage("Trading will start at " + startTrading.toString());
    }
    
    stopTradingFlg = false;
  }
  
  private long getDurationSecondsToStart() {
    
    long initalDelay = 1;
    LocalTime startTrading = priceActionService
        .getAppSettingsModel().getStartTrading();
    
    final LocalTime now = LocalTime.now();
    
    if (now.isBefore(startTrading)) {
      Duration duration = Duration.between(now, startTrading);
      initalDelay = duration.getSeconds();
    }
    return initalDelay;
  }
  
  private void doTrading() {
    try {
    
    /*
     If current time is > Start Trading Time - if true continue,  
     If current time is < Cancel Pending Orders - if true continue, 
     If current position no. < max open positions 
     
     Q: how to calculate current open positions number?
         is it global or for each symbol? 
     
     If Squareoff is not initiated for the day  then place order. 
       Anyways if Squareoff is initiated it will close the trading for the day.  
       
       LTP (last trader price) is close to Entry%,  
       
       
       if Open for the day is between the entry and stoploss then place order immediately
       
       let me give you and example for both buy and short

       Example: Buy 100 with stoploss 99 and open for the day is 99.50 
       and we need to read the LTP, if LTP Is above Stoploss price, 
       then place order with 100 and 99 SL. 

       Short 100 with stoploss 101 and open for the day is 100.50 
       and we need to read the LTP, if LTP is below stoploss price, 
       then we place order for 100 wtih SL 101
       
       
       Q: Open for the day is taken once a day, 
       so, in case of this condition we just remove this symbol from trading, right?
       Q: Open for the day is used if there is no LTP? No transactions on this symbol yet?
       
       A: we just ignore this particular trade 
          and we would not need to subscribe for this symbol now, 
          however there may be a new trade in the same symbol later day. 
          So that time we will need to follow the process back
       
       
       -----
       
       Q: we can have several orders for the same symbol 
          but with different entry prices quantities or stoplosses? 
          
          there may be several entries for a same symbol, 
          so once one trade for a symbol is executed and trade is active, 
          then the other orders in excel for the same symbol will be in hold until this is closed.
           
          basically those orders wll be ignored
       Q: ok, thanks, so we should have this business  logic, right? or it is done by Arrow ?
      ------
          
       
       Q: in Excel data: for buy there should be stoploss < entry price 
          while for short there should be stiploss > entry price. Correct?
       A: yes perfect
          
       Q: should we filter out Excel data if values are incorrect?
       A: this will be good

       ---- 
       Q: what is target price in Excel data? May be price for Exit Order?
          for Entry Order we use entry price, 
          and once Entry Order is filled we place Exit Order with target price, 
          is this right?
       
       Q: how many orders should be generated for one Excel data entry (line)?
       
       Q: Should we generate Entry Order and Exit Order at the same time? 
          Is Excel Target prices for Exit orders? 
          At which moment we should place Exit order?
       ----
       
       
       Q: can we change values in CashSetting or futures settings while trading?
       
       Q: Probably, we should distinguish starting trading from scratch and restarting after update/re-import.
       
       
       Q: how to get quantity for CASH if flag take qtty from Excel is false? 
       
       
        
     */
    
    starting = true;
    
    boolean subsribed = false;
    
    openPosittionsNum = 0;
    tradesCompleted = 0;
    
    if (stopTradingFlg) {
      stateService.fireTranzition(StateService.State.CONNECTED);
      starting = false;
      return;
    }
    
    stateService.fireTranzition(StateService.State.TRADING);
    
    final List<ExcelData> excelData = priceActionService.getExDataImport();
    if (excelData == null || excelData.isEmpty()) {
      stateService.fireTranzition(StateService.State.CONNECTED);
      starting = false;
      return;
    }
    
    priceActionService.updateExcelData();
    
  //  List<Order> orders = getOrdersFromExcelData(excelData);
    
    SubscriptionRs subscription = null;
    
    Map<Instrument, MarketChange> marketChanges = null;
    
    
    while(true) {
      
      if (stopTradingFlg) {
        if (subscription != null) {
          final UnsubscriptionRs unsubscription = xtsClient.getUnsubscription();
          System.out.println("UNSUBSCRIBED insts:  " + unsubscription
              .result.unsubList.size());
          //TODO: move this into completable future.
        }
        break;
      }
      
      if (squareOffInitiated) {
        break;
      }
      
      if (cancelTradingTimeCondition()) {
        break;
      }
      
      if (maxPositionsReachedCondition()) {
        break;
      }
      
      if (squareOffStartedCondition()) {
        break;
      }
      
      if (!subsribed) {
        /*
        subscribeForSymbols(excelData);
        subsribed = true;
        final Map<String, MarketData> marketData = getSubscribedMarketData();
        filterOdersByOpenDayPrice(orders, marketData);
        */
        
        
       // executeByQuotes();
        
        System.out.println("Start Subscription.");
        subscription = xtsClient.getSubscription();
        final List<Quote> subscriptionQuotes = xtsRequestProvider.getSubscriptionQuotes(subscription);
        marketChanges = orderPlacementService.coolectMarketChanges(subscriptionQuotes);
        System.out.println("subscribed on " + subscriptionQuotes.size() + " quotes.");
        subsribed = true;
        sleep(1000);
        continue;
      }
      
      /*
      
      final Map<String, MarketData> marketData = getSubscribedMarketData();
      
      final Map<Boolean, List<Order>> dividedOrders = partitionOrdersByLastTraderPrice(orders, marketData);
      
      placeOrders(dividedOrders.get(Boolean.TRUE));
      orders = dividedOrders.get(Boolean.FALSE);
      */
      
      
  //    final List<QuoteRs> quotes = xtsClient.getQuotes();
      
    //  executeByQuotes();
      
      if (subscription != null) {
        
        /*
        final SubscriptionRs unsubscription = xtsClient.getUnsubscription();
        
        final List<Quote> subscriptionQuotes = xtsRequestProvider.getSubscriptionQuotes(unsubscription);
        System.out.println("UNSUBSCRIBED on " + subscriptionQuotes.size() + " quotes.");
        */
        
        final UnsubscriptionRs unsubscription = xtsClient.getUnsubscription();
        System.out.println("UNSUBSCRIBED insts:  " + unsubscription
            .result.unsubList.size());
        //subsribed = false;
        subscription = null;
      } else  {
        System.out.println("Start Subscription 1.");
        subscription = xtsClient.getSubscription();
        
        final List<Quote> subscriptionQuotes = xtsRequestProvider.getSubscriptionQuotes(subscription);
        System.out.println("subscribed on " + subscriptionQuotes.size() + " quotes.");
        marketChanges = orderPlacementService.coolectMarketChanges(subscriptionQuotes);
        System.out.println("Market changes collected: " + marketChanges.size());
        //dumpMarketChanges(marketChanges);
        //dumpExcelData(excelData);
        
        dumpMarketData(marketChanges, excelData);
        
        //subsribed = true;
        
        
        
        //MAIN ACTION:
        System.out.println("Start Order Book.");
        final OrderBookRs orderBook = xtsClient.getOrderBook();
        System.out.println("Order Books Found: " + orderBook.result.size());
        //orderPlacementService.coolectMarketChanges(marketChanges, orderBook.result);
        final Map<String, OrderBook> mapOrderBooks = orderPlacementService.mapOrderBooks(orderBook.result);
        System.out.println("Order Books Mapped: " + mapOrderBooks.size());
        final List<OrderPlacementAction> orderActions = orderPlacementService.createOrderActions(excelData, marketChanges, mapOrderBooks);
        System.out.println("Actions created: " + orderActions.size());
        
        orderActions.forEach(action -> action.execute()); //move to com future 
        System.out.println("Actions executed.");
      }
      sleep(1000);
    }
    if (squareOffInitiated) {
      doSuareOff();
    }
    
    cancelPendingOrders();
    starting = false;
    stateService.fireTranzition(StateService.State.CONNECTED);
    }catch(Throwable t) {
      t.printStackTrace();
    }
  }
  
  private void dumpMarketData(final Map<Instrument, MarketChange> marketChanges, 
      final List<ExcelData> excelData) {
    System.out.println("----");
    excelData.forEach(ed -> {
      final Instrument i = new Instrument();
      i.setExchangeInstrumentID(ed.exchangeInstrumentID);
      i.setExchangeSegment(ed.exchangeSegment);
      final MarketChange marketChange = marketChanges.get(i);
      if (marketChange != null) {
      System.out.print(ed.symbol + "  " + "Inst: " + marketChange.quote.exchangeInstrumentID +
          "Enter: " + ed.entryPrice + "Open: " + marketChange.getOpenPrice() +
          "LTP: " + marketChange.getLtp());
      }
      
    });
    System.out.println("----");
  }
  
  private void dumpMarketChanges(final Map<Instrument, MarketChange> marketChanges) {
    marketChanges.entrySet().forEach(en -> {
      System.out.println("    Inst: " + en.getKey().getExchangeInstrumentID());
      System.out.println("    Open: " + en.getValue().getOpenPrice());
      System.out.println("     LTP: " + en.getValue().getLtp());
    });
  }
  
  private void dumpExcelData(final List<ExcelData> excelData) {
    excelData.forEach(ed -> {
      System.out.println("     Inst: " + ed.exchangeInstrumentID);
      System.out.println("   Symbol: " + ed.symbol);
      System.out.println("    Enter: " + ed.entryPrice);
      System.out.println("stoploass: " + ed.stoplossPrice);
    });
  }

  /*
  private void executeByQuotes() {
    final List<QuoteRs> quotes = new ArrayList<>();
    final QuoteRs quote = xtsClient.getQuote();
    quotes.add(quote);
    //final List<QuoteRs> quotes = xtsClient.getQuotes();
    final OrderPlacementAction orderPlacementData = orderPlacementService
        .calculateOrderPlacementData(priceActionService.getExDataImport(), quotes);
    xtsClient.executeOrderPlacementData(orderPlacementData);
  }
  */
  
  
  
  public void stopTrading() {
    if(starting) {
    stopTradingFlg = true;
    if (scheduler != null) {
      try {
        scheduler.awaitTermination(200, TimeUnit.MILLISECONDS);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    } else {
      if (scheduler != null) {
        scheduler.shutdownNow();
        stateService.fireTranzition(StateService.State.CONNECTED);
      }
    }
  }
  
  private void sleep(final long ms) {
    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
    }
  }
  
  //If current time is < Cancel Pending Orders - if true continue, If 
  private boolean cancelTradingTimeCondition() {
    final LocalTime now = LocalTime.now();
    final LocalTime cancelPendingOrders = priceActionService
        .getAppSettingsModel().getCancelPendingOrders();
    return now.isAfter(cancelPendingOrders);
  }
  
  private void doSuareOff() {
    sleep(200);
    squareOffInitiated = false;
  }
  
  //If current position no. < max open positions
  private boolean maxPositionsReachedCondition() {
    return false;
  }
  
  //If Squareoff is not initiated for the day 
  private boolean squareOffStartedCondition() {
    return false;
  }
  
  private void cancelPendingOrders() {
    System.out.println("cancelPendingOrders:  Start Order Book.");
    final OrderBookRs orderBook = xtsClient.getOrderBook();
    System.out.println("cancelPendingOrders:   Order Books Found: " + orderBook.result.size());
    
  }
  
  
  /*
  private void subscribeForSymbols(final List<ExcelData> dataEntries) {
    
  }
  
  private void placeOrders(final List<Order> dataEntries) {
    
  }
  
  
  
  private Map<String, MarketData> getSubscribedMarketData() {
    return null;
  }
  */
  
  private List<Order> getOrdersFromExcelData(final List<ExcelData> exData) {
    final List<Order> orders = exData.stream()
    .flatMap(ed -> getOrdersFromExcelDataEntry(ed).stream())
    .collect(Collectors.toList());
    
    return orders;  //TODO: break input list on two parts
  }
  
  private List<Order> getOrdersFromExcelDataEntry(final ExcelData exd) {
    List<Order> orders = new ArrayList<Order>();
    final BigDecimal entryPercent = priceActionService
        .getAppSettingsModel().getEntryPercent();
    
    priceActionService
    .getAppSettingsModel().getMaxMtmLoss();
    
    priceActionService
    .getAppSettingsModel().getMaxOpenPositions();
    
    priceActionService
    .getAppSettingsModel().getNomOfTargets();
    
    priceActionService
    .getAppSettingsModel().getQuantityFromExel();
    
    priceActionService
    .getFuturesSettings().getQuantity();
    
    priceActionService.getAppSettingsModel().isSegmentCash();
    
    return orders;
  }
  
  private List<Order> filterOdersByOpenDayPrice(final List<Order> orders, 
      final Map<String, MarketData> marketData) {
    orders.removeIf(o -> !isOpenDayInEntryStopLossRange(o, marketData));
    return orders;
  }
  
  private static boolean isOpenDayInEntryStopLossRange(final Order o, 
      final Map<String, MarketData> marketData) {
    final MarketData orderMarketData = marketData.get(o.symbol);
    if (orderMarketData == null) {
      
      return false; //skip strange order and TODO: report error in logs
    }
    final BigDecimal openDayPrice = orderMarketData.openMarketPrice;
    return o.isOpenDayInEntryStopLossRange(openDayPrice);
  }
  
  /*
  private Map<Boolean, List<Order>> partitionOrdersByLastTraderPrice(final List<Order> orders, 
      final Map<String, MarketData> marketData) {
    return orders.stream()
        .collect(Collectors
            .partitioningBy(o -> isLastTraderPriceCloseToEntry(o, marketData)));
  }
  
  private static boolean isLastTraderPriceCloseToEntry(final Order o, 
      final Map<String, MarketData> marketData) {
    final MarketData orderMarketData = marketData.get(o.symbol);
    if (orderMarketData == null) {
      
      return false; //skip strange order and TODO: report error in logs
    }
    final BigDecimal lastTraderPrice = orderMarketData.lastTraderPrice;
    return o.isLastTraderPriceCloseToEntry(lastTraderPrice);
  }
  */
  
  
  /*
  private void onMarcteWatchData(final List<MarketData> marketData) {
    //final BigDecimal last
  }
  */
  
  private boolean isLtpCloseToEntryPst(final BigDecimal ltp, final BigDecimal entryPct) {
    final BigDecimal dlt = ltp.subtract(entryPct).abs();
    return dlt.doubleValue() <= 10.0; 
  }
  
  
  /*
  public void checkPlacedOrdersStatus(final Map<String, MarketData> marketData) {
    if (testMode) {
      int increase = 0;
      Random random = new Random();
      final int rndInt = random.ints(0,(10)).findFirst().getAsInt();
      if (rndInt == 9) {
        increase = 3;
      } else if (rndInt == 8) {
        increase = 2;
      }else if (rndInt == 7) {
        increase = 1;
      }
      if (increase > 0) {
        tradesCompleted =+ increase;
        final TradingCurrentData td = new TradingCurrentData();
        td.tradesNumber = tradesCompleted;
        stateService.tradeDataUpdates(td);
      }
    }
  }
  */
  
  
}
