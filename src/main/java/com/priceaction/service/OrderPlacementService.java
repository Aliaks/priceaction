package com.priceaction.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.priceaction.app.AppConfig;
import com.priceaction.client.xts.XtsClient;
import com.priceaction.client.xts.XtsRequestProvider;
import com.priceaction.model.ExcelData;
import com.priceaction.model.OrderPlacementAction;
import com.priceaction.model.OrderPlacementAction.ActionTarget;
import com.priceaction.model.OrderPlacementAction.ActionType;
import com.priceaction.model.xts.Instrument;
import com.priceaction.model.xts.MarketChange;
import com.priceaction.model.xts.Quote;
import com.priceaction.model.xts.QuoteRs;
import com.priceaction.model.xts.OrderBookRs.OrderBook;
import com.priceaction.service.UiLog.RqRsUiLog;

@Service(value = "orderPlacementService")
public class OrderPlacementService {
  
  @Autowired
  private AppConfig appSettings;
  
  @Autowired
  private PriceActionService priceActionService;
  
  @Autowired
  @Qualifier("xtsRequestProvider")
  private XtsRequestProvider xtsRequestProvider;
  
  @Autowired
  @Qualifier("rqRsUiLog")
  private RqRsUiLog rqRsUiLog; 
  
  @Autowired
  @Qualifier("xtsClient")
  private XtsClient xtsClient;
  
  public Map<Instrument, MarketChange> coolectMarketChanges(final List<Quote> quotes) {
    Map<Instrument, MarketChange> changes = 
    quotes.stream().collect(Collectors.toMap(q -> {
      final Instrument i = new Instrument();
      i.setExchangeInstrumentID(q.exchangeInstrumentID);
      i.setExchangeSegment(q.exchangeSegment);
      return i;
    },
        q -> {
          final MarketChange marketChange = new MarketChange();
          marketChange.quote = q;
          return marketChange;
        }));
    
    return changes;
  }
 
  public Map<Instrument, MarketChange> coolectMarketChanges(final Map<Instrument, MarketChange> changes, 
      final List<OrderBook> orderBooks) {
    orderBooks.stream().forEach(ob -> {
      final Instrument i = new Instrument();
      i.setExchangeInstrumentID(Integer.parseInt(ob.exchangeInstrumentID));
      i.setExchangeSegment(Integer.parseInt(ob.exchangeSegment));
      
      final MarketChange marketChange = changes.get(i);
      if (marketChange != null) {
        marketChange.orderBooks.add(ob);
      }
    });
    return changes;
  }
  
  
  public Map<String, OrderBook> mapOrderBooks(final List<OrderBook> orderBooks) {
    Map<String, OrderBook> result = orderBooks.stream()
        .collect(Collectors.toMap(ob->ob.appOrderID, Function.identity()));
    return result;
  }
  
  public List<OrderPlacementAction> createOrderActions(final List<ExcelData> excelData,
      final Map<Instrument, MarketChange> marketChanges, 
      final Map<String, OrderBook> orderBooks) {
    List<OrderPlacementAction> actions = new ArrayList<>();
    excelData.forEach(ed -> {
      final Instrument i = new Instrument();
      i.setExchangeInstrumentID(ed.exchangeInstrumentID);
      i.setExchangeSegment(ed.exchangeSegment);
      final MarketChange marketChange = marketChanges.get(i);
      if (marketChange != null) {
        createOrderAction(ed, marketChange, orderBooks, actions);
      }
    });
    
    return actions;
  }
  
  
  public List<OrderPlacementAction> createOrderAction(final ExcelData excelData, final MarketChange marketChange,
      final Map<String, OrderBook> orderBooks, final List<OrderPlacementAction> actions
  /*
   * additional parameter indicating market change: BookOrder(s), changing prices
   */) {

    if (!excelData.isExitOrdersPlaced()) {
      if (!excelData.isEntryOrderPlaced()) {
        // 1. check if we have bookOrder for this, if yes --- ?? incorrect state?
        // 2. create entryOrder and create placeOrder and return placemant data

        final boolean placeEntryOrder = checkEntryOrderPlaceCondition(excelData, marketChange);
        if (placeEntryOrder) {
          final OrderPlacementAction action = createAction(excelData).withType(ActionType.PLACE)
              .withTarget(ActionTarget.ENTRY);
          actions.add(action);
        }
        return actions;
      }

      // 1. check if we have in bookOrder order for entry order. If not, create
      // placeOrder and return placemant data
      // 2. check order state: pending -- do nothing, cancelrd -- ??, rejected -- ??
      // 3. if order state is filled or partially filled
      // create exit orders and stoploss order and return create placeOrder and return
      // placemant data
      final OrderBook entryOrderBook = orderBooks.get(excelData.entryAppOrderID);
      if (entryOrderBook == null) {
        // todo report warning?
      } else if (Arrays.asList("Filled").contains(entryOrderBook.orderStatus)) {
//        if (is (entryOrderBook.orderStatus, in.Set, "Filled")) {
        /*
         * If buy order with 1000 quantity placed And if 250 quantities are filled and
         * we realized it, then imdtly we place stoploss order for 250 quatity, as the
         * quantity of buy order gets filled we keep modifying stoploss orders quatity
         */
        final OrderPlacementAction allTargetsAction = createAction(excelData)
            .withType(ActionType.PLACE)
            .withTarget(ActionTarget.TAEGET);
        actions.add(allTargetsAction);

        final OrderPlacementAction stoplossAction = createAction(excelData)
            .withType(ActionType.PLACE)
            .withTarget(ActionTarget.STOPLOSS);
        actions.add(stoplossAction);

      } else if ("Partially Filled".equalsIgnoreCase(entryOrderBook.orderStatus)) {
        final int orderFilledQuantity = (int) entryOrderBook.orderQuantity; // is this filled quantiyy?
        final OrderPlacementAction allTargetsAction = createAction(excelData)
            .withType(ActionType.MODIFY)
            .withTarget(ActionTarget.TAEGET)
            .withFilledQuantity(orderFilledQuantity);
        actions.add(allTargetsAction);
      }

    }
    return actions;
    // if entry order in filled or partially filled state
    // check if this is new changes. if yes, modify exit orders and stoploss order
    // and set modifications in placement data

    // if entry order in canceled or rejected state -- ?? remove exits and stoploss?

    // check stoploss exit orders states and changes. if there are fillment changes,
    // calculate fin values: mtm, ..

    // return pl dt
  }

  private boolean checkEntryOrderPlaceCondition(final ExcelData excelData, final MarketChange marketChange) {
    boolean placeEntryOrder;
    final Double ltp = marketChange.getLtp();
    if (ltp != null && ltp >0.0) {
      placeEntryOrder = excelData.isLastTraderPriceCloseToEntry(
          marketChange.getLtpBigDec(), priceActionService.getAppSettingsModel()
          .getEntryPercent());
    } else {
      placeEntryOrder = excelData.isOpenDayInEntryStopLossRange(
          marketChange.getOpenPriceBigDec());
    }
    return placeEntryOrder;
  }
  
  private OrderPlacementAction createAction(final ExcelData excelEntry) {
    return new OrderPlacementAction(xtsClient, xtsRequestProvider, excelEntry);
  }
  
  
  
  
  /*
   
   Order book consists of states of all the orders placed by an user. The states of the orders can be as follows:
Pending New
Open or New
Partially Filled
Filled
Pending Replace
Replaced
Cancelled
Rejected
    
   */
  
  public enum in {
    Set
  }
  
  public static boolean is (final String v, final in i, final String ...vs) {
    for (String s: vs) {
      if (s.equalsIgnoreCase(v)) {
        return true;
      }
    }
    return false;
  }
  
  /*
  public static class INC {
    public INC(final String str) {
      
    }
    
    public boolean in (final List<String> l) {
      return 
    }
  }
  */
  
  
  

}
