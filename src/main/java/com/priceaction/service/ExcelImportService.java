package com.priceaction.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.priceaction.model.ExcelData;
import com.priceaction.testclnt.TransactionType;

@Service
public class ExcelImportService {
  
  private final static String CSV_SEP = ",";
  
  @Autowired
  @Qualifier("uiExelLog")
  private UiLog uiExelLog;  
  
  public List<ExcelData> importEcelData(final String filePath) {
    List<ExcelData> excelData = new ArrayList<ExcelData>();
    try {
      Stream<String> lines = Files.lines(Paths.get(filePath));
      final List<ExcelData> exData = lines
      .skip(1)
      .map(this::bind)
      .filter(ed ->ed.transactionType != null)
      .collect(Collectors.toList());
      //.forEach(uiExelLog::info);
      lines.close();
      uiExelLog.info("imported enteries: " + exData.size());
      return exData;
  } catch(IOException io) {
      io.printStackTrace();
  }
    return excelData;
  }

  private ExcelData bind (final String line) {
    ExcelData ed = new ExcelData();
    final String[] entries = line.split(CSV_SEP);
    
    //Symbol, Segment, TransactionType, EntryPrice, StoplossPrice, Target1Price, Target2Price, Target3Price, 
    //Target4Price, Qty1, Qty2, Qty3, Qty4
    ed.symbol = entries[0].trim();
    ed.exchangeSegment = entries[1].trim();
    ed.transactionType = TransactionType.valueOfString(entries[2].trim());
    ed.entryPrice = new BigDecimal(entries[3].trim());
    ed.stoplossPrice = new BigDecimal(entries[4].trim());
    ed.target1Price = new BigDecimal(entries[5].trim());
    ed.target2Price = new BigDecimal(entries[6].trim());
    ed.target3Price = new BigDecimal(entries[7].trim());
    ed.target4Price = new BigDecimal(entries[8].trim());
    ed.qty1 = Integer.parseInt(entries[9].trim());
    ed.qty2 = Integer.parseInt(entries[10].trim());
    ed.qty3 = Integer.parseInt(entries[11].trim());
    ed.qty4 = Integer.parseInt(entries[12].trim());
    
    return ed;
  }
  
}
