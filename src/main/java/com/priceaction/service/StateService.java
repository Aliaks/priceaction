package com.priceaction.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.swing.SwingUtilities;

import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.priceaction.model.TradingCurrentData;
import com.priceaction.ui.PaStatus;

@Service
public class StateService {
  
  
  private State state;
  
  private Error error = Error.NO_ERROR;
  
  private List<StateService.TranzitionObsorver> observers = new ArrayList<>();
  
  private MultiValueMap<State, BiConsumer<State, State>> uiActionSubscriptios = new LinkedMultiValueMap<>();
  
  private List<Consumer<TradingCurrentData>> uiTradingDataSubscriptions = new ArrayList<>();
  
  
  /*
  public void subscribe (final StateService.TranzitionObsorver obsorver) {
    observers.add(obsorver);
  }

  public void unsubscribe (final StateService.TranzitionObsorver obsorver) {
    observers.remove(obsorver);
  }
  */
  
  public synchronized void tradeDataUpdates(final TradingCurrentData data) {
    SwingUtilities.invokeLater(() -> {
    uiTradingDataSubscriptions.forEach(uia -> uia.accept(data));
    });
  }
  
  public synchronized void subscrideOnTradingUpdate(Consumer<TradingCurrentData> uiTradeAction) {
    uiTradingDataSubscriptions.add(uiTradeAction);
  }
  
  public synchronized void unsubscrideOnTradingUpdate(Consumer<TradingCurrentData> uiTradeAction) {
    uiTradingDataSubscriptions.remove(uiTradeAction);
  }
  
  public synchronized void subscribeUiActionOnState (final State state, final BiConsumer<State, State> uiAction) {
    uiActionSubscriptios.add(state, uiAction);
  }
  
  public synchronized void unsubscribeUiActionOnState (final State state, final BiConsumer<State, State> uiAction) {
    uiActionSubscriptios.remove(state, uiAction);
  }
  
  private void postUiActions (final StateService.State from, final StateService.State to) {
    final List<BiConsumer<State, State>> uiActions = uiActionSubscriptios.get(to);
    if (uiActions != null) {
      SwingUtilities.invokeLater(() -> {
        uiActions.forEach(a->a.accept(from, to));
      });
    }
  }

  public synchronized State getState() {
    return state;
  }
  
  public synchronized boolean isError() {
    return error == Error.NO_ERROR;
  }
  
  
  public synchronized Error getError() {
    return error;
  }
  
  public synchronized void fireError(final Error err) {
    error = err;
    observers.forEach(o -> o.onError(error, this)); 
  }
  
  public synchronized void fireTranzition(final State newState) {
    error = Error.NO_ERROR;
    postUiActions(state, newState);
    state = newState;
    observers.forEach(o -> o.onTranzition(state, newState, this));
  }
  
  public synchronized void fireReTranzition() {
    error = Error.NO_ERROR;
    postUiActions(state, state);
    observers.forEach(o -> o.onTranzition(state, state, this));
  }
  
  
  public synchronized void fireTranzition(final State newState, Error err) {
    error = err;
    postUiActions(state, newState);
    state = newState;
    observers.forEach(o -> o.onTranzition(state, newState, error, this));
  }
  
   public enum State {
    NOT_CONNECTED, CONNECTING, CONNECTED, PRE_TRADING, TRADING
  }
  
  public static class Error {
    public Throwable e;
    public String msg;
    public String category;
    
    private static final Error NO_ERROR = new Error(); 
  }
  
  interface TranzitionObsorver {
    boolean onTranzition (final StateService.State from, final StateService.State to, 
        final StateService service);
    
    boolean onTranzition (final StateService.State from, final StateService.State to, 
        final StateService.Error err, final StateService service);
    boolean onError (final StateService.Error err, final StateService service);
  }

}
  
