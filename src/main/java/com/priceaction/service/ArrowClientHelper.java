package com.priceaction.service;

import static org.apache.commons.lang3.StringUtils.isEmpty;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CompletableFuture;

import com.priceaction.model.arrow.LoginRequestRs;
import com.priceaction.ui.SeqTokenInputDlg;

public class ArrowClientHelper {

  public void doLoginArrow(PriceActionService priceActionService) {
    priceActionService.uiAppLog.info("Connecting...");
    priceActionService.stateService.fireTranzition(StateService.State.CONNECTING);
    
    final LoginRequestRs loginResp = priceActionService.arrowClient.doUserLoginRequest();
    
    priceActionService.uiAppLog.info(loginResp.toString());
    if (loginResp.isResponseOk()) {
      priceActionService.uiAppLog.info("LoginRequest successful.");
      final String sessId = loginResp.getResponse().getSessId();
      priceActionService.appSettings.setSessId(sessId);
      final String signature = priceActionService.signatureHelper.getSignature(priceActionService.appSettings.getApiKey(), priceActionService.appSettings.getSecret(), 
          sessId);
      priceActionService.uiAppLog.info(" Signature: " + signature);
      
      //final String redirectRs = arrowClient.doRedirectRq(loginResp.getResponse().getLongUrl(), signature);
      //uiAppLog.uisLog("redirect rs: " + redirectRs);
      
      final String paramUrl = loginResp.getResponse().getLongUrl() + "?signature=" + signature;
      priceActionService.arrowClientHelper.openUrl(priceActionService, paramUrl);
      final String securitySubQuery = SeqTokenInputDlg.showReturnUrlDlg(priceActionService.seqTokenInputDlg);
      final boolean secQueryValid = priceActionService.arrowClientHelper.validateRedirectUrl(priceActionService, securitySubQuery);
      priceActionService.stateService.fireTranzition(StateService.State.CONNECTED);
    } else {
      priceActionService.onLoginFail();
    }
    
    //System.out.println(loginResp.toString());
  }

  String doSecondFaktorLogin(PriceActionService priceActionService, LoginRequestRs loginRequestRs) {
    final String sessId = loginRequestRs.getResponse().getSessId();
    priceActionService.appSettings.setSessId(sessId);
     final String signature = priceActionService.signatureHelper.getSignature(priceActionService.appSettings.getApiKey(), priceActionService.appSettings.getSecret(), 
         sessId);
     priceActionService.uiAppLog.info(" Signature: " + signature);
     
     final String paramUrl = loginRequestRs.getResponse().getLongUrl() + "?signature=" + signature;
     priceActionService.arrowClientHelper.openUrl(priceActionService, paramUrl);
     final String securitySubQuery = SeqTokenInputDlg.showReturnUrlDlg(priceActionService.seqTokenInputDlg);
    return securitySubQuery;
  }

  public void doLoginAsincArrow(PriceActionService priceActionService) {
     priceActionService.uiAppLog.info("Connecting...");
     priceActionService.stateService.fireTranzition(StateService.State.CONNECTING);
     
     final CompletableFuture<LoginRequestRs> ftSessId = CompletableFuture
     .supplyAsync(() -> {
       final LoginRequestRs rs = priceActionService.arrowClient.doUserLoginRequest(); 
       return rs;
     })
     .exceptionally(t -> {
       priceActionService.uiAppLog.error(t.getMessage());
       t.printStackTrace();
       priceActionService.onLoginFail();
        return null; 
     }).thenApply(rs->{
       final String securitySubQuery = doSecondFaktorLogin(priceActionService, rs);
       final boolean secQueryValid = priceActionService.arrowClientHelper.validateRedirectUrl(priceActionService, securitySubQuery);
       if (!secQueryValid) {
         priceActionService.onLoginFail();
         return null;
       }
       priceActionService.stateService.fireTranzition(StateService.State.CONNECTED);
       return rs;
     });
     
   }

  void openUrl (PriceActionService priceActionService, final String url) {
    if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
      try {
        Desktop.getDesktop().browse(new URI(url));
      } catch (IOException | URISyntaxException e) {
        priceActionService.uiAppLog.error(e);
        e.printStackTrace();
      }
  }
  }

  boolean validateRedirectUrl(PriceActionService priceActionService, final String securitySUbQuery) {
    priceActionService.appSettings.setSecuritySubQuery(securitySUbQuery);
    if (isEmpty(securitySUbQuery)) {
      return false;
    }
    final String[] split1 = securitySUbQuery.split("request_token=");
    if (isEmpty(split1[1])) {
      return false;
    }
    final String[] split2 = split1[1].split("&");
    
    if (isEmpty(split2[0])) {
      return false;
    }
    priceActionService.appSettings.setRequestToken(split2[0]);
    
    return true;
  }

  public void doLoginFake(PriceActionService priceActionService) {
     priceActionService.uiAppLog.info("Connecting...");
     priceActionService.stateService.fireTranzition(StateService.State.CONNECTING);
     
     CompletableFuture
     .runAsync(() -> {
        try {
          Thread.sleep(300);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
     })
     .exceptionally(t -> {
       priceActionService.uiAppLog.error(t.getMessage());
       t.printStackTrace();
       priceActionService.onLoginFail();
        return null; 
     }).thenRun(()->{
       priceActionService.stateService.fireTranzition(StateService.State.CONNECTED);
     });
     
   }

}
