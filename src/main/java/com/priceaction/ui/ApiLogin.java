package com.priceaction.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;

public class ApiLogin extends JDialog {

  private final JPanel contentPanel = new JPanel();
  private JTextField txtName;
  private JTextField txtApiKey;
  private JLabel lblName;
  private JLabel lblApiKey;
  
  private static ApiLogin apiLogin;
  private final Action actOkExit = new SwingAction();
  private final Action actCancelExit = new SwingAction_1();

  /**
   * Launch the application.
   */
  public static void main(String[] args) {
    startApiLoginDialog();
  }

  public static void startApiLoginDialog() {
    try {
      ApiLogin dialog = new ApiLogin();
      apiLogin = dialog;
      dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      dialog.setVisible(true);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Create the dialog.
   */
  public ApiLogin() {
    setTitle("API Login Details");
    setBounds(100, 100, 440, 212);
    getContentPane().setLayout(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
    getContentPane().add(contentPanel, BorderLayout.CENTER);
    contentPanel.setLayout(new GridLayout(2, 2, 10, 20));
    {
      lblName = new JLabel("Name:");
      lblName.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(lblName);
    }
    {
      txtName = new JTextField();
      txtName.setFont(new Font("Tahoma", Font.BOLD, 16));
      lblName.setLabelFor(txtName);
      contentPanel.add(txtName);
      txtName.setColumns(10);
    }
    {
      lblApiKey = new JLabel("API Key:");
      lblApiKey.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(lblApiKey);
    }
    {
      txtApiKey = new JTextField();
      lblApiKey.setLabelFor(txtApiKey);
      contentPanel.add(txtApiKey);
      txtApiKey.setColumns(10);
    }
    {
      JPanel buttonPane = new JPanel();
      buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
      getContentPane().add(buttonPane, BorderLayout.SOUTH);
      {
        JButton okButton = new JButton("OK");
        okButton.setAction(actOkExit);
        okButton.setFont(new Font("Tahoma", Font.BOLD, 16));
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);
      }
      {
        JButton cancelButton = new JButton("Cancel");
        cancelButton.setAction(actCancelExit);
        cancelButton.setFont(new Font("Tahoma", Font.BOLD, 16));
        cancelButton.setActionCommand("Cancel");
        buttonPane.add(cancelButton);
      }
    }
  }
  
  private static void exitApiLoginDialog() {
    apiLogin.setVisible(false);
    apiLogin.dispose();
    apiLogin = null;
  }

  private class SwingAction extends AbstractAction {
    public SwingAction() {
      putValue(NAME, "OK");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      exitApiLoginDialog();
    }
  }
  private class SwingAction_1 extends AbstractAction {
    public SwingAction_1() {
      putValue(NAME, "Cancel");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      exitApiLoginDialog();
    }
  }
}
