package com.priceaction.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import com.priceaction.app.AppConfig;

import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Dialog.ModalityType;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;

public class SeqTokenInputDlg extends JDialog {

  private final JPanel contentPanel = new JPanel();
  private JTextField txtRetUrl;
  
  @Autowired
  private AppConfig appSettings;
  private final Action action = new OkAction();
  private final Action action_1 = new CancelAction();

  /**
   * Launch the application.
   */
  public static void main(String[] args) {
    showReturnUrlDlg(new SeqTokenInputDlg());
  }

  public static String showReturnUrlDlg(final SeqTokenInputDlg dialog) {
    try {
    //  SeqTokenInputDlg dialog = new SeqTokenInputDlg();
      dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      dialog.setVisible(true);
      return dialog.txtRetUrl.getText();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "";
  }

  /**
   * Create the dialog.
   */
  public SeqTokenInputDlg() {
    setModalityType(ModalityType.APPLICATION_MODAL);
    setBounds(100, 100, 778, 127);
    getContentPane().setLayout(new BorderLayout());
    contentPanel.setLayout(new FlowLayout());
    contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(contentPanel, BorderLayout.CENTER);
    {
      JLabel lblEnterToken = new JLabel("Enter Return URL:  ");
      lblEnterToken.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(lblEnterToken);
    }
    {
      Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
      contentPanel.add(rigidArea);
    }
    {
      txtRetUrl = new JTextField();
      txtRetUrl.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(txtRetUrl);
      txtRetUrl.setColumns(30);
    }
    {
      JPanel buttonPane = new JPanel();
      buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
      getContentPane().add(buttonPane, BorderLayout.SOUTH);
      {
        JButton okButton = new JButton("OK");
        okButton.setAction(action);
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);
      }
      {
        JButton cancelButton = new JButton("Cancel");
        cancelButton.setAction(action_1);
        cancelButton.setActionCommand("Cancel");
        buttonPane.add(cancelButton);
      }
    }
  }

  public JTextField getTxtRetUrl() {
    return txtRetUrl;
  }
  private class OkAction extends AbstractAction {
    public OkAction() {
      putValue(NAME, "OK");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
  //    final String retUrl = txtRetUrl.getText();
      
    //  appSettings.setSecuritySubQuery(retUrl);
      
      SeqTokenInputDlg.this.setVisible(false);
    
      /*
      
      MultiValueMap<String, String> queryParams =
          UriComponentsBuilder.fromUriString(retUrl).build().getQueryParams();
      
      appSettings.setRequestToken(requestToken);
      */
      
      
      
    }
  }
  private class CancelAction extends AbstractAction {
    public CancelAction() {
      putValue(NAME, "Cancel");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      txtRetUrl.setText("");
      SeqTokenInputDlg.this.setVisible(false);
    }
  }
}
