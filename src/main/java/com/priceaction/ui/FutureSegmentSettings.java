package com.priceaction.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.springframework.beans.factory.annotation.Autowired;

import com.priceaction.model.CashSettingsModel;
import com.priceaction.model.FuturesSettingsModel;
import com.priceaction.service.PriceActionService;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JFormattedTextField;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Dialog.ModalityType;

public class FutureSegmentSettings extends JDialog {

  private final JPanel contentPanel = new JPanel();
  private JFormattedTextField txtStartTradingTime;
  private JFormattedTextField txtCancelPending;
  private JFormattedTextField txtMaxMTMLoss;
  private JLabel lblStartTradinf;
  private JLabel lblCancelOendind;
  private JLabel lblMaxPositionss;
  private JLabel lblMaxMTMLoss;
  private JFormattedTextField txtQuantity;
  private JFormattedTextField txtEntryPct;
  private JFormattedTextField txtCancelPct;
  private JLabel lblQuantityFromExcel;
  private JLabel lblCancelPct;
  private JLabel lblEntryPct;
  private JLabel lblNomOfTargets;
  private JLabel lblQuantity;
  private JLabel lblintradayPositional;
  private JComboBox cmbIntradayPositional;
  private final Action actOkExit = new OkExitAction();
  private final Action actCancelExit = new CancelExitAction();
  
  private static FutureSegmentSettings futureSegmentSettings;
  private JSpinner spnMaxPositions;
  private JComboBox cmbNumOfTargets;
  private JCheckBox chbQtyFromExcel;
  
  @Autowired
  private PriceActionService priceActionService;
  
  @Autowired
  private UiBinding uiBinding;

  /**
   * Launch the application.
   */
  public static void main(String[] args) {
   // startFuturesSettingsDialog();
  }

  public static void startFuturesSettingsDialog(final FutureSegmentSettings futuresSegmentSettings) {
    try {
      //FutureSegmentSettings dialog = new FutureSegmentSettings();
      futureSegmentSettings = futuresSegmentSettings;
      futuresSegmentSettings.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      futuresSegmentSettings.setVisible(true);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Create the dialog.
   */
  public FutureSegmentSettings() {
    setModalityType(ModalityType.APPLICATION_MODAL);
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowOpened(WindowEvent e) {
        final FuturesSettingsModel cashSettings = priceActionService.getFuturesSettings();
        uiBinding.setFuturesSettings(cashSettings,  FutureSegmentSettings.this);
      }
    });
    setTitle("Future Segment Settings");
    setFont(new Font("Dialog", Font.BOLD, 17));
    setBounds(100, 100, 760, 579);
    getContentPane().setLayout(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(10, 20, 10, 20));
    getContentPane().add(contentPanel, BorderLayout.CENTER);
    contentPanel.setLayout(new GridLayout(0, 2, 20, 20));
    {
      lblStartTradinf = new JLabel("Start Trading Time");
      lblStartTradinf.setHorizontalAlignment(SwingConstants.LEFT);
      lblStartTradinf.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(lblStartTradinf);
    }
    {
      txtStartTradingTime = PriceActionUiMain.createTimeFormattedTxtField();
      txtStartTradingTime.setFont(new Font("Tahoma", Font.BOLD, 16));
      lblStartTradinf.setLabelFor(txtStartTradingTime);
      contentPanel.add(txtStartTradingTime);
      txtStartTradingTime.setColumns(10);
    }
    {
      lblCancelOendind = new JLabel("Cancel Pending Orders:");
      lblCancelOendind.setHorizontalAlignment(SwingConstants.LEFT);
      lblCancelOendind.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(lblCancelOendind);
    }
    {
      txtCancelPending = PriceActionUiMain.createTimeFormattedTxtField();
      txtCancelPending.setFont(new Font("Tahoma", Font.BOLD, 16));
      lblCancelOendind.setLabelFor(txtCancelPending);
      contentPanel.add(txtCancelPending);
      txtCancelPending.setColumns(10);
    }
    {
      lblMaxPositionss = new JLabel("Max Open Positions: ");
      lblMaxPositionss.setHorizontalAlignment(SwingConstants.LEFT);
      lblMaxPositionss.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(lblMaxPositionss);
    }
    {
      spnMaxPositions = new JSpinner();
      spnMaxPositions.setFont(new Font("Tahoma", Font.BOLD, 16));
      lblMaxPositionss.setLabelFor(spnMaxPositions);
      spnMaxPositions.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
      contentPanel.add(spnMaxPositions);
    }
    {
      lblMaxMTMLoss = new JLabel("Max MTM Loss: ");
      lblMaxMTMLoss.setHorizontalAlignment(SwingConstants.LEFT);
      lblMaxMTMLoss.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(lblMaxMTMLoss);
    }
    {
      txtMaxMTMLoss = PriceActionUiMain.createNomberTxtField();
      txtMaxMTMLoss.setFont(new Font("Tahoma", Font.BOLD, 16));
      lblMaxMTMLoss.setLabelFor(txtMaxMTMLoss);
      contentPanel.add(txtMaxMTMLoss);
      txtMaxMTMLoss.setColumns(10);
    }
    {
      lblQuantity = new JLabel("Quantity:");
      lblQuantity.setHorizontalAlignment(SwingConstants.LEFT);
      lblQuantity.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(lblQuantity);
    }
    {
      txtQuantity = PriceActionUiMain.createIntTextField();
      lblQuantity.setLabelFor(txtQuantity);
      contentPanel.add(txtQuantity);
    }
    {
      lblNomOfTargets = new JLabel("No. of Targets: ");
      lblNomOfTargets.setHorizontalAlignment(SwingConstants.LEFT);
      lblNomOfTargets.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(lblNomOfTargets);
    }
    {
      cmbNumOfTargets = new JComboBox();
      cmbNumOfTargets.setFont(new Font("Tahoma", Font.BOLD, 16));
      lblNomOfTargets.setLabelFor(cmbNumOfTargets);
      cmbNumOfTargets.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4"}));
      contentPanel.add(cmbNumOfTargets);
    }
    {
      lblEntryPct = new JLabel("Entry % :");
      lblEntryPct.setHorizontalAlignment(SwingConstants.LEFT);
      lblEntryPct.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(lblEntryPct);
    }
    {
      txtEntryPct = PriceActionUiMain.createPercentTxtField();
      txtEntryPct.setFont(new Font("Tahoma", Font.BOLD, 16));
      lblEntryPct.setLabelFor(txtEntryPct);
      contentPanel.add(txtEntryPct);
      txtEntryPct.setColumns(10);
    }
    {
      lblCancelPct = new JLabel("Cancel % :");
      lblCancelPct.setHorizontalAlignment(SwingConstants.LEFT);
      lblCancelPct.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(lblCancelPct);
    }
    {
      txtCancelPct = PriceActionUiMain.createPercentTxtField();
      txtCancelPct.setFont(new Font("Tahoma", Font.BOLD, 16));
      lblCancelPct.setLabelFor(txtCancelPct);
      contentPanel.add(txtCancelPct);
      txtCancelPct.setColumns(10);
    }
    {
      lblintradayPositional = new JLabel("intraday/positional");
      lblintradayPositional.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(lblintradayPositional);
    }
    {
      cmbIntradayPositional = new JComboBox();
      cmbIntradayPositional.setFont(new Font("Tahoma", Font.BOLD, 16));
      cmbIntradayPositional.setModel(new DefaultComboBoxModel(new String[] {"intraday", "positional"}));
      contentPanel.add(cmbIntradayPositional);
    }
    {
      lblQuantityFromExcel = new JLabel("Quantity From Excel:");
      lblQuantityFromExcel.setHorizontalAlignment(SwingConstants.LEFT);
      lblQuantityFromExcel.setFont(new Font("Tahoma", Font.BOLD, 16));
      contentPanel.add(lblQuantityFromExcel);
    }
    {
      chbQtyFromExcel = new JCheckBox("");
      chbQtyFromExcel.setFont(new Font("Tahoma", Font.BOLD, 16));
      lblQuantityFromExcel.setLabelFor(chbQtyFromExcel);
      contentPanel.add(chbQtyFromExcel);
    }
    {
      JPanel buttonPane = new JPanel();
      buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
      getContentPane().add(buttonPane, BorderLayout.SOUTH);
      {
        JButton okButton = new JButton("OK");
        okButton.setAction(actOkExit);
        okButton.setFont(new Font("Tahoma", Font.BOLD, 16));
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);
      }
      {
        JButton cancelButton = new JButton("Cancel");
        cancelButton.setAction(actCancelExit);
        cancelButton.setFont(new Font("Tahoma", Font.BOLD, 16));
        cancelButton.setActionCommand("Cancel");
        buttonPane.add(cancelButton);
      }
    }
  }

  private class OkExitAction extends AbstractAction {
    public OkExitAction() {
      putValue(NAME, "OK");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      final FuturesSettingsModel futuresSettings = uiBinding.getFuturesSettings(FutureSegmentSettings.this);
      priceActionService.setFuturesSettings(futuresSettings);
      exitFutureSegmentSettingsDialog();
    }
  }
  private class CancelExitAction extends AbstractAction {
    public CancelExitAction() {
      putValue(NAME, "Cancel");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      exitFutureSegmentSettingsDialog();
    }
  }
  
  private void exitFutureSegmentSettingsDialog() {
    futureSegmentSettings.setVisible(false);
    futureSegmentSettings.dispose();
    futureSegmentSettings = null;
  }
  public JFormattedTextField getTxtStartTradingTime() {
    return txtStartTradingTime;
  }
  public JFormattedTextField getTxtCancelPending() {
    return txtCancelPending;
  }
  public JSpinner getSpnMaxPositions() {
    return spnMaxPositions;
  }
  public JFormattedTextField getTxtMaxMTMLoss() {
    return txtMaxMTMLoss;
  }
  public JFormattedTextField getQuantity() {
    return txtQuantity;
  }
  public JComboBox getCmbNumOfTargets() {
    return cmbNumOfTargets;
  }
  public JFormattedTextField getTxtEntryPct() {
    return txtEntryPct;
  }
  public JFormattedTextField getTxtCancelPct() {
    return txtCancelPct;
  }
  public JComboBox getCmbIntradayPositional() {
    return cmbIntradayPositional;
  }
  public JCheckBox getChbQtyFromExcel() {
    return chbQtyFromExcel;
  }
}
