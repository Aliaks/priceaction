package com.priceaction.ui;

import java.time.LocalTime;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.swing.JFormattedTextField;

import org.springframework.stereotype.Service;

import com.priceaction.model.CashSettingsModel;
import com.priceaction.model.FuturesSettingsModel;
import com.priceaction.utils.Convertors;

@Service
public class UiBinding {
  
  public CashSettingsModel getCashSettings(final CashSegmentSettings dlg) {
    final CashSettingsModel cashSettingsModel = new CashSettingsModel();
    
    map(dlg::getTxtStartTradingTime, Convertors::getTimeFieldValue, cashSettingsModel::setStartTrading);
    
    map(dlg::getTxtCancelPending, Convertors::getTimeFieldValue, cashSettingsModel::setCancelPendingOrders);
    
    Supplier<Integer> mop = () -> (Integer)dlg.getSpnMaxPositions().getValue();
    map(mop, i->i, cashSettingsModel::setMaxOpenPositions);

    map(dlg::getTxtMaxMTMLoss, Convertors::getNumField, cashSettingsModel::setMaxMtmLoss);
    
    map(dlg::getTxtRiskPerTrade, Convertors::getNumField, cashSettingsModel::setRiskPerTrade);
    
    map(()->(String)dlg.getCmbNumOfTargets().getSelectedItem(), 
        Integer::parseInt, cashSettingsModel::setNomOfTargets);
    
    map(dlg::getTxtEntryPct, Convertors::getNumField, cashSettingsModel::setEntryPercent);
    
    map(dlg::getTxtCancelPct, Convertors::getNumField, cashSettingsModel::setCancelPercent);
    
    map(()->(String)dlg.getCmbIntradayPositional().getSelectedItem(), 
        s->"intraday".equalsIgnoreCase(s), cashSettingsModel::setIntraday);
    
    map(()->dlg.getChbQtyFromExcel().isSelected(), b->b, cashSettingsModel::setQuantityFromExel);

    return cashSettingsModel;
  }

  public FuturesSettingsModel getFuturesSettings(final FutureSegmentSettings dlg) {
    final FuturesSettingsModel futuresSettingsModel = new FuturesSettingsModel();
    
    map(dlg::getTxtStartTradingTime, Convertors::getTimeFieldValue, futuresSettingsModel::setStartTrading);
    
    map(dlg::getTxtCancelPending, Convertors::getTimeFieldValue, futuresSettingsModel::setCancelPendingOrders);
    
    Supplier<Integer> mop = () -> (Integer)dlg.getSpnMaxPositions().getValue();
    map(mop, i->i, futuresSettingsModel::setMaxOpenPositions);

    map(dlg::getTxtMaxMTMLoss, Convertors::getNumField, futuresSettingsModel::setMaxMtmLoss);
    
    map(dlg::getQuantity, Convertors::getNumField, futuresSettingsModel::setRiskPerTrade);
    
    map(()->(String)dlg.getCmbNumOfTargets().getSelectedItem(), 
        Integer::parseInt, futuresSettingsModel::setNomOfTargets);
    
    map(dlg::getTxtEntryPct, Convertors::getNumField, futuresSettingsModel::setEntryPercent);
    
    map(dlg::getTxtCancelPct, Convertors::getNumField, futuresSettingsModel::setCancelPercent);
    
    map(()->(String)dlg.getCmbIntradayPositional().getSelectedItem(), 
        s->"intraday".equalsIgnoreCase(s), futuresSettingsModel::setIntraday);
    
    map(()->dlg.getChbQtyFromExcel().isSelected(), b->b, futuresSettingsModel::setQuantityFromExel);

    return futuresSettingsModel;
  }

  
  
  public static <I,O> void map (final Supplier<I> s, final Function<I,O> f, Consumer<O> c) {
    final I i = s.get();
    final O o = f.apply(i);
    c.accept(o);
  }
  
  public static <I,U> void maps (final Supplier<I> s, final Supplier<U> u, final BiConsumer<I, U> bc) {
    final I i = s.get();
    final U ui = u.get();
    bc.accept(i, ui);
  }
  
  
  public void setCashSettings(final CashSettingsModel model, final CashSegmentSettings dlg) {
    
    maps(model::getStartTrading, dlg::getTxtStartTradingTime, Convertors::setTimeField);
    
    maps(model::getCancelPendingOrders, dlg::getTxtCancelPending, Convertors::setTimeField);
    
    maps(model::getMaxOpenPositions, dlg::getSpnMaxPositions, (i, spn)->spn.setValue(i));
    
    maps(model::getMaxMtmLoss, dlg::getTxtMaxMTMLoss, Convertors::setNumField);
    
    maps(model::getRiskPerTrade, dlg::getTxtRiskPerTrade, Convertors::setNumField);
    
   // dlg.getCmbNumOfTargets().setSelectedItem(anObject);
    
    maps(model::getNomOfTargets, dlg::getCmbNumOfTargets, 
        (i, cbx)->cbx.setSelectedItem(Integer.toString(i)));
    
    maps(model::getEntryPercent, dlg::getTxtEntryPct, Convertors::setNumField);
    
    maps(model::getCancelPercent, dlg::getTxtCancelPct, Convertors::setNumField);
    
    maps(model::getIntraday, dlg::getCmbIntradayPositional, (s, cbx)->cbx.setSelectedItem(s));
    
    maps(model::getQuantityFromExel, dlg::getChbQtyFromExcel, (b, cbx)->cbx.setSelected(b));
  }
  
  public void setFuturesSettings(final FuturesSettingsModel model, final FutureSegmentSettings dlg) {
    
    maps(model::getStartTrading, dlg::getTxtStartTradingTime, Convertors::setTimeField);
    
    maps(model::getCancelPendingOrders, dlg::getTxtCancelPending, Convertors::setTimeField);
    
    maps(model::getMaxOpenPositions, dlg::getSpnMaxPositions, (i, spn)->spn.setValue(i));
    
    maps(model::getMaxMtmLoss, dlg::getTxtMaxMTMLoss, Convertors::setNumField);
    
    maps(model::getQuantity, dlg::getQuantity, (i, txt)->txt.setValue(i));
    
   // dlg.getCmbNumOfTargets().setSelectedItem(anObject);
    
    maps(model::getNomOfTargets, dlg::getCmbNumOfTargets, 
        (i, cbx)->cbx.setSelectedItem(Integer.toString(i)));
    
    maps(model::getEntryPercent, dlg::getTxtEntryPct, Convertors::setNumField);
    
    maps(model::getCancelPercent, dlg::getTxtCancelPct, Convertors::setNumField);
    
    maps(model::getIntraday, dlg::getCmbIntradayPositional, (s, cbx)->cbx.setSelectedItem(s));
    
    maps(model::getQuantityFromExel, dlg::getChbQtyFromExcel, (b, cbx)->cbx.setSelected(b));
  }

}
