package com.priceaction.ui;

public enum PaStatus {
  
  NOT_CONNECTED("Not connected"),
  CONNECTING("Connecting.."),
  CONNECTED("Connected"),
  ERROR("Error"),
  TRADING("Trading"),
  PRE_TRADING("Scheduled"),
  SQAREOFF("Square OFF"),
  CANCELING_ORDERS("Canceling Oders");
  
  private PaStatus(String st) {
    status = st;
  }
  private String status;
  
  public String getStatus() {
    return status;
  }

}
