package com.priceaction.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.MaskFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.priceaction.model.AppSettingsModel;
import com.priceaction.model.TradingCurrentData;
import com.priceaction.service.PriceActionService;
import com.priceaction.service.StateService;
import com.priceaction.service.UiLog;

import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import javax.swing.BoxLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.GridLayout;
import javax.swing.border.BevelBorder;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.CompoundBorder;
import java.awt.Component;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.JMenu;
import javax.annotation.PostConstruct;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextArea;
import javax.swing.JTabbedPane;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.JFormattedTextField;
import java.awt.Toolkit;
import java.awt.event.ItemListener;
import java.io.File;
import java.math.BigDecimal;
import java.awt.event.ItemEvent;

public class PriceActionUiMain extends JFrame {

  private JPanel contentPane;
  private JTextField txtImpFile;
  private JTextField txtConnected;
  private final Action actExit = new ExitAction();
  private final Action actApiLogin = new LoginDialog();
  private final Action actCashSettingsDlg = new CashSettingsDialog();
  private final Action actFutureSettingsDlg = new FuturesSettingDialog();
  
  public static PriceActionUiMain frame;
  private JTextField txtExelUpdate;
  private JTextField txtMTM;
  private JTextField txtSegment;
  private JTextField txtTrades;
  private JTextArea txaAppLog;
  private final Action connectAction = new ConnectAction();
  private final Action disconnectAction = new DisconnectAction();
  private final Action importAction = new ImportAction();
  
  @Autowired
  private PriceActionService priceActionService;
  
  @Autowired
  private CashSegmentSettings cashSegmentSettings;
  
  @Autowired
  private FutureSegmentSettings futuresSegmentSettings;
  
  
//  @Autowired
//  private SeqTokenInputDlg seqTokenInputDlg;
  
  @Autowired
  private StateService stateService;
  
  private JTextArea txaExelLogs;
  private JTextArea txaRqRs;
  private final Action startTradingAction = new StartTradingAction();
  private final Action saveAction = new SaveAction();
  private JButton btnConnect;
  private JButton btnDisconnect;
  private JButton btnStart;
  private JButton btnStop;
  private JButton btnUpdate;
  private JButton btnSquareOff;
  private JComboBox cmbTradeMode;
  private JButton btnSave;
  private JButton btnImport;
  
  public static void setLookAndFeel() {
    try {
      UIManager.setLookAndFeel(
          UIManager.getCrossPlatformLookAndFeelClassName()
          //UIManager.getSystemLookAndFeelClassName()
          );
    } catch (UnsupportedLookAndFeelException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  /**
   * Create the frame.
   */
  public PriceActionUiMain() {
    setIconImage(Toolkit.getDefaultToolkit().getImage(PriceActionUiMain.class.getResource("/img/spread-icon.jpg")));
    setFont(new Font("Segoe UI", Font.BOLD, 17));
    setTitle("Flash Trader - PA");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 1440, 727);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
    
    JPanel pnlMenu = new JPanel();
    contentPane.add(pnlMenu);
    pnlMenu.setLayout(new GridLayout(1, 4, 20, 20));
    
   
    JMenuBar menuBar = new JMenuBar();
    menuBar.setAlignmentX(Component.LEFT_ALIGNMENT);
    pnlMenu.add(menuBar);
    
    JMenu mnFileMenu = new JMenu("File");
    mnFileMenu.setFont(new Font("Segoe UI", Font.BOLD, 17));
    menuBar.add(mnFileMenu);
    
    JMenuItem mnitmExit = new JMenuItem("Exit");
    mnFileMenu.add(mnitmExit);
    mnitmExit.setAction(actExit);
    mnitmExit.setHorizontalAlignment(SwingConstants.LEFT);
    mnitmExit.setFont(new Font("Segoe UI", Font.BOLD, 17));
    
    JMenu mnSettingsMenu = new JMenu("Settings");
    mnSettingsMenu.setFont(new Font("Segoe UI", Font.BOLD, 17));
    menuBar.add(mnSettingsMenu);
    
    JMenuItem mnitmCashSettings = new JMenuItem("Settings: Cash Segment");
    mnSettingsMenu.add(mnitmCashSettings);
    mnitmCashSettings.setAction(actCashSettingsDlg);
    mnitmCashSettings.setFont(new Font("Segoe UI", Font.BOLD, 17));
    
    JMenuItem mnitmFuturesSettings = new JMenuItem("Settings: Futures Segment");
    mnSettingsMenu.add(mnitmFuturesSettings);
    mnitmFuturesSettings.setAction(actFutureSettingsDlg);
    mnitmFuturesSettings.setFont(new Font("Segoe UI", Font.BOLD, 17));
    
    JMenuItem mnitmApiLogin = new JMenuItem("Settings: API Loin");
    mnSettingsMenu.add(mnitmApiLogin);
    mnitmApiLogin.setAction(actApiLogin);
    mnitmApiLogin.setFont(new Font("Segoe UI", Font.BOLD, 17));
    
    Component horizontalGlue = Box.createHorizontalGlue();
    pnlMenu.add(horizontalGlue);
    
    JLabel lblCurrFile = new JLabel("File:");
    pnlMenu.add(lblCurrFile);
    lblCurrFile.setFont(new Font("Tahoma", Font.BOLD, 16));
    lblCurrFile.setHorizontalAlignment(SwingConstants.RIGHT);
    
    txtImpFile = new JTextField();
    lblCurrFile.setLabelFor(txtImpFile);
    pnlMenu.add(txtImpFile);
    txtImpFile.setFont(new Font("Tahoma", Font.BOLD, 16));
    txtImpFile.setEditable(false);
    txtImpFile.setText("some imported file");
    txtImpFile.setColumns(20);
    
    btnImport = new JButton("Import");
    btnImport.setAction(importAction);
    btnImport.setFont(new Font("Tahoma", Font.BOLD, 16));
    pnlMenu.add(btnImport);
    
    JPanel pnlActions = new JPanel();
    pnlActions.setBorder(new EmptyBorder(20, 20, 10, 10));
    contentPane.add(pnlActions);
    GridBagLayout gbl_pnlActions = new GridBagLayout();
    gbl_pnlActions.columnWidths = new int[]{164, 164, 164, 164, 164, 164, 164, 164, 0};
    gbl_pnlActions.rowHeights = new int[]{29, 0};
    gbl_pnlActions.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    gbl_pnlActions.rowWeights = new double[]{0.0, Double.MIN_VALUE};
    pnlActions.setLayout(gbl_pnlActions);
    
    btnConnect = new JButton("Connect");
    btnConnect.setEnabled(false);
    btnConnect.setAction(connectAction);
    btnConnect.setFont(new Font("Tahoma", Font.BOLD, 16));
    GridBagConstraints gbc_btnConnect = new GridBagConstraints();
    gbc_btnConnect.insets = new Insets(0, 0, 0, 5);
    gbc_btnConnect.gridx = 0;
    gbc_btnConnect.gridy = 0;
    pnlActions.add(btnConnect, gbc_btnConnect);
    
    btnDisconnect = new JButton("Disconnect");
    btnDisconnect.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
      }
    });
    btnDisconnect.setAction(disconnectAction);
    btnDisconnect.setFont(new Font("Tahoma", Font.BOLD, 16));
    GridBagConstraints gbc_btnDisconnect = new GridBagConstraints();
    gbc_btnDisconnect.insets = new Insets(0, 0, 0, 5);
    gbc_btnDisconnect.gridx = 1;
    gbc_btnDisconnect.gridy = 0;
    pnlActions.add(btnDisconnect, gbc_btnDisconnect);
    
    btnStart = new JButton("Start Trading");
    btnStart.setAction(startTradingAction);
    GridBagConstraints gbc_btnStart = new GridBagConstraints();
    gbc_btnStart.insets = new Insets(0, 0, 0, 5);
    gbc_btnStart.gridx = 2;
    gbc_btnStart.gridy = 0;
    pnlActions.add(btnStart, gbc_btnStart);
    btnStart.setFont(new Font("Tahoma", Font.BOLD, 16));
    
    btnStop = new JButton("Stop Trading");
    btnStop.setAction(stopTradingAction);
    GridBagConstraints gbc_btnStop = new GridBagConstraints();
    gbc_btnStop.insets = new Insets(0, 0, 0, 5);
    gbc_btnStop.gridx = 3;
    gbc_btnStop.gridy = 0;
    pnlActions.add(btnStop, gbc_btnStop);
    btnStop.setFont(new Font("Tahoma", Font.BOLD, 16));
    
    btnUpdate = new JButton("Update Trading");
    GridBagConstraints gbc_btnUpdate = new GridBagConstraints();
    gbc_btnUpdate.insets = new Insets(0, 0, 0, 5);
    gbc_btnUpdate.gridx = 4;
    gbc_btnUpdate.gridy = 0;
    pnlActions.add(btnUpdate, gbc_btnUpdate);
    btnUpdate.setFont(new Font("Tahoma", Font.BOLD, 16));
    
    btnSquareOff = new JButton("Square-Off");
    GridBagConstraints gbc_btnSquareOff = new GridBagConstraints();
    gbc_btnSquareOff.insets = new Insets(0, 0, 0, 5);
    gbc_btnSquareOff.gridx = 5;
    gbc_btnSquareOff.gridy = 0;
    pnlActions.add(btnSquareOff, gbc_btnSquareOff);
    btnSquareOff.setFont(new Font("Tahoma", Font.BOLD, 16));
    
    cmbTradeMode = new JComboBox(AppSettingsModel.Segment.values());
    
    cmbTradeMode.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent evt) {
        AppSettingsModel.Segment segment = (AppSettingsModel.Segment) cmbTradeMode.getSelectedItem();
        priceActionService.setSegment(segment);
        
      }
    });
    
    cmbTradeMode.setFont(new Font("Tahoma", Font.BOLD, 16));
   // cmbTradeMode.setModel(new DefaultComboBoxModel(new String[] {"Cash", "Futures"}));
    GridBagConstraints gbc_cmbTradeMode = new GridBagConstraints();
    gbc_cmbTradeMode.insets = new Insets(0, 0, 0, 5);
    gbc_cmbTradeMode.gridx = 6;
    gbc_cmbTradeMode.gridy = 0;
    pnlActions.add(cmbTradeMode, gbc_cmbTradeMode);
    
    btnSave = new JButton("Save");
    btnSave.setAction(saveAction);
    btnSave.setFont(new Font("Tahoma", Font.BOLD, 16));
    GridBagConstraints gbc_btnSave = new GridBagConstraints();
    gbc_btnSave.gridx = 7;
    gbc_btnSave.gridy = 0;
    pnlActions.add(btnSave, gbc_btnSave);
    
    JPanel pnvViews = new JPanel();
    contentPane.add(pnvViews);
    pnvViews.setLayout(new BoxLayout(pnvViews, BoxLayout.X_AXIS));
    
    JTabbedPane tbpnViews = new JTabbedPane(JTabbedPane.TOP);
    tbpnViews.setFont(new Font("Tahoma", Font.BOLD, 16));
    tbpnViews.setBorder(new EmptyBorder(20, 20, 20, 20));
    pnvViews.add(tbpnViews);
    
    JPanel pnlSummary = new JPanel();
    tbpnViews.addTab("Summary", null, pnlSummary, null);
    tbpnViews.setEnabledAt(0, true);
    GridBagLayout gbl_pnlSummary = new GridBagLayout();
    gbl_pnlSummary.columnWidths = new int[] {200, 200};
    gbl_pnlSummary.rowHeights = new int[] {30, 200};
    gbl_pnlSummary.columnWeights = new double[]{0.0, 0.0};
    gbl_pnlSummary.rowWeights = new double[]{0.0, 0.0};
    pnlSummary.setLayout(gbl_pnlSummary);
    
    JLabel lblExelLogs = new JLabel("Exel Logs");
    lblExelLogs.setFont(new Font("Tahoma", Font.BOLD, 16));
    lblExelLogs.setVerticalAlignment(SwingConstants.TOP);
    GridBagConstraints gbc_lblExelLogs = new GridBagConstraints();
    gbc_lblExelLogs.insets = new Insets(0, 0, 5, 5);
    gbc_lblExelLogs.weightx = 1.0;
    gbc_lblExelLogs.fill = GridBagConstraints.HORIZONTAL;
    gbc_lblExelLogs.gridx = 0;
    gbc_lblExelLogs.gridy = 0;
    pnlSummary.add(lblExelLogs, gbc_lblExelLogs);
    
    JLabel lblRqRs = new JLabel("Request & Response");
    lblRqRs.setFont(new Font("Tahoma", Font.BOLD, 16));
    lblRqRs.setVerticalAlignment(SwingConstants.TOP);
    GridBagConstraints gbc_lblRqRs = new GridBagConstraints();
    gbc_lblRqRs.insets = new Insets(0, 0, 5, 0);
    gbc_lblRqRs.weightx = 1.0;
    gbc_lblRqRs.fill = GridBagConstraints.HORIZONTAL;
    gbc_lblRqRs.gridx = 1;
    gbc_lblRqRs.gridy = 0;
    pnlSummary.add(lblRqRs, gbc_lblRqRs);
    
    JScrollPane scrollPane_1 = new JScrollPane();
    GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
    gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
    gbc_scrollPane_1.insets = new Insets(0, 0, 0, 5);
    gbc_scrollPane_1.gridx = 0;
    gbc_scrollPane_1.gridy = 1;
    pnlSummary.add(scrollPane_1, gbc_scrollPane_1);
    
    txaExelLogs = new JTextArea();
    txaExelLogs.setFont(new Font("Monospaced", Font.PLAIN, 14));
    scrollPane_1.setViewportView(txaExelLogs);
    lblExelLogs.setLabelFor(txaExelLogs);
    txaExelLogs.setRows(14);
    
    JScrollPane scrollPane_2 = new JScrollPane();
    GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
    gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
    gbc_scrollPane_2.gridx = 1;
    gbc_scrollPane_2.gridy = 1;
    pnlSummary.add(scrollPane_2, gbc_scrollPane_2);
    
    txaRqRs = new JTextArea();
    txaRqRs.setFont(new Font("Monospaced", Font.PLAIN, 14));
    scrollPane_2.setViewportView(txaRqRs);
    txaRqRs.setRows(14);
    
    JPanel pnlOrderBook = new JPanel();
    tbpnViews.addTab("Order Book", null, pnlOrderBook, null);
    pnlOrderBook.setLayout(new BoxLayout(pnlOrderBook, BoxLayout.Y_AXIS));
    
    JScrollPane scrollPane_3 = new JScrollPane();
    pnlOrderBook.add(scrollPane_3);
    
    JTextArea txaOrderBook = new JTextArea();
    txaOrderBook.setFont(new Font("Monospaced", Font.PLAIN, 14));
    scrollPane_3.setViewportView(txaOrderBook);
    
    JPanel pnlTradeBook = new JPanel();
    tbpnViews.addTab("Trade Book", null, pnlTradeBook, null);
    tbpnViews.setEnabledAt(2, true);
    pnlTradeBook.setLayout(new BoxLayout(pnlTradeBook, BoxLayout.Y_AXIS));
    
    JScrollPane scrollPane_4 = new JScrollPane();
    pnlTradeBook.add(scrollPane_4);
    
    JTextArea txaTradeBook = new JTextArea();
    txaTradeBook.setFont(new Font("Monospaced", Font.PLAIN, 14));
    scrollPane_4.setViewportView(txaTradeBook);
    
    JPanel pnlPositions = new JPanel();
    tbpnViews.addTab("Positions", null, pnlPositions, null);
    tbpnViews.setEnabledAt(3, true);
    pnlPositions.setLayout(new BoxLayout(pnlPositions, BoxLayout.Y_AXIS));
    
    JScrollPane scrollPane_5 = new JScrollPane();
    pnlPositions.add(scrollPane_5);
    
    JTextArea txaPositions = new JTextArea();
    txaPositions.setFont(new Font("Monospaced", Font.PLAIN, 14));
    scrollPane_5.setViewportView(txaPositions);
    
    JPanel pnlAppLog = new JPanel();
    contentPane.add(pnlAppLog);
    pnlAppLog.setLayout(new BoxLayout(pnlAppLog, BoxLayout.Y_AXIS));
    
    JLabel lblAppLog = new JLabel("Application Log");
    lblAppLog.setFont(new Font("Tahoma", Font.BOLD, 16));
    lblAppLog.setHorizontalAlignment(SwingConstants.LEFT);
    pnlAppLog.add(lblAppLog);
    
    txaAppLog = new JTextArea();
    txaAppLog.setFont(new Font("Monospaced", Font.PLAIN, 14));
    txaAppLog.setEditable(false);
    txaAppLog.setRows(10);
   // pnlAppLog.add(txaAppLog);
    
    JScrollPane scrollPane = new JScrollPane(txaAppLog);
    pnlAppLog.add(scrollPane);
    
    Component verticalGlue = Box.createVerticalGlue();
    contentPane.add(verticalGlue);
    
    JPanel pnlStatus = new JPanel();
    contentPane.add(pnlStatus);
    pnlStatus.setBorder(new EmptyBorder(10, 20, 20, 20));
    pnlStatus.setLayout(new BoxLayout(pnlStatus, BoxLayout.X_AXIS));
    
    JLabel lblApp = new JLabel("App: ");
    lblApp.setFont(new Font("Tahoma", Font.BOLD, 16));
    pnlStatus.add(lblApp);
    
    txtConnected = new JTextField();
    pnlStatus.add(txtConnected);
    txtConnected.setFont(new Font("Tahoma", Font.PLAIN, 16));
    txtConnected.setEditable(false);
    txtConnected.setText("Connected");
    txtConnected.setHorizontalAlignment(SwingConstants.LEFT);
    txtConnected.setColumns(10);
    
    Component horizontalStrut = Box.createHorizontalStrut(20);
    pnlStatus.add(horizontalStrut);
    
    Component horizontalGlue_1 = Box.createHorizontalGlue();
    pnlStatus.add(horizontalGlue_1);
    
    JLabel lblExecUpdate = new JLabel("Excel Update: ");
    lblExecUpdate.setFont(new Font("Tahoma", Font.BOLD, 16));
    pnlStatus.add(lblExecUpdate);
    
    txtExelUpdate = new JTextField();
    txtExelUpdate.setEditable(false);
    pnlStatus.add(txtExelUpdate);
    txtExelUpdate.setColumns(10);
    
    Component horizontalStrut_1 = Box.createHorizontalStrut(20);
    pnlStatus.add(horizontalStrut_1);
    
    JLabel lblMTM = new JLabel("MTM:");
    lblMTM.setFont(new Font("Tahoma", Font.BOLD, 16));
    pnlStatus.add(lblMTM);
    
    txtMTM = new JTextField();
    txtMTM.setFont(new Font("Tahoma", Font.BOLD, 16));
    txtMTM.setEditable(false);
    pnlStatus.add(txtMTM);
    txtMTM.setColumns(10);
    
    Component horizontalStrut_2 = Box.createHorizontalStrut(20);
    pnlStatus.add(horizontalStrut_2);
    
    JLabel lblSegment = new JLabel("Segment: ");
    lblSegment.setFont(new Font("Tahoma", Font.BOLD, 16));
    pnlStatus.add(lblSegment);
    
    txtSegment = new JTextField();
    txtSegment.setEditable(false);
    txtSegment.setFont(new Font("Tahoma", Font.BOLD, 16));
    pnlStatus.add(txtSegment);
    txtSegment.setColumns(10);
    
    Component horizontalStrut_3 = Box.createHorizontalStrut(20);
    pnlStatus.add(horizontalStrut_3);
    
    JLabel lblTrades = new JLabel("Trades: ");
    lblTrades.setFont(new Font("Tahoma", Font.BOLD, 16));
    pnlStatus.add(lblTrades);
    
    txtTrades = new JTextField();
    txtTrades.setEditable(false);
    txtTrades.setFont(new Font("Tahoma", Font.BOLD, 16));
    pnlStatus.add(txtTrades);
    txtTrades.setColumns(10);
    
    Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
    pnlStatus.add(rigidArea);
    
    final Dimension maximumSize = new Dimension(1500, 20);
    pnlStatus.setMaximumSize(maximumSize);
  }

  public JTextArea getTxaAppLog() {
    return txaAppLog;
  }
   
  //-------------------------- Sate tranzition handlers ----------
  
  private void onNotConnected(final StateService.State from, final StateService.State to) {
    uiLog("onNotConnected");
    setEnability(notConnectedEnabled);
    dispalayStatus(PaStatus.NOT_CONNECTED);
  }
  
  private void onConnecting(final StateService.State from, final StateService.State to) {
    uiLog("onConnecting");
    setEnability(connectingEnabled);
    dispalayStatus(PaStatus.CONNECTING);
  }
  
  private void onConnected(final StateService.State from, final StateService.State to) {
    uiLog("onConnected");
    setEnability(connectedEnabled);
    btnStart.setEnabled(priceActionService.isExcelDataImported());
    dispalayStatus(PaStatus.CONNECTED);
  }
  
  private void onPreTrating(final StateService.State from, final StateService.State to) {
    uiLog("onPreTrating");
    setEnability(tradingStartedEnabled);
    dispalayStatus(PaStatus.PRE_TRADING);
  }
  
  
   private void onTrating(final StateService.State from, final StateService.State to) {
     uiLog("onTrating");
     setEnability(tradingStartedEnabled);
     dispalayStatus(PaStatus.TRADING);
  }
   
   private void onTradingDataUpdate(final TradingCurrentData td) {
     txtTrades.setText(Integer.toString(td.tradesNumber));
   }
  
  @PostConstruct
  public void init() {
    
    allControlableComponents = Arrays.asList(btnConnect, btnSave, btnImport
        , btnDisconnect, btnStart, btnStop, btnSquareOff, cmbTradeMode);
    
    notConnectedEnabled = Arrays.asList(btnConnect, btnSave, btnImport, cmbTradeMode);
    
    connectedEnabled = Arrays.asList(btnSave, btnDisconnect, btnStart, cmbTradeMode, btnImport);
    
    tradingStartedEnabled = Arrays.asList(btnSave, btnStop);
    
    connectingEnabled = Arrays.asList( btnSave, cmbTradeMode);  
    
    stateService.subscribeUiActionOnState(StateService.State.NOT_CONNECTED, this::onNotConnected);
    
    stateService.subscribeUiActionOnState(StateService.State.CONNECTING, this::onConnecting);
    
    stateService.subscribeUiActionOnState(StateService.State.CONNECTED, this::onConnected);
    
    stateService.subscribeUiActionOnState(StateService.State.PRE_TRADING, this::onPreTrating);
    
    stateService.subscribeUiActionOnState(StateService.State.TRADING, this::onTrating);
    
    stateService.subscrideOnTradingUpdate(this::onTradingDataUpdate);
    
    setEnability(notConnectedEnabled);
    setStatusPane();
    dispalayStatus(PaStatus.NOT_CONNECTED);
  }
  
  //-------------------------------- Actions ---------------------
  
  
  private class ExitAction extends AbstractAction {
    public ExitAction() {
      putValue(NAME, "Exit");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      frame.setVisible(false);
      frame.dispose();
    }
  }
  private class LoginDialog extends AbstractAction {
    public LoginDialog() {
      putValue(NAME, "API Login");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      ApiLogin.startApiLoginDialog();
    }
  }
  private class CashSettingsDialog extends AbstractAction {
    public CashSettingsDialog() {
      putValue(NAME, "Cash Settings");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      CashSegmentSettings.startCashSegmentsSettings(cashSegmentSettings);
    }
  }
  private class FuturesSettingDialog extends AbstractAction {
    public FuturesSettingDialog() {
      putValue(NAME, "Futures Setting");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      FutureSegmentSettings.startFuturesSettingsDialog(futuresSegmentSettings);
    }
  }
  private class ConnectAction extends AbstractAction {
    public ConnectAction() {
      putValue(NAME, "Connect");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      //priceActionService.doLoginAsinc();
      //priceActionService.doLoginFake();
      priceActionService.doLogin();
    }
  }
  private class DisconnectAction extends AbstractAction {
    public DisconnectAction() {
      putValue(NAME, "Disconnect");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      uiLog("Disconnecting...");
      priceActionService.doLogout();
    }
  }
  private class ImportAction extends AbstractAction {
    private String absolutePath = ".";
    public ImportAction() {
      putValue(NAME, "Import");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      uiLog("Importing...");
      
      JFileChooser chooser = new JFileChooser(absolutePath);
      FileNameExtensionFilter filter = new FileNameExtensionFilter(
          "Coma Separated Values", "csv");
      chooser.setFileFilter(filter);
      int returnVal = chooser.showOpenDialog(frame);
      if(returnVal == JFileChooser.APPROVE_OPTION) {
         final String selFile = chooser.getSelectedFile().getName();
         frame.txtImpFile.setText(selFile);
        absolutePath = chooser.getSelectedFile().getAbsolutePath();
        uiLog("Imported file: " + absolutePath);
        priceActionService.importExData(absolutePath);
      }
    }
  }
  /**
   * @wbp.factory
   */
  public static JFormattedTextField createTimeFormattedTxtField() {
    JFormattedTextField formattedTextField = new JFormattedTextField(createFormatter("##:##:##"));
    formattedTextField.setFont(new Font("Tahoma", Font.BOLD, 16));
    formattedTextField.setText("13:45:00");
    formattedTextField.setColumns(10);
    return formattedTextField;
  }
  
  private static MaskFormatter createFormatter(String s) {
    MaskFormatter formatter = null;
    try {
        formatter = new MaskFormatter(s);
    } catch (java.text.ParseException exc) {
      uiLog(exc.getMessage());
    }
    return formatter;
}

  
  public static void uiLog(final String txt) {
    frame.txaAppLog.append(LocalDateTime.now() + ": " + txt + "\n");
  }
  

  /**
   * @wbp.factory
   */
  public static JFormattedTextField createNomberTxtField() {
    final NumberFormat numberInstance = NumberFormat.getNumberInstance();
    numberInstance.setMinimumFractionDigits(0);
    JFormattedTextField formattedTextField = new JFormattedTextField(numberInstance);
    formattedTextField.setColumns(10);
    return formattedTextField;
  }
  /**
   * @wbp.factory
   */
  public static JFormattedTextField createPercentTxtField() {
    final NumberFormat percentInstance = NumberFormat.getNumberInstance();//NumberFormat.getPercentInstance();
    percentInstance.setMinimumFractionDigits(2);
    JFormattedTextField formattedTextField = new JFormattedTextField(percentInstance);
    formattedTextField.setFont(new Font("Tahoma", Font.BOLD, 16));
    formattedTextField.setColumns(10);
    return formattedTextField;
  }
  /**
   * @wbp.factory
   */
  public static JFormattedTextField createIntTextField() {
    final NumberFormat integerInstance = NumberFormat.getIntegerInstance();
    JFormattedTextField formattedTextField = new JFormattedTextField(integerInstance);
    formattedTextField.setFont(new Font("Tahoma", Font.BOLD, 16));
    formattedTextField.setColumns(10);
    return formattedTextField;
  }
  public JTextArea getTxaExelLogs() {
    return txaExelLogs;
  }
  public JTextArea getTxaRqRs() {
    return txaRqRs;
  }
  private class StartTradingAction extends AbstractAction {
    public StartTradingAction() {
      putValue(NAME, "Start Trading");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      priceActionService.doStartTrading();
    }
  }
  private class SaveAction extends AbstractAction {
    public SaveAction() {
      putValue(NAME, "Save");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      priceActionService.saveAppSettings();
    }
  }
  public JButton getBtnConnect() {
    return btnConnect;
  }
  protected JButton getBtnDisconnect() {
    return btnDisconnect;
  }
  protected JButton getBtnStart() {
    return btnStart;
  }
  protected JButton getBtnStop() {
    return btnStop;
  }
  protected JButton getBtnUpdate() {
    return btnUpdate;
  }
  protected JButton getBtnSquareOff() {
    return btnSquareOff;
  }
  protected JComboBox getCmbTradeMode() {
    return cmbTradeMode;
  }
  protected JButton getBtnSave() {
    return btnSave;
  }
  protected JButton getBtnImport() {
    return btnImport;
  }
  
  private List<JComponent> allControlableComponents;
  
  private List<JComponent> notConnectedEnabled;
  
  private List<JComponent> connectedEnabled;
  
  private List<JComponent> connectingEnabled;
  
  private List<JComponent> tradingStartedEnabled; 
  private final Action stopTradingAction = new StopTradingAction();
  
  private void setEnability(final List<JComponent> enabledList) {
    enabledList.forEach(jc -> jc.setEnabled(true));
    final List<JComponent> disnabledList = new ArrayList<JComponent>(allControlableComponents);
    disnabledList.removeAll(enabledList);
    disnabledList.forEach(jc -> jc.setEnabled(false));
  }
  
  private void setStatusPane() {
    AppSettingsModel.Segment segment = priceActionService.getAppSettingsModel().currentSegment;
    final BigDecimal maxMtmLoss = priceActionService.getAppSettingsModel().getMaxMtmLoss();
    txtMTM.setText(maxMtmLoss.toPlainString());
    txtSegment.setText(segment.toString());
    
    doCmbSilentRunable(cmbTradeMode, () -> cmbTradeMode.setSelectedItem(segment));
    
    final Integer nomOfTargets = priceActionService.getAppSettingsModel().getNomOfTargets();
    txtTrades.setText(nomOfTargets.toString());
    if (!priceActionService.isExcelDataImported()) {
      txtExelUpdate.setText("Not Imported");
    } else {
      final int recordsNum = priceActionService.getExDataImport().size();
      txtExelUpdate.setText(recordsNum + " records");
      final String importedFile = priceActionService.getAppSettingsModel().importedFile;
      String fileName = new File(importedFile).getName();
      txtImpFile.setText(fileName);
    }
  }
  
  private void dispalayStatus(final PaStatus status) {
    txtConnected.setText(status.getStatus());
    setStatusPane();
  }

  private class StopTradingAction extends AbstractAction {
    public StopTradingAction() {
      putValue(NAME, "Stop Tradin");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }
    public void actionPerformed(ActionEvent e) {
      priceActionService.doStopTrading();
    }
  }
  
  
  public static void doCmbSilentRunable(final JComboBox component, final Runnable f) {
    final ActionListener[] actionListeners = component.getActionListeners();
    for (final ActionListener listener : actionListeners)
        component.removeActionListener(listener);
    try {
        f.run();
    } finally {
        for (final ActionListener listener : actionListeners)
            component.addActionListener(listener);
    }
}
  
}

