package com.priceaction.app.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.swing.JFormattedTextField;
import javax.swing.text.NumberFormatter;

public class Convertors {
  
  public static Function<JFormattedTextField, LocalTime> timeFieldToLocalTime = 
      new Function<JFormattedTextField, LocalTime>() {
        @Override
        public LocalTime apply(JFormattedTextField t) {
          return getTimeFieldValue(t);
        }

      };

      /*
      public static Function<LocalTime, Consumer<JFormattedTextField>> localTimeToTimeField = 
          new Function<LocalTime, Consumer<JFormattedTextField>>() {
            @Override
            public Consumer<JFormattedTextField> apply(LocalTime t) {
              DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
              final String strTime = t.format(formatter);
              return new Consumer<JFormattedTextField>() {
                @Override
                public void accept(JFormattedTextField t) {
                  t.setValue(strTime);
                }
              };
            }
          };

      public void convert (final LocalTime t, final JFormattedTextField f) {
        localTimeToTimeField.apply(t).accept(f);
      }
      */

  public static final BiConsumer<LocalTime, JFormattedTextField> localTimeToTimeField = new BiConsumer<LocalTime, JFormattedTextField>() {
    @Override
    public void accept(LocalTime t, JFormattedTextField u) {
      setTimeField(t, u);
    }
  };

  public static void setTimeField(LocalTime t, JFormattedTextField u) {
    DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
    final String strTime = t.format(formatter);
    u.setValue(strTime);
  }
  
  public static LocalTime getTimeFieldValue(JFormattedTextField t) {
    final String txtTime = t.getText();
    return LocalTime.parse(txtTime);
  }

  public static void setNumField(BigDecimal t, JFormattedTextField u) {
    u.setValue(t.doubleValue());
  }
  
  public static BigDecimal getNumField(JFormattedTextField t) {
    final String txtPrct = t.getText();
    return new BigDecimal(txtPrct);
  }
}
