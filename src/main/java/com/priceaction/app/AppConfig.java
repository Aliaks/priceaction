package com.priceaction.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.priceaction.client.xts.MarketSymboleParser.SymbolData;

//@PropertySource(value = {"price-action.properties"})
@Component
public class AppConfig {  
  
  public final static String APP_MODEL_FILE = "pa.json";
  
  public final static String APP_CONFIG_FILE = "price-action.properties";
  
  @Value("${arrow.api.url}")
  private String baseArrowUrl = "https://api.tradejini.com/2.0";
  
  private String requestToken;
  
  private String sessId;
  
  @Value("${arrow.api.subquery}")
  private String securitySubQuery;
  
  private final static String defaultConfig =""
      + "arrow.api.url=https://api.tradejini.com/2.0\n"
      + "arrow.api.key=7cfcfb112abc5661d8d63ceae2a29784\n"
      + "arrow.api.secret=155a5db5b6f70f968ff7c664d3433e19\n"
      + "arrow.api.clientId=TP0694\n"
      + "arrow.api.subquery=\n"
      + "xts.api.interactive.userid=PRADEEP\n"
      + "xts.api.interactive.password=Pn@12345\n"
      + "xts.api.interactive.publicKey=5cdb50bb34cb5574\n"
      + "xts.api.interactive.source=WebAPI\n"
      + "xts.api.interactive.brokername=D2\n"
      + "xts.api.interactive.url=http://103.69.169.10:10521/interactive\n"
      + "xts.api.market.userid=PRADEEP\n"
      + "xts.api.market.password=Pn@12345\n"
      + "xts.api.market.publicKey=584df31575495516\n"
      + "xts.api.market.source=WebAPI\n"
      + "xts.api.market.brokername=D2\n"
      + "xts.api.market.url=http://103.69.169.10:10521/marketdata\n"
      + "xts.api.market,symbols.url=http://103.69.169.10:10521/marketdata/instruments/master\n"
      + "";
  
  /*

   xts creds:
   UI Login:
   
   Client Name: Pradeep Nazre

    User ID: PRADEEPNZ
    
    Login pass: Pass!@12
    
    Tranz pass: Pass12!@
    
    first five questions: aa
-------------------------------------------

Interactive API
Broker Name D2
User ID PRADEEP
Password  Pn@12345
Trading Key 5cdb50bb34cb5574
URL to Connect: http://103.69.169.10:10521/interactive

Interactive API Document :
https://symphonyfintech.com/xts-trading-front-end-api/  



Market Data API
Broker Name D2
User ID PRADEEP  
Password  Pn@12345
MarketData Key  584df31575495516
URL to Connect: http://103.69.169.10:10521/marketdata  

MarketData API Document :
https://symphonyfintech.com/xts-market-data-front-end-api/  




   
   */
  
  public static boolean initializationError = false;

  @Value("${arrow.api.key}")
  private String apiKey;

  @Value("${arrow.api.secret}")
  private String secret;

  @Value("${arrow.api.clientId}")
  private String clientId;
  
  
  
 //--------------- xts --------------------
//"xts.api.interactive.userid=PRADEEPNZ\n"  
  @Value("${xts.api.interactive.userid}")
  private String xtsInteractiveUserId;
  
  //"xts.api.interactive.password=Pn@12345\n"
  @Value("${xts.api.interactive.password}")
  private String xtsInteractivePassword;
  
  
  //"xts.api.interactive.publicKey=5cdb50bb34cb5574\n"
  @Value("${xts.api.interactive.publicKey}")
  private String xtsInteractivePublicKey;
  
  
  //"xts.api.interactive.source=WebAPI\n"
  @Value("${xts.api.interactive.source}")
  private String xtsInteractiveSource;
  
  //"xts.api.interactive.brokername=D2\n"
  @Value("${xts.api.interactive.brokername}")
  private String xtsInteractiveBrokername;
  
  //"xts.api.interactive.url=http://103.69.169.10:10521/interactive\n"
  @Value("${xts.api.interactive.url}")
  private String xtsInteractiveUrl;
  
  //"xts.api.market.userid=PRADEEP\n"
  @Value("${xts.api.market.userid}")
  private String xtsMarketUsedId;
  
  //"xts.api.market.password=Pn@12345\n"
  @Value("${xts.api.market.password}")
  private String xtsMarketPassword;
  
  //"xts.api.market.publicKey=5cdb50bb34cb5574\n"
  @Value("${xts.api.market.publicKey}")
  private String xtsMarketPublicKey;
  
  //"xts.api.market.source=WebAPI\n"
  @Value("${xts.api.market.source}")
  private String xtsMarketSource;
  
  //"xts.api.market.brokername=D2\n"
  @Value("${xts.api.market.brokername}")
  private String xtsMarketBrokername;
  
  //"xts.api.market.url=http://103.69.169.10:10521/interactive\n"
  @Value("${xts.api.market.url}")
  private String xtsMarketUrl;
  
  //http://103.69.169.10:10521/marketdata/instruments/master
  @Value("${xts.api.market.symbols.url}")
  private String marketSymbolsDataUrl;
  
  private String xtsInteractiveSeaaionToken;
  
  public Map<String, SymbolData> getSymbolsData() {
    return symbolsData;
  }

  public void setSymbolsData(Map<String, SymbolData> symbolsData) {
    this.symbolsData = symbolsData;
  }

  private String xtsMarketSeaaionToken;
  
  private Map<String, SymbolData> symbolsData = null;
  
  
  public String getMarketSymbolsDataUrl() {
    return marketSymbolsDataUrl;
  }
  
  public String getXtsInteractiveSeaaionToken() {
    return xtsInteractiveSeaaionToken;
  }

  public void setXtsInteractiveSeaaionToken(String xtsInteractiveSeaaionToken) {
    this.xtsInteractiveSeaaionToken = xtsInteractiveSeaaionToken;
  }

  public String getXtsMarketSeaaionToken() {
    return xtsMarketSeaaionToken;
  }

  public void setXtsMarketSeaaionToken(String xtsMarketSeaaionToken) {
    this.xtsMarketSeaaionToken = xtsMarketSeaaionToken;
  }

  private static void createDefaultConfig() {
    if (Files.exists(Paths.get(AppConfig.APP_CONFIG_FILE))) {
      return;
    }
    try {
      Files.write(Paths.get(AppConfig.APP_CONFIG_FILE), defaultConfig.getBytes());
    } catch (IOException e) {
      initializationError = true;
      e.printStackTrace();
    }
    
  }
  
  static {
    createDefaultConfig();
  }
  
  
  public String getSecuritySubQuery() {
    return securitySubQuery;
  }

  public void setSecuritySubQuery(String securitySubQuery) {
    this.securitySubQuery = securitySubQuery;
  }

  public String getRequestToken() {
    return requestToken;
  }

  public void setRequestToken(String requestToken) {
    this.requestToken = requestToken;
  }

  public String getSessId() {
    return sessId;
  }

  public void setSessId(String sessId) {
    this.sessId = sessId;
  }

  public String getUserLoginRequestUrl() {
    return baseArrowUrl + "/userlogin-request";
  }
  
  public String getUserLoginResponseUrl() {
    return baseArrowUrl + "/userlogin-response";
  }
  
  public String getAccessTokenUrl() {
    return baseArrowUrl + "/getaccesstoken";
  }
  
  public String getDefaultLoginUrl() {
    return baseArrowUrl + "/DefaultLogin";
  }
  
  public String getAccountInfoUrl() {
    return baseArrowUrl + "/AccountInfo";
  }
  
  public String getMarketWatchNamesUrl() {
    return baseArrowUrl + "/MWList";
  }
  
  public String getMarketWatchUrl() {
    return baseArrowUrl + "/MarketWatch";
  }
  
  public String getAddScripsToMWUrl() {
    return baseArrowUrl + "/AddScripsToMW";
  }
  
  
  public String getXtsInteractiveUserId() {
    return xtsInteractiveUserId;
  }

  public String getXtsInteractivePassword() {
    return xtsInteractivePassword;
  }

  public String getXtsInteractivePublicKey() {
    return xtsInteractivePublicKey;
  }

  public String getXtsInteractiveSource() {
    return xtsInteractiveSource;
  }

  public String getXtsInteractiveUrl() {
    return xtsInteractiveUrl;
  }

  public String getXtsMarketUsedId() {
    return xtsMarketUsedId;
  }

  public String getXtsMarketPassword() {
    return xtsMarketPassword;
  }

  public String getXtsMarketPublicKey() {
    return xtsMarketPublicKey;
  }

  public String getXtsMarketSource() {
    return xtsMarketSource;
  }

  public String getXtsMarketUrl() {
    return xtsMarketUrl;
  }

  public String getDeleteMWScripsUrl() {
    return baseArrowUrl + "/DeleteMWScrips";
  }
  
  
  //------------------------------- XTS ---------------------------
  
  public String getXtsInteractiveLoginUrl() {
    return xtsInteractiveUrl + "/user/session"; 
  }
  
  public String getXtsInteractiveOrderUrl() {
    return xtsInteractiveUrl + "/orders"; 
  }
  
  public String getXtsMarketLoginUrl() {
    return xtsMarketUrl + "/auth/login"; 
  }
  
  public String getXtsMarketQuoteUrl() {
    return xtsMarketUrl + "/instruments/quotes"; 
  }
  
  public String getXtsMarketSubscriptionUrl() {
    return xtsMarketUrl + "/instruments/subscription"; 
  }
  
  
  /*
   
   Cliend ID -  TP0694
Password - one001!
APIkey - 7cfcfb112abc5661d8d63ceae2a29784
secretkey - 155a5db5b6f70f968ff7c664d3433e19
   
   */
  
  public String getApiKey() {
  //  apiKey = "7cfcfb112abc5661d8d63ceae2a29784";
    return apiKey;
  }
  
  public String getSecret() {
  //  secret = "155a5db5b6f70f968ff7c664d3433e19";
    return secret;
  }
  
  public String getClientId() {
//    clientId = "TP0694";
    return clientId;
  }
  
}
