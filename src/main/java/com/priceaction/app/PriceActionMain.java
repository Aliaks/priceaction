package com.priceaction.app;

import java.awt.EventQueue;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.priceaction.ui.PriceActionUiMain;

public class PriceActionMain {

  public static void main(String[] args) {
    AnnotationConfigApplicationContext context = 
        new AnnotationConfigApplicationContext(PriceActionConfig.class);
    
    for (String beanName : context.getBeanDefinitionNames()) {
      System.out.println(beanName);
  }
    
    
    final PriceActionUiMain mainUi = context.getBean(PriceActionUiMain.class);
    PriceActionUiMain.frame = mainUi;
    EventQueue.invokeLater(new Runnable() {      
      public void run() {
        try {
          mainUi.setLookAndFeel();
          
          mainUi.setVisible(true);
          PriceActionUiMain.uiLog("Flash Trader - PA started");
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
    
  }
  

}
