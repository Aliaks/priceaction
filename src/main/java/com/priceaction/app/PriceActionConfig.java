package com.priceaction.app;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.priceaction.service.UiLog;
import com.priceaction.ui.CashSegmentSettings;
import com.priceaction.ui.FutureSegmentSettings;
import com.priceaction.ui.PriceActionUiMain;
import com.priceaction.ui.SeqTokenInputDlg;

@Configuration
@ComponentScan(basePackages = {"com.priceaction"})
//@PropertySource("file:${app.home}/app.properties")
@PropertySource("file:./price-action.properties")
public class PriceActionConfig {
  
  @Bean
  public CashSegmentSettings cashSegmentSettings() {
    return new CashSegmentSettings();
  }
  
  @Bean
  public FutureSegmentSettings futuresSegmentSettings() {
    return new FutureSegmentSettings();
  }
  
  @Bean
  public SeqTokenInputDlg seqTokenInputDlg() {
    return new SeqTokenInputDlg();
  }
    
  @Bean
  public PriceActionUiMain priceActionUiMain() {
    final PriceActionUiMain priceActionUiMain = new PriceActionUiMain();
    return priceActionUiMain;
  }
  
  @Bean
  public ObjectMapper objectMapper() {
    // Up to Jackson 2.9: (but not with 3.0)
       final ObjectMapper mapper = new ObjectMapper()
          .registerModule(new ParameterNamesModule())
          .registerModule(new Jdk8Module())
          .registerModule(new JavaTimeModule()); // new module, NOT JSR310Module
       return mapper;

     }

  
  @Bean
  public RestTemplate arrowRestTemplete() {
    final RestTemplate restTemplate = new RestTemplate();
    restTemplate.setErrorHandler(new XtsErrorHandler());
    
    return restTemplate;
    }
  

//To resolve ${} in @Value
  @Bean
  public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
    return new PropertySourcesPlaceholderConfigurer();
  }
  
 // public static class XtsErrorHandler implements ResponseErrorHandler {
  
  public static class XtsErrorHandler extends DefaultResponseErrorHandler {
    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
      final InputStream body = response.getBody();
      body.available();
    }

    /*
    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
      return false; //response.
    }
    */
  }
  
}
