package com.priceaction.testclnt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class TestClient {
  
  private List<SymbolInfo> tradingSymbols = Arrays.asList(
      new SymbolInfo("AAA", 100.0, 3.0),
      new SymbolInfo("BBB", 100.0, 3.0),
      new SymbolInfo("AAA", 100.0, 3.0),
      new SymbolInfo("CCC", 100.0, 3.0),
      new SymbolInfo("DDD", 100.0, 3.0),
      new SymbolInfo("EEE", 100.0, 3.0),
      new SymbolInfo("FFF", 100.0, 3.0),
      new SymbolInfo("GGG", 100.0, 3.0),
      new SymbolInfo("LLL", 100.0, 3.0),
      new SymbolInfo("MMM", 100.0, 3.0),
      new SymbolInfo("NNN", 100.0, 3.0),
      new SymbolInfo("OOO", 100.0, 3.0),
      new SymbolInfo("PPP", 100.0, 3.0),
      new SymbolInfo("RRR", 100.0, 3.0),
      new SymbolInfo("SSS", 100.0, 3.0),
      new SymbolInfo("TTT", 100.0, 3.0),
      new SymbolInfo("VVV", 100.0, 3.0),
      new SymbolInfo("WWW", 100.0, 3.0),
      new SymbolInfo("XXX", 100.0, 3.0),
      new SymbolInfo("YYY", 100.0, 3.0),
      new SymbolInfo("AA1", 100.0, 3.0),
      new SymbolInfo("CC1", 100.0, 3.0),
      new SymbolInfo("DD1", 100.0, 3.0),
      new SymbolInfo("EE1", 100.0, 3.0),
      new SymbolInfo("ZZZ", 100.0, 3.0)
      );

  private void openTrade() {
    final Random rn = new Random();
    tradingSymbols.forEach(ts-> {
      ts.open(rn);
    });
  }
  
  private void updateTrade() {
    final Random rn = new Random();
    tradingSymbols.forEach(ts-> {
      final boolean bought = rn.nextBoolean();
      if (bought) {
        ts.update(rn);
      }
    });
  }
  
}
