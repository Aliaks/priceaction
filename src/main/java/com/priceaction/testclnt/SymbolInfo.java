package com.priceaction.testclnt;

import java.util.Random;

public class SymbolInfo {
  public String sympol;
  
  public double meanPrice;
  
  public double deviation;
  
  public SymbolInfo(String sympol, double meanPrice, double deviation) {
    super();
    this.sympol = sympol;
    this.meanPrice = meanPrice;
    this.deviation = deviation;
  }

  public double lastTraderPrice;
  
  public double openPrice;
  
  private double generateLastTraderPrice(final Random rdm) {
    double newPrice = rdm.nextGaussian() * deviation + meanPrice;
    return newPrice;
  }
  
  public void open(final Random rdm) {
    openPrice = generateLastTraderPrice(rdm);
  }
  
  public void update(final Random rdm) {
    lastTraderPrice = generateLastTraderPrice(rdm);
  }
  

}
