package com.priceaction.testclnt;

public enum TransactionType {
  SHORT("Short"),
  BUY("Buy");
  
  private final String typeName;
  
  TransactionType(final String name) {
    typeName = name;
  }
  
  public String getName() {
    return typeName;
  }
  
  public static TransactionType valueOfString (final String name) {
    //values()
    for (TransactionType t: values()) {
      if (t.typeName.equalsIgnoreCase(name)) {
        return t;
      }
    }
    return null;
  }
    
}