package com.priceaction.utils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Service;

@Service
public class SignatureHelper {
  
  private String getChecksum (final String apiKey, final String apiSecret) {
    return apiKey + apiSecret;
  }
  
  private String hashHMac(final String data, final String secretKey) {
   // String mykey = secret;
   // String test = "test";
    
    final StringBuilder builder = new StringBuilder();
    
    try {
      final String HMAC_SHA512 = "HmacSHA512";
        Mac mac = Mac.getInstance(HMAC_SHA512);
        SecretKeySpec secret = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA512);
        mac.init(secret);
        byte[] digest = mac.doFinal(data.getBytes());
        
        for (byte b : digest) {
          System.out.format("%02x", b);
          builder.append(String.format("%02x", b));
      }
        
        //String enc = new String(digest);
        //System.out.println(enc);  
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
    return builder.toString();
  }
  
  public String getSignature(final String apiKey, final String apiSecret, final String sessionId) {
    final String checksum = getChecksum(apiKey, apiSecret);
    return hashHMac(checksum, sessionId);
  }

}
