package com.priceaction.client.xts;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.priceaction.service.UiLog.RqRsUiLog;



//Name is the symbol name 
//In cash, the header "series", in this we only need EQ
//NSECM

//EQ is a type of series in NSECM, we only trade EQ. EQ means equities. 
//If u check the output file and search for EQ, u will find stocks with eq series



/*
 0      1   2    3          4          5
NSECM|12698|8|RAJSREESUG|RAJSREESUG-BL|BL|RAJSREESUG-BL|1100100012698|17.35|17.05|92108.3136|0.05|1\n
NSECM|12290|8|CENTENKA|CENTENKA-BL|BL|CENTENKA-BL|1100100012290|176.05|172.65|56352.66903099999|0.05|1\n
NSECM|18518|8|UTFRMP25P4|UTFRMP25P4-MF|MF|UTFRMP25P4-MF|1100100018518|11|9|686|0.01|1\n
NSECM|14317|8|TTL|TTL-BL|BL|TTL-BL|1100100014317|35.75|35.05|70298.6235|0.05|1\n
NSECM|1362|8|BPFM394SM1|BPFM394SM1-MF|MF|BPFM394SM1-MF|1100100001362|11|9|817|0.01|1\n
NSECM|4772|8|DIAPOWER|DIAPOWER-BZ|BZ|DIAPOWER-BZ|1100100004772|0.8|0.7|8819539|0.05|1\n

0        1  2   3            4                  5      6                     7
NSEFO|62901|2|BATAINDIA|BATAINDIA19OCT1580PE|OPTSTK|BATAINDIA-OPTSTK|2930400062901|197.45|56.65|33001|0.05|550|1100100000371||2019-10-31T14:30:00|1580|4\n
NSEFO|59429|2|APOLLOTYRE|APOLLOTYRE19OCT210PE|OPTSTK|APOLLOTYRE-OPTSTK|2930400059429|62.75|27.85|180001|0.05|3000|1100100000163||2019-10-31T14:30:00|210|4\n
NSEFO|57593|2|ADANIENT|ADANIENT19OCT105CE|OPTSTK|ADANIENT-OPTSTK|2930400057593|47.65|18.15|280001|0.05|4000|1100100000025||2019-10-31T14:30:00|105|3\n
NSEFO|61327|2|BAJFINANCE|BAJFINANCE19OCT3500CE|OPTSTK|BAJFINANCE-OPTSTK|2930400061327|428|27.1|12501|0.05|250|1100100000317||2019-10-31T14:30:00|3500|3\n
NSEFO|35844|2|TORNTPHARM|TORNTPHARM19OCT2060CE|OPTSTK|TORNTPHARM-OPTSTK|2930400035844|35.5|0.05|30001|0.05|500|1100100003518||2019-10-31T14:30:00|2060|3\n
NSEFO|65273|2|BHEL|BHEL19OCT79CE|OPTSTK|BHEL-OPTSTK|2930400065273|3.3|0.05|600001|0.05|7500|1100100000438||2019-10-31T14:30:00|79|3\n
NSEFO|90882|2|MOTHERSUMI|MOTHERSUMI19OCT97.5CE|OPTSTK|MOTHERSUMI-OPTSTK|2930400090882|23.25|2.45|264001|0.05|3300|1100100004204||2019-10-31T14:30:00|97.5|3\n
NSEFO|97641|2|SRTRANSFIN|SRTRANSFIN19OCT1280CE|OPTSTK|SRTRANSFIN-OPTSTK|2930400097641|60.25|0.05|36001|0.05|600|1100100004306||2019-10-31T14:30:00|1280|3\n
NSEFO|96871|2|SHREECEM|SHREECEM19OCT16500CE|OPTSTK|SHREECEM-OPTSTK|2930400096871|3974.2|1591.6|2501|0.05|50|1100100003103||2019-10-31T14:30:00|16500|3\n
NSEFO|72380|2|EQUITAS|EQUITAS19OCT95PE|OPTSTK|EQUITAS-OPTSTK|2930400072380|8.15|0.55|320001|0.05|4000|1100100016852||2019-10-31T14:30:00|95|4\n
NSEFO|83007|2|INFRATEL|INFRATEL19OCT360CE|OPTSTK|INFRATEL-OPTSTK|2930400083007|3.2|0.05|140001|0.05|2000|1100100029135||2019-10-31T14:30:00|360|3\n
NSEFO|94510|2|PIDILITIND|PIDILITIND19OCT960PE|OPTSTK|PIDILITIND-OPTSTK|2930400094510|3.1|0.05|35001|0.05|500|1100100002664||2019-10-31T14:30:00|960|4



ExchangeSegment|ExchangeInstrumentID|InstrumentType|Name|Description|Series|NameWithSeries
|InstrumentID|PriceBand.High|PriceBand.Low|FreezeQty|TickSize|LotSize

*/

// If  InstrumentType is Futures, Options & Spread enums value then use below header format 
//
// ExchangeSegment|ExchangeInstrumentID|InstrumentType|Name|Description|Series
//|NameWithSeries|InstrumentID|PriceBand.High|PriceBand.Low|FreezeQty|TickSize|LotSize
//|UnderlyingInstrumentId|UnderlyingIndexName|ContractExpiration|StrikePrice|OptionType


@Service
public class MarketSymboleParser {

  public static final String CASH_SEGMENT = "NSECM";
  
  public static final String FUTURES_SEGMENT = "NSEFO";

  private final static int SEGMENT_IDX = 0;
  
  private final static int EXCNGE_INST_IDX = 1;
  
  private final static int INST_TYPE_IDX = 2;
  
  private final static int NAME_IDX = 3;
  
  private final static int SERIES_IDX = 5;
  
  private final static int CONTRACT_EXPIRATION_IDX = 15;
  
  private final static int FUTURES_SIZE = 18;
  
  private final static int CASH_SIZE = 12;
  
  @Autowired
  @Qualifier("rqRsUiLog")
  private RqRsUiLog rqRsUiLog; 
  
  public Map<String, SymbolData> parseData(final String data) {
    final String[] lines = data.split("\\n");
    final Stream<String> linesStream = Arrays.stream(lines);
    final Map<String, List<SymbolData>> symbolsDataGrouped = linesStream
    .map(l->l.split("\\|"))
    .filter(this::filterOnFields)
    .map(this::getSymbolData)
    .collect(Collectors.groupingBy(sd-> sd.getKey()));
    
    final Map<String, SymbolData> symbolsData = symbolsDataGrouped.values().stream()
    .map(this::getSymbolDataFromList)
    .map(this::dumpSymbolData)
    .collect(Collectors.toMap(
       sd->sd.getKey(), Function.identity()));
    
    return symbolsData;
  }

  private SymbolData getSymbolDataFromList(final List<SymbolData> sysmDates) {
    if (sysmDates.size() == 1) {
      return sysmDates.get(0);
    }
    if (sysmDates.get(0).isCashSegment()) {
      //todo: report error/warning
      return sysmDates.get(0);
    }
    sysmDates.sort((sd1, sd2) -> sd1.expDate.compareTo(sd2.expDate));
    return sysmDates.get(sysmDates.size()-1);
  }
  
  private boolean filterOnFields(final String[] fields) {
    final String segment = fields[SEGMENT_IDX];
//    rqRsUiLog.writeToFile(segment + "\n");
    if (segment.equals(CASH_SEGMENT)) {
      final String series = fields[SERIES_IDX];
      if (!series.equals("EQ")) {
        return false;
      }
      return true;
    } else {
      //Futures, NSEFO 
      return true;
    }
  }
  
  private boolean isContractExpDateCorrect(final String contrExpDate) {
    //2019-10-31T14:30:00
    //2011-12-03T10:15:30
    final LocalDateTime expDate = LocalDateTime.parse(contrExpDate, 
        DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    final int expMonth = expDate.getMonthValue();
    final int curMonth = LocalDateTime.now().getMonthValue();
    
    return expMonth == curMonth;
  }
  
  public SymbolData getSymbolData(final String[] fields) {
    SymbolData symbolData = new SymbolData();
    symbolData.name = fields[NAME_IDX];
    symbolData.exchangeSegment = fields[SEGMENT_IDX];
    symbolData.exchangeInstrumentID = Integer.parseInt(fields[EXCNGE_INST_IDX]);
    symbolData.instrumentType = Integer.parseInt(fields[INST_TYPE_IDX]);
    if (symbolData.exchangeSegment.equals(FUTURES_SEGMENT)) {
      final String expDate = fields[CONTRACT_EXPIRATION_IDX];
      symbolData.expDate = LocalDateTime.parse(expDate, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }
 //   rqRsUiLog.writeToFile(symbolData.exchangeSegment + "\n");
    return symbolData;
  }
  
  public SymbolData dumpSymbolData(final SymbolData symbolData) {
    rqRsUiLog.writeToFile(symbolData.getDumpString());
    return symbolData;
  }
  
  public static class SymbolData {
    public String exchangeSegment;
    public String name;
    public int exchangeInstrumentID;
    public LocalDateTime expDate = LocalDateTime.now(); 
    //productType
    /*
    CO
    CNC
    MIS
    NRML
    */
    public String productType; //?? 
    
    public int instrumentType;
    
    public boolean isCashSegment() {
      return exchangeSegment.equals(CASH_SEGMENT);
    }
    
    String getDumpString() {
      final String expDateStr = isCashSegment()? "": expDate.toString();
      return String.join("|", exchangeSegment, name, 
          Integer.toString(exchangeInstrumentID), expDateStr) + "\n";
    }
    
    public String getKey() {
      return String.join("|", exchangeSegment, name);
    }
    
  }
  
}
