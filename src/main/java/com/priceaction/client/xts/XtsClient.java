package com.priceaction.client.xts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.priceaction.app.AppConfig;
import com.priceaction.client.xts.MarketSymboleParser.SymbolData;
import com.priceaction.model.OrderPlacementAction;
import com.priceaction.model.xts.LoginRq;
import com.priceaction.model.xts.LoginRs;
import com.priceaction.model.xts.ModifyOrderRq;
import com.priceaction.model.xts.ModifyOrderRs;
import com.priceaction.model.xts.OrderBookRs;
import com.priceaction.model.xts.PlaceOrderRq;
import com.priceaction.model.xts.PlaceOrderRs;
import com.priceaction.model.xts.QuoteRq;
import com.priceaction.model.xts.QuoteRs;
import com.priceaction.model.xts.SubscriptionRq;
import com.priceaction.model.xts.SubscriptionRs;
import com.priceaction.model.xts.SymbolsMarketDataRq;
import com.priceaction.model.xts.SymbolsMarketDataRs;
import com.priceaction.model.xts.UnsubscriptionRq;
import com.priceaction.model.xts.UnsubscriptionRs;
import com.priceaction.service.UiLog.RqRsUiLog;

@Service(value = "xtsClient")
public class XtsClient {
  
  private static final String AUTHORIZATION_HEADER = "Authorization";
  
  @Autowired
  @Qualifier("arrowRestTemplete")
  private RestTemplate restTemplate;
  
  @Autowired
  private AppConfig appSettings;
  
  @Autowired
  @Qualifier("rqRsUiLog")
  private RqRsUiLog rqRsUiLog; 
  
  @Autowired
  private ObjectMapper objectMapper;
  
  @Autowired
  private MarketSymboleParser marketSymboleParser;
  
  @Autowired
  @Qualifier("xtsRequestProvider")
  private XtsRequestProvider xtsRequestProvider;
  
  public LoginRs doUserInteractiveLogin() {       
    final LoginRq loginRq = new LoginRq();
    loginRq.userID = appSettings.getXtsInteractiveUserId();
    loginRq.password = appSettings.getXtsInteractivePassword();
    loginRq.publicKey = appSettings.getXtsInteractivePublicKey();
    loginRq.source = appSettings.getXtsInteractiveSource();
    
    HttpEntity<LoginRq> request = new HttpEntity<>(loginRq);
    ResponseEntity<LoginRs> response = restTemplate.exchange(appSettings.getXtsInteractiveLoginUrl(), 
          HttpMethod.POST, request, LoginRs.class);
    if (response.getStatusCode().equals(HttpStatus.OK)) {
      return response.getBody();
    }
    throw new RuntimeException("Interactive Login Failed."); 
  }
  
  
  private HttpHeaders getInteractiveAauth() {
    HttpHeaders headers = new HttpHeaders();
    headers.add("Content-Type", "application/json");
    headers.add(AUTHORIZATION_HEADER, appSettings.getXtsInteractiveSeaaionToken());
    return headers;
  }
  
  private HttpHeaders getMarketAauth() {
    HttpHeaders headers = new HttpHeaders();
    headers.add("Content-Type", "application/json");
    headers.add(AUTHORIZATION_HEADER, appSettings.getXtsMarketSeaaionToken());
    return headers;
  }

  /*
  public QuoteRs getQuote(final QuoteRq quoteRq) {
    final HttpHeaders marketAauth = getMarketAauth();
    HttpEntity<QuoteRq> request = new HttpEntity<>(quoteRq, marketAauth);
    ResponseEntity<QuoteRs> response = restTemplate.exchange(appSettings.getXtsMarketQuoteUrl(), 
        HttpMethod.POST, request, QuoteRs.class);
    if (response.getStatusCode().equals(HttpStatus.OK)) {
      return response.getBody();
    }
    throw new RuntimeException("Market Quote Failed.");
  }
  */
  
  /*
  public PlaceOrderRs placeOrder(final PlaceOrderRq placeOrderRq) {
    final HttpHeaders interactiveAauth = getInteractiveAauth();
    HttpEntity<PlaceOrderRq> request = new HttpEntity<>(placeOrderRq, interactiveAauth);
    ResponseEntity<PlaceOrderRs> response = restTemplate.exchange(appSettings.getXtsMarketQuoteUrl(), 
        HttpMethod.POST, request, PlaceOrderRs.class);
    if (response.getStatusCode().equals(HttpStatus.OK)) {
      return response.getBody();
    }
    throw new RuntimeException("Place order Failed.");
    
  }
  */
  
  private static <Rq, Rs> Rs doAuthReqquest(final Rq rq, final String url, final HttpHeaders auth, 
      final String errMsg, final RestTemplate restTemplate, Class<Rs> cls, HttpMethod httpMethod) {
    HttpEntity<Rq> request = new HttpEntity<>(rq, auth);
    ResponseEntity<Rs> response = restTemplate.exchange(url, 
        httpMethod, request, cls);
    if (response.getStatusCode().equals(HttpStatus.OK)) {
      return response.getBody();
    }
    
    System.out.println(response.toString());
    throw new RuntimeException(errMsg);
  }
  
  private static <Rs> Rs doAuthReqquestGet(final String url, final HttpHeaders auth, 
      final String errMsg, final RestTemplate restTemplate, Class<Rs> cls) {
    HttpEntity<?> request = new HttpEntity<>(null, auth);
    ResponseEntity<Rs> response = restTemplate.exchange(url, 
        HttpMethod.GET, request, cls);
    if (response.getStatusCode().equals(HttpStatus.OK)) {
      return response.getBody();
    }
    
    System.out.println(response.toString());
    throw new RuntimeException(errMsg);
  }
  
  
  
  private static <Rq, Rs> void doAuthReqquests(final List<Rq> rqs, final String url, final HttpHeaders auth, 
      final String errMsg, final RestTemplate restTemplate, Class<Rs> cls, HttpMethod httpMethod) {
    rqs.forEach(rq -> {
      doAuthReqquest(rq, url, auth, errMsg, restTemplate, cls, httpMethod);
    });
  }
  
  private static <Rq, Rs> void doAuthPostReqquests(final List<Rq> rqs, final String url, final HttpHeaders auth, 
      final String errMsg, final RestTemplate restTemplate, Class<Rs> cls, HttpMethod httpMethod) {
    doAuthReqquests(rqs, url, auth, errMsg, restTemplate, cls, HttpMethod.POST);
  }
  
  private static <Rq, Rs> void doAuthPutReqquests(final List<Rq> rqs, final String url, final HttpHeaders auth, 
      final String errMsg, final RestTemplate restTemplate, Class<Rs> cls, HttpMethod httpMethod) {
    doAuthReqquests(rqs, url, auth, errMsg, restTemplate, cls, HttpMethod.POST);
  }
  
  private static <Rq, Rs> Rs doAuthPostReqquest(final Rq rq, final String url, final HttpHeaders auth, 
      final String errMsg, final RestTemplate restTemplate, Class<Rs> cls) {
    return doAuthReqquest(rq, url, auth, errMsg, restTemplate, cls, HttpMethod.PUT);
  }
  
  
  public PlaceOrderRs placeOrder(final PlaceOrderRq placeOrderRq) {
    return doAuthPostReqquest(placeOrderRq, 
        appSettings.getXtsInteractiveOrderUrl(), getInteractiveAauth(), 
        "Place order Failed.", restTemplate, PlaceOrderRs.class);
    
//    String s = doAuthPostReqquest(placeOrderRq, 
//        appSettings.getXtsInteractiveOrderUrl(), getInteractiveAauth(), 
//        "Place order Failed.", restTemplate, String.class);
//    System.out.println(s);
//    return null;
  }
  
  public ModifyOrderRs modifyOrder(final ModifyOrderRq modifyOrderRq) {
    return doAuthPostReqquest(modifyOrderRq, 
        appSettings.getXtsInteractiveOrderUrl(), getInteractiveAauth(), 
        "Madify order Failed.", restTemplate, ModifyOrderRs.class);
  }
  
  
  //http://192.168.1.165:3000/interactive/orders?appOrderId=11111&orderUniqueIdentifier=648468731
  public String cancelOrder(final String appOrderId, final String orderUniqueIdentifier) {
    final String cancelUrl = appSettings.getXtsInteractiveOrderUrl() + "?" 
  + "appOrderId=" + appOrderId + "orderUniqueIdentifier=" + orderUniqueIdentifier;
    return doAuthReqquest("", cancelUrl, getInteractiveAauth(), "Cancel order failed.",
        restTemplate, String.class, HttpMethod.DELETE);
  }
  
  public QuoteRs getQuote(final QuoteRq quoteRq) {
    try {
      QuoteRs rs =doAuthPostReqquest(quoteRq, appSettings.getXtsMarketQuoteUrl(), getMarketAauth(), 
          "Market Quote Failed.", restTemplate, QuoteRs.class);
      rqRsUiLog.dumpQuoteRs(rs);
    return rs;
        
    } catch(Throwable t) {
      t.printStackTrace();
      return null;
    }
  }
  
  public QuoteRs getQuote() {
    final QuoteRq quoteRq = xtsRequestProvider.getQuoteRqMultiInst();
    return getQuote(quoteRq);
  }
  
  
  public SubscriptionRs getSubscription(final SubscriptionRq subscriptionRq) {
    try {
      SubscriptionRs rs =doAuthPostReqquest(subscriptionRq, appSettings.getXtsMarketSubscriptionUrl(), getMarketAauth(), 
          "Market Quote Failed.", restTemplate, SubscriptionRs.class);
      rqRsUiLog.writeToFile(objectMapper.writeValueAsString(rs));
    return rs;
        
    } catch(Throwable t) {
      t.printStackTrace();
      return null;
    }
  }
  
  public SubscriptionRs getSubscription() {
    final SubscriptionRq subscriptionRq = xtsRequestProvider.getSubscriptionRqMultiInst();
    return getSubscription(subscriptionRq);
  }
  
  
  /*
  public String getUnsubscription(final SubscriptionRq unsubscriptionRq) {
    try {
      String rs =doAuthReqquest(unsubscriptionRq, appSettings.getXtsMarketSubscriptionUrl(), getMarketAauth(), 
          "Market Quote Failed.", restTemplate, String.class, HttpMethod.PUT);
      rqRsUiLog.writeToFile(rs);
    return rs;
        
    } catch(Throwable t) {
      t.printStackTrace();
      return null;
    }
  }
  
  public String getUnsubscription() {
 //   final UnsubscriptionRq unsubscriptionRq = xtsRequestProvider.getUnsubscriptionRqMultiInst();
    final SubscriptionRq subscriptionRq = xtsRequestProvider.getSubscriptionRqMultiInst();
    
    return getUnsubscription(subscriptionRq);
  }

  */
  
 
  
  public UnsubscriptionRs getUnsubscription(final UnsubscriptionRq unsubscriptionRq) {
    try {
      UnsubscriptionRs rs =doAuthReqquest(unsubscriptionRq, appSettings.getXtsMarketSubscriptionUrl(), getMarketAauth(), 
          "Market Quote Failed.", restTemplate, UnsubscriptionRs.class, HttpMethod.PUT);
      rqRsUiLog.writeToFile(objectMapper.writeValueAsString(rs));
    return rs;
        
    } catch(Throwable t) {
      t.printStackTrace();
      return null;
    }
  }
  
  public UnsubscriptionRs getUnsubscription() {
    final UnsubscriptionRq unsubscriptionRq = xtsRequestProvider.getUnsubscriptionRqMultiInst();
    return getUnsubscription(unsubscriptionRq);
  }

 
  public OrderBookRs getOrderBook() {
    try {
      OrderBookRs rs =doAuthReqquestGet(appSettings.getXtsInteractiveOrderUrl(), getInteractiveAauth(), 
          "Market Quote Failed.", restTemplate, OrderBookRs.class);
      rqRsUiLog.writeToFile(objectMapper.writeValueAsString(rs));
    //  System.out.println(strRS);
      //OrderBookRs rs = new OrderBookRs();
    return rs;
        
    } catch(Throwable t) {
      t.printStackTrace();
      return null;
    }
  }
  
  
  /*
  public SubscriptionRs getUnsubscription(final UnsubscriptionRq unsubscriptionRq) {
    try {
      SubscriptionRs rs =doAuthReqquest(unsubscriptionRq, appSettings.getXtsMarketSubscriptionUrl(), getMarketAauth(), 
          "Market Quote Failed.", restTemplate, SubscriptionRs.class, HttpMethod.PUT);
      rqRsUiLog.writeToFile(objectMapper.writeValueAsString(rs));
    return rs;
        
    } catch(Throwable t) {
      t.printStackTrace();
      return null;
    }
  }
  
  public SubscriptionRs getUnsubscription() {
    final UnsubscriptionRq unsubscriptionRq = xtsRequestProvider.getUnsubscriptionRqMultiInst();
    return getUnsubscription(unsubscriptionRq);
  }
  
  */
  
  /*
  public List<QuoteRs> getQuotes() {
    final List<QuoteRq> quoteRqs = xtsRequestProvider.getQuoteRqListWithListener();
    
    rqRsUiLog.info("Total Quote Rqs found: " + quoteRqs.size());
    
    final List<QuoteRs> quotes = new ArrayList<QuoteRs>();
    quoteRqs.forEach(qrq -> {
      final QuoteRs quoteRs = getQuote(qrq);
      quotes.add(quoteRs);
    });
    
    rqRsUiLog.info("Total Quote Rss reciever: " + quotes.size());
    return quotes;
  }
  */
  
  public LoginRs doUserMarktLogin() {       
    final LoginRq loginRq = new LoginRq();
    loginRq.userID = appSettings.getXtsMarketUsedId();
    loginRq.password = appSettings.getXtsMarketPassword();
    loginRq.publicKey = appSettings.getXtsMarketPublicKey();
    loginRq.source = appSettings.getXtsMarketSource();
    HttpEntity<LoginRq> request = new HttpEntity<>(loginRq);
    ResponseEntity<LoginRs> response = restTemplate.exchange(appSettings.getXtsMarketLoginUrl(), 
          HttpMethod.POST, request, LoginRs.class);
    
    if (response.getStatusCode().equals(HttpStatus.OK)) {
      return response.getBody();
    }
    throw new RuntimeException("Market Login Failed.");
  }

  public void doUserInteractiveLogout() {
    restTemplate.delete(appSettings.getXtsInteractiveLoginUrl());
  }
  
  
  public void doUserMarketLogout() {
    restTemplate.delete(appSettings.getXtsMarketLoginUrl());
  }
  
  public Map<String, SymbolData> doFetchMarketSymbols() {
    final SymbolsMarketDataRq symbolsMarketDataRq = new SymbolsMarketDataRq();
    symbolsMarketDataRq.exchangeSegmentList = Arrays.asList("NSECM", "NSEFO");
    
    HttpEntity<SymbolsMarketDataRq> request = new HttpEntity<>(symbolsMarketDataRq);
    final String marketSymbolsDataUrl = appSettings.getMarketSymbolsDataUrl();
    rqRsUiLog.info("start fetching sysmbols. URL: " + marketSymbolsDataUrl);
    try {
      rqRsUiLog.info("RQ: " + objectMapper.writeValueAsString(symbolsMarketDataRq));
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    ResponseEntity<SymbolsMarketDataRs> response = restTemplate.exchange(marketSymbolsDataUrl, 
          HttpMethod.POST, request, SymbolsMarketDataRs.class);
    
    if (!response.getStatusCode().equals(HttpStatus.OK)) {
      throw new RuntimeException("Symbols Data Fetch Failed.");
    }
    final SymbolsMarketDataRs symbolsMarketDataRs = response.getBody();
    
    final Map<String, SymbolData> symbolsData = marketSymboleParser
        .parseData(symbolsMarketDataRs.result);
    return symbolsData;
  }

  public void executeOrderPlacementData(final OrderPlacementAction opd) {
//    doAuthPostPoReqquests(opd.ordersToPlace, 
//        appSettings.getXtsInteractiveUrl(), getInteractiveAauth(), 
//        "Place order Failed.", restTemplate, PlaceOrderRs.class);
//    
  }
  

}
