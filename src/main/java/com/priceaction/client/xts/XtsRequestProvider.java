package com.priceaction.client.xts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.StreamSerializer;
import com.priceaction.app.AppConfig;
import com.priceaction.client.xts.MarketSymboleParser.SymbolData;
import com.priceaction.model.ExcelData;
import com.priceaction.model.xts.QuoteRq;
import com.priceaction.model.xts.SubscriptionRq;
import com.priceaction.model.xts.SubscriptionRs;
import com.priceaction.model.xts.UnsubscriptionRq;
import com.priceaction.model.xts.Instrument;
import com.priceaction.model.xts.MarketChange;
import com.priceaction.model.xts.ModifyOrderRq;
import com.priceaction.model.xts.OrderBookRs.OrderBook;
import com.priceaction.model.xts.PlaceOrderRq;
import com.priceaction.model.xts.Quote;
import com.priceaction.service.PriceActionService;
import com.priceaction.service.UiLog.RqRsUiLog;
import com.priceaction.testclnt.TransactionType;

@Service(value = "xtsRequestProvider")
public class XtsRequestProvider {
  
  @Autowired
  private AppConfig appSettings;
  
  @Autowired
  @Qualifier("rqRsUiLog")
  private RqRsUiLog rqRsUiLog; 
  
  @Autowired
  private ObjectMapper objectMapper;
  
  @Autowired
  private MarketSymboleParser marketSymboleParser;
  
  @Autowired
  private PriceActionService priceActionService;
  
  private ServerSocket quoteServerSocket;
  
  /*
 
{"NSECM": 1, "NSEFO": 2, "NSECD": 3,"BSECM": 11, "BSEFO": 12} 
It is segment, which represents cash, derivative, commodity or currency market. 
   */

  public enum Segment {
    NSECM("NSECM", 1),
    NSEFO("NSEFO", 2),
    NSECD("NSECD", 3),
    BSECM("BSECM", 11),
    BSEFO("BSEFO", 12);
    
    public String name;
    public int id;
    
    Segment(final String nm, final int id) {
      name = nm;
      this.id = id;
    }
    public static Segment getByNamy(final String name) {
     final Optional<Segment> segment = Arrays.stream(Segment.values())
     .filter(s -> s.name.equalsIgnoreCase(name))
     .findFirst();
     return segment.get();
    }
  }
  
  public enum OrderSide {
    BUY("BUY"), SELL("SELL");
    OrderSide(final String nm) {
      name = nm;
    }
    public String name;
  }

  public enum PositionSqureOffMode {
    DayWise, NetWise;
  }

  public enum PositionSquareOffQuantityType {
    Percentage, ExactQty;
  }

  public enum DayOrNet {
    DAY, NET;
  }

  public enum ProductType {
    CO, CNC, MIS, NRML;
  }

  public enum OrderType {
    StopLimit, StopMarket, Limit, Market;
  }
  
  public List<QuoteRq> getQuoteRqListWithListener() {
    final int port = initQuoteServerSocket();
    final List<QuoteRq> quoteRqList = getQuoteRqList(Integer.toString(port));
    readQuotesAsynch();
    return quoteRqList;
  }
  
  public List<QuoteRq> getQuoteRqList(final String port) {
    final List<ExcelData> excelData = priceActionService.getExDataImport();
    final Map<String, SymbolData> symbolsData = appSettings.getSymbolsData();
    final List<QuoteRq> quoteRequests = excelData.stream()
    .map(ed -> getQuoteRq(ed, symbolsData, port))
    .filter(Objects::nonNull)
    .collect(Collectors.toList());
    
    return quoteRequests; 
  }
  
  private QuoteRq getQuoteRq(final ExcelData excelDataEntry, final Map<String, SymbolData> symbolsData, 
      final String port) {
    
    final SymbolData symData = symbolsData.get(excelDataEntry.getKey());
    if (symData == null) {
   //   rqRsUiLog.info("getQuoteRq: no data found for symbol: " + excelDataEntry.symbol);    
      return null;
    }
    
    excelDataEntry.exchangeInstrumentID = symData.exchangeInstrumentID;
    
    final QuoteRq quoteRq = new QuoteRq();
    quoteRq.clientID = appSettings.getXtsInteractiveUserId();
    quoteRq.userID = appSettings.getXtsInteractiveUserId();
    quoteRq.marketDataPort = 1502; //port;
    quoteRq.publishFormat = "JSON";
    quoteRq.source = "WebApi";
    
    
    quoteRq.instruments = new ArrayList<Instrument>();
    Instrument instrument = new Instrument();
    instrument.setExchangeInstrumentID(symData.exchangeInstrumentID);
    instrument.setExchangeSegment(Segment.getByNamy(symData.exchangeSegment).id);
    quoteRq.instruments.add(instrument);
    return quoteRq;
  }
  
  public QuoteRq getQuoteRqMultiInst() {
    final List<ExcelData> excelData = priceActionService.getExDataImport();
    final Map<String, SymbolData> symbolsData = appSettings.getSymbolsData();
    return getQuoteRqMultiInst(excelData, symbolsData);    
  }
  
  private QuoteRq getQuoteRqMultiInst(final List<ExcelData> excelDataEntries, 
      final Map<String, SymbolData> symbolsData) {
    final QuoteRq quoteRq = new QuoteRq();
    quoteRq.clientID = appSettings.getXtsInteractiveUserId();
    quoteRq.userID = appSettings.getXtsInteractiveUserId();
    quoteRq.marketDataPort = 1502; 
    quoteRq.publishFormat = "JSON";
    quoteRq.source = "WebApi";
    final List<Instrument> instruments = getInstruments(excelDataEntries, symbolsData);
    quoteRq.instruments = instruments;
    return quoteRq; 
  }
  
  
  public UnsubscriptionRq getUnsubscriptionRqMultiInst() {
    final List<ExcelData> excelData = priceActionService.getExDataImport();
    final Map<String, SymbolData> symbolsData = appSettings.getSymbolsData();
    return getUnsubscriptionRqMultiInst(excelData, symbolsData);    
  }
  
  private UnsubscriptionRq getUnsubscriptionRqMultiInst(final List<ExcelData> excelDataEntries, 
      final Map<String, SymbolData> symbolsData) {
    final UnsubscriptionRq unsubscriptionRq = new UnsubscriptionRq();
    unsubscriptionRq.clientID = appSettings.getXtsInteractiveUserId();
    unsubscriptionRq.userID = appSettings.getXtsInteractiveUserId();
    unsubscriptionRq.marketDataPort = 1502; 
   // subscriptionRq.publishFormat = "JSON";
    unsubscriptionRq.source = "WebApi";
    final List<Instrument> instruments = getInstruments(excelDataEntries, symbolsData);
    unsubscriptionRq.instruments = instruments;
    return unsubscriptionRq; 
  }
  
  
  public SubscriptionRq getSubscriptionRqMultiInst() {
    final List<ExcelData> excelData = priceActionService.getExDataImport();
    final Map<String, SymbolData> symbolsData = appSettings.getSymbolsData();
    return getSubscriptionRqMultiInst(excelData, symbolsData);    
  }
  
  private SubscriptionRq getSubscriptionRqMultiInst(final List<ExcelData> excelDataEntries, 
      final Map<String, SymbolData> symbolsData) {
    final SubscriptionRq subscriptionRq = new SubscriptionRq();
    subscriptionRq.clientID = appSettings.getXtsInteractiveUserId();
    subscriptionRq.userID = appSettings.getXtsInteractiveUserId();
    subscriptionRq.marketDataPort = 1502; 
    subscriptionRq.publishFormat = "JSON";
    subscriptionRq.source = "WebApi";
    final List<Instrument> instruments = getInstruments(excelDataEntries, symbolsData);
    subscriptionRq.instruments = instruments;
    return subscriptionRq; 
  }
  
  public List<Quote> getSubscriptionQuotes(final SubscriptionRs subscriptionRs) {
    final List<Quote> quotes = subscriptionRs.result.listQuotes.stream()
    .map(s -> {
      try {
        return objectMapper.readValue(s, Quote.class);
      } catch (IOException e) {
        e.printStackTrace();
        return null;
      }
    })
    .filter(Objects::nonNull)
    .collect(Collectors.toList());
    return quotes;
  }
  
  

  private List<Instrument> getInstruments(final List<ExcelData> excelDataEntries,
      final Map<String, SymbolData> symbolsData) {
    final List<Instrument> instruments = excelDataEntries.stream()
      .map(de -> getInstrument(symbolsData, de))
      .filter(Objects::nonNull)
      .collect(Collectors.toList());
    return instruments;
  }

  private Instrument getInstrument(final Map<String, SymbolData> symbolsData, final ExcelData de) {
    final SymbolData symData = symbolsData.get(de.getKey());
    if (symData == null) {
 //     rqRsUiLog.info("getQuoteRq: no data found for symbol: " + de.symbol);    
      return null;
    }
    Instrument instrument = new Instrument();
    instrument.setExchangeInstrumentID(symData.exchangeInstrumentID);
    instrument.setExchangeSegment(Segment.getByNamy(symData.exchangeSegment).id);
    return instrument;
  }
  
  
  private int initQuoteServerSocket() {
    try {
      quoteServerSocket = new ServerSocket(0);
      return quoteServerSocket.getLocalPort();
    } catch (IOException e) {
      e.printStackTrace();
      return -1;
    }
  }
  
  
  private void readQuotes() {
    try (
        Socket clientSocket = quoteServerSocket.accept();
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));) {
      String inputLine;

      // Initiate conversation with client

      while ((inputLine = in.readLine()) != null) {
        System.out.println(inputLine);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    try {
      quoteServerSocket.close();
    } catch (IOException e) {
    }
  }
  
  public void readQuotesAsynch() {
    CompletableFuture
    .runAsync(() -> {
      readQuotes();
    })
    //.exceptionally(t -> {})
    ;
  }

  
  public PlaceOrderRq placeEntryOrder(final ExcelData excelEntry) {
    final OrderSide side = excelEntry.transactionType == TransactionType.BUY?
        OrderSide.BUY: OrderSide.SELL;
    Integer totalQuantity = getTotalQuantity(excelEntry);
    return placeLimitOrder(side, totalQuantity, excelEntry.exchangeInstrumentID, 
        excelEntry.exchangeSegment, excelEntry.entryPrice.doubleValue());
  }

  public ModifyOrderRq modifyEntryOrder(final ExcelData excelEntry, final Integer newQtty, final Double newPrice) {
    final OrderSide side = excelEntry.transactionType == TransactionType.BUY?
        OrderSide.BUY: OrderSide.SELL;
    //Integer totalQuantity = getTotalQuantity(excelEntry);
    return modifyLimitOrder(excelEntry.entryAppOrderID, newQtty, newPrice );
  }

  
  
  public PlaceOrderRq placeExitOrder(final ExcelData excelEntry, 
      final Integer quantity, final Double price) {
    final OrderSide side = excelEntry.transactionType == TransactionType.BUY?
        OrderSide.SELL: OrderSide.BUY;
    return placeLimitOrder(side, quantity, excelEntry.exchangeInstrumentID, 
        excelEntry.exchangeSegment, price);
  }
  
  /*
  public PlaceOrderRq modifyExitOrder(final ExcelData excelEntry, 
      final Integer quantity, final Double price) {
    final OrderSide side = excelEntry.transactionType == TransactionType.BUY?
        OrderSide.SELL: OrderSide.BUY;
    return placeLimitOrder(side, quantity, excelEntry.exchangeInstrumentID, 
        excelEntry.exchangeSegment, price);
  }
  */
  
  public List<PlaceOrderRq> placeExitOrders(final ExcelData excelEntry, 
      final List<Integer> quantities) {
    final List<PlaceOrderRq> orders = IntStream.range(0,  quantities.size())
        .mapToObj(i -> placeExitOrder(excelEntry, quantities.get(i), 
            excelEntry.getTargetDoublePrice(i)))
        .collect(Collectors.toList());
        return orders;
  }

  public List<ModifyOrderRq> modifyExitOrders(final ExcelData excelEntry, 
      final Integer filledQtties) {
    final List<Integer> newQuantities = getNewQuantities(excelEntry, filledQtties);
    return modifyExitOrders(excelEntry, newQuantities);
  }
  
  public List<ModifyOrderRq> modifyExitOrders(final ExcelData excelEntry, 
      final List<Integer> newQtties) {
    final List<ModifyOrderRq> orders = IntStream.range(0,  newQtties.size())
        .mapToObj(i -> modifyLimitOrder(excelEntry.exitAppOrderIDs.get(i), 
            newQtties.get(i),  excelEntry.getTargetDoublePrice(i)))
        .collect(Collectors.toList());
        return orders;
  }
  
  
  public PlaceOrderRq placeExitOrder(final ExcelData excelEntry, final int targetNum) {
    final OrderSide side = excelEntry.transactionType == TransactionType.BUY?
        OrderSide.SELL: OrderSide.BUY;
    final Integer quantity = excelEntry.getTargetQauntity(targetNum);
    final Double price =  excelEntry.getTargetDoublePrice(targetNum);
    return placeLimitOrder(side, quantity, excelEntry.exchangeInstrumentID, 
        excelEntry.exchangeSegment, price);
  }
  
  public List<PlaceOrderRq> placeExitOrders(final ExcelData excelEntry) {
    final Integer nomOfTargets = priceActionService.getAppSettingsModel().getNomOfTargets();
    final List<PlaceOrderRq> orders = IntStream.range(0,  nomOfTargets)
    .mapToObj(i -> placeExitOrder(excelEntry, i))
    .collect(Collectors.toList());
    return orders;
  }

  public PlaceOrderRq placeStopLossOrder(final ExcelData excelEntry, final Integer quantity) {
    final OrderSide side = excelEntry.transactionType == TransactionType.BUY?
        OrderSide.SELL: OrderSide.BUY;
    final Double price = excelEntry.stoplossPrice.doubleValue();
    return placeStopLossOrder(side, quantity, excelEntry.exchangeInstrumentID, 
        excelEntry.exchangeSegment, price);
  }

  
  public ModifyOrderRq modifyStopLossOrder(final ExcelData excelEntry, final Integer quantity) {
    final OrderSide side = excelEntry.transactionType == TransactionType.BUY?
        OrderSide.SELL: OrderSide.BUY;
    final Double price = excelEntry.stoplossPrice.doubleValue();
    return modifyStopLossOrder(excelEntry.stoplossAppOrderID, side, quantity, price);
  }

  
  public Integer getTotalQuantity(final ExcelData excelEntry) {
    final Integer nomOfTargets = priceActionService.getAppSettingsModel().getNomOfTargets();
    if (nomOfTargets == 1) {
      return excelEntry.qty1;
    }
    if (nomOfTargets == 2) {
      return excelEntry.qty1 + excelEntry.qty2;
    }
    if (nomOfTargets == 3) {
      return excelEntry.qty1 + excelEntry.qty2 + excelEntry.qty3;
    }
    if (nomOfTargets == 4) {
      return excelEntry.qty1 + excelEntry.qty2 + excelEntry.qty3  + excelEntry.qty4;
    }
    return null; // error , will be NPE
  }
  
  public List<Integer> getNewQuantities(final ExcelData excelEntry, final Integer filledQuantity) {
    final Integer totalQuantity = getTotalQuantity(excelEntry);
    if (totalQuantity < filledQuantity) {
      rqRsUiLog.error("INCORRECT PARTIALLY FILLED NUMBER");
      throw new RuntimeException("INCORRECT PARTIALLY FILLED NUMBER");
    }
    final Integer nomOfTargets = priceActionService.getAppSettingsModel().getNomOfTargets();
    final Integer leftQtty = totalQuantity - filledQuantity;
    final List<Integer> newQtties = new ArrayList<>();
    for(int i=0; i<nomOfTargets; i++) {
      final Double pct = ((double)excelEntry.getTargetQauntity(i)) / totalQuantity;
      Integer qtty = (int) Math.floor(leftQtty * pct);
      newQtties.add(qtty);
    }
    return newQtties;
  }
  
  
  /**
  aa: by the way, there is orderType enum: StopLimit, StopMarket, Limit, Market
  how should we match this order type to created from excel entity orders: entry, stoploss, targets?

  This is simple math, let me tell share it 
  Limit order need only one price, which is limitprice

  Market order does not need any price at all, so we can keep it 0, it gets filled immediately

  Stoplimit order, needs 2 prices, one is trigger price and one is limit price. 
  Usually price in excel is to be considered as trigger price if stoplimit situation and limit price is 0.5% difference from limit price, which is

  for buy order limitprice = triggerprice+(triggerprice*0.5%)

   for sell order limitprice =  triggerprice-(triggerprice*0.5%)

  Stop market needs just triggerprice which will be in excel.


  Any stoploss order is placed has to use stop market order only

  Targets usually are limit order

  Entries are limit order. 
  
  **/
  
  /**
   
Aliaksandr, 3:38 PM
productType: CO, CNC, MIS, NRML. WHich one should we use? 
Here is integer InstrumentType in symbols dada. Is this connected?

Pradeep, 3:41 PM
MIS is used for now, 
there is a product called BO too, which is not added by XTS, 
they are under testing phase, 
so if that is done we may have to edit our tool later to add BO order type too         
   */
public PlaceOrderRq placeLimitOrder(final OrderSide side, final Integer quantity, 
    final Integer exchangeInstrumentID, final String exchangeSegment, final Double price) {
  
  final PlaceOrderRq placeOrderRq = placeOrder(side, quantity, exchangeInstrumentID, exchangeSegment, price, 0.0);
  
  placeOrderRq.orderType = OrderType.Limit.name();
  
  return placeOrderRq;
}



public ModifyOrderRq modifyLimitOrder(final String appOrderID, final Integer newQtty, final Double newPrice) {
  final ModifyOrderRq modifyOrderRq = modifyOrder(appOrderID, newQtty,  newPrice, 0.0);
  modifyOrderRq.orderType = OrderType.Limit.name();
  return modifyOrderRq;
}



public PlaceOrderRq placeStopLossOrder(final OrderSide side, final Integer quantity, 
    final Integer exchangeInstrumentID, final String exchangeSegment, final Double stopLossPrice) {
  //for buy order limitprice = triggerprice+(triggerprice*0.5%)
  //for sell order limitprice =  triggerprice-(triggerprice*0.5%)  
  final Double limitprice = (OrderSide.BUY == side)? 
      stopLossPrice + stopLossPrice * 0.005:
        stopLossPrice - stopLossPrice * 0.005;  
  
  final PlaceOrderRq placeOrderRq = placeOrder(side, quantity, exchangeInstrumentID, exchangeSegment, limitprice, stopLossPrice);
  placeOrderRq.orderType = OrderType.StopMarket.name();  
  return placeOrderRq;
}


public ModifyOrderRq modifyStopLossOrder(final String appOrderID, final OrderSide side, 
    final Integer newQtty, final Double newStopLossPrice) {
  //for buy order limitprice = triggerprice+(triggerprice*0.5%)
  //for sell order limitprice =  triggerprice-(triggerprice*0.5%)  
  final Double limitprice = (OrderSide.BUY == side)? 
      newStopLossPrice + newStopLossPrice * 0.005:
        newStopLossPrice - newStopLossPrice * 0.005;  
  final ModifyOrderRq modifyOrderRq = modifyOrder(appOrderID, newQtty, limitprice, newStopLossPrice);
  modifyOrderRq.orderType = OrderType.StopMarket.name();  
  return modifyOrderRq;
}

public PlaceOrderRq placeMarketOrder(final OrderSide side, final Integer quantity, 
    final Integer exchangeInstrumentID, final String exchangeSegment) {
  final PlaceOrderRq placeOrderRq = placeOrder(side, quantity, exchangeInstrumentID, exchangeSegment, 0.0, 0.0);
  placeOrderRq.orderType = OrderType.Market.name();
  return placeOrderRq;
}

public ModifyOrderRq modifyMarketOrder(final String appOrderID, final Integer quantity) {
  final ModifyOrderRq modifyOrderRq = modifyOrder(appOrderID, quantity, 0.0, 0.0);
  modifyOrderRq.orderType = OrderType.Market.name();
  return modifyOrderRq;
}


  private PlaceOrderRq placeOrder(final OrderSide side, final Integer quantity, 
      final Integer exchangeInstrumentID, final String exchangeSegment, 
      final Double limitPrice, final Double stopPrice) {
    final PlaceOrderRq placeOrderRq = new PlaceOrderRq();
    placeOrderRq.productType = ProductType.MIS.name();
    /*
     We will keep Disclosedquantity by default 0 
     */
    placeOrderRq.disclosedQuantity = 0;
    placeOrderRq.orderSide = side.name;
    placeOrderRq.orderQuantity = quantity;
    placeOrderRq.exchangeInstrumentID = exchangeInstrumentID;
    placeOrderRq.exchangeSegment = exchangeSegment;
    //Limit order need only one price, which is limitprice
    placeOrderRq.limitPrice = limitPrice;
    placeOrderRq.stopPrice = stopPrice;
    
    
    return placeOrderRq;
  }
  
  private ModifyOrderRq modifyOrder(final String appOrderID, final Integer newQtty, 
      final Double newLimitPrice, final Double stopPrice) {
    final ModifyOrderRq modifyOrderRq = new ModifyOrderRq();
    modifyOrderRq.appOrderID = appOrderID;
    modifyOrderRq.productType = ProductType.MIS.name();
    /*
     We will keep Disclosedquantity by default 0 
     */
    modifyOrderRq.modifiedDisclosedQuantity = 0;
    modifyOrderRq.modifiedOrderQuantity = newQtty;
    //Limit order need only one price, which is limitprice
    modifyOrderRq.modifiedLimitPrice = newLimitPrice;
    modifyOrderRq.modifiedStopPrice = stopPrice;
    
    
    return modifyOrderRq;
  }
  
  
  
}
