package com.priceaction.client.arrow;

import org.springframework.stereotype.Service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
//import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.priceaction.app.AppConfig;
import com.priceaction.model.arrow.ArrowRespResponseRs;
import com.priceaction.model.arrow.ArrowResponse;
import com.priceaction.model.arrow.ArrowResponseRs;
import com.priceaction.model.arrow.LoginRequestRq;
import com.priceaction.model.arrow.LoginRequestRs;
import com.priceaction.model.arrow.MWLError;
import com.priceaction.model.arrow.MarketWatchList;
import com.priceaction.model.arrow.MarketWatchListRq;
import com.priceaction.model.arrow.ResStat;
import com.priceaction.model.arrow.ResStatResp;
import com.priceaction.service.UiLog;
import com.priceaction.service.UiLog.RqRsUiLog;

@Service(value = "arrowClient")
public class ArrowClient {
  
  @Autowired
  @Qualifier("arrowRestTemplete")
  private RestTemplate restTemplate;
  
  @Autowired
  private AppConfig appSettings;
  
  @Autowired
  @Qualifier("rqRsUiLog")
  private RqRsUiLog rqRsUiLog; 
  
  @Autowired
  private ObjectMapper objectMapper;

  
  public LoginRequestRs doUserLoginRequest() {    
    final LoginRequestRq rqData = new LoginRequestRq();
    rqData.setApiKey(appSettings.getApiKey());    
    final HttpEntity<LoginRequestRq> request = new HttpEntity<>(rqData);
    final LoginRequestRs loginRequestRs = restTemplate
        .postForObject(appSettings.getUserLoginRequestUrl(), 
            request, LoginRequestRs.class);
    if (!loginRequestRs.isResponseOk()) {
      throw new RuntimeException(convertToJson(loginRequestRs));
    }
    return loginRequestRs;
  }
  
  public String doRedirectRq(final String redirectUrl, final String signature) {
    final String paramUrl = redirectUrl + "?signature=" + signature;
    final HttpEntity<String> request = new HttpEntity<>("");
    
    ResponseEntity<String> response
    = restTemplate.getForEntity(paramUrl, String.class);
    return response.getBody();
    
   // return restTemplate.postForObject(paramUrl, request, String.class);
  }
  
//  public ArrowResponse<MarketWatchList, MWLError> doGetMWList() {
  public ArrowRespResponseRs<MarketWatchList, MWLError> doGetMWList() {
    final String marketWatchUrl = appSettings.getMarketWatchNamesUrl(); //getReqUrl(appSettings.getMarketWatchNamesUrl());
    rqRsUiLog.info("RQ URL: " + marketWatchUrl);
    
    //final String body = "{\"uid\":\"" + appSettings.getClientId() + "\"}";
    
    final MarketWatchListRq marketWatchListRequest = createMarketWatchListRequest();
    
    rqRsUiLog.info("RQ body: " + convertToJson(marketWatchListRequest));
    final HttpEntity<MarketWatchListRq> request = new HttpEntity<>(marketWatchListRequest);
    /*
    final MarketWatchList marketWatchListRs = restTemplate
     .postForObject(marketWatchUrl, request, MarketWatchList.class);
    */
    
    
    
    final String res = restTemplate
        .postForObject(marketWatchUrl, request, String.class);
    rqRsUiLog.info(res);
    
    /*
    final ArrowResponse<MarketWatchList, MWLError> mwlResp = convertStringToResp(res, 
        MarketWatchList.class, MWLError.class);
    */
    ResStatResp<MarketWatchList> kk = new ResStatResp<MarketWatchList>();
    ResStatResp<MWLError> mm = new ResStatResp<MWLError>();
    final ArrowRespResponseRs<MarketWatchList, MWLError> mwlResp = convertStringToResponseResp(res, 
    //    ResStatResp<MarketWatchList>
    kk.getType(), mm.getType());

    
//    rqRsUiLog.logRespRs(mwlResp);
    
    return mwlResp;
  }
  
  
  private static <D, E> ArrowRespResponseRs<D, E> 
      convertStringToResponseResp(final String strRes, 
      final Class<? extends ResStatResp<D>> dc, final Class<? extends ResStatResp<E>> ce) {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
     final ResStatResp<D> data = objectMapper.readValue(strRes, dc);
     if (data.isSuccess()) {
        return new ArrowRespResponseRs<D, E>(data, null);
     }
     final ResStatResp<E> error = objectMapper.readValue(strRes, ce);
     return new ArrowRespResponseRs<D, E>(null, error);
   } catch (IOException e) {
     e.printStackTrace();
   }
    return null;
  }

  
   private static <D extends ResStat, E> ArrowResponse<D, E> convertStringToResp(final String strRes, 
       final Class<D> dc, final Class<E> ce) {
     ObjectMapper mapper = new ObjectMapper();
     try {
      final D data = mapper.readValue(strRes, dc);
      if (data.isSuccess()) {
         return new ArrowResponseRs<D, E>(data, null);
      }
      final E error = mapper.readValue(strRes, ce);
      return new ArrowResponseRs<D, E>(null, error);
    } catch (IOException e) {
      e.printStackTrace();
    }
     return null;
   }
  
  private String getReqUrl(final String baseUrl) {
    /*
    return baseUrl + "?request_token=" + appSettings.getRequestToken()
      + "&" + appSettings.getSessId();
      */
    return baseUrl + "?" + appSettings.getSecuritySubQuery();
  }
  
  

  private static <T> String convertToJson(final T data) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return  mapper.writeValueAsString(data);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      return "error";
    }
  }
  
  private static <T> T convertToObject(final String data, final Class<T> c) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.readValue(data, c);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }
  
  private MarketWatchListRq createMarketWatchListRequest() {
   
    /*
    final String body = 
        "{\"user_apikey\":\"" + appSettings.getApiKey() + "\"," + 
         "\"session_id\":\"" + appSettings.getSessId() + "\"," +
         "\"access_token\":\"" + appSettings.getRequestToken() + "\"," +
         "\"data\": {\"acctId\":\"" + appSettings.getClientId() + "\"}" +
        "}";
   */
    
    MarketWatchListRq rq = new MarketWatchListRq();
    rq.user_apikey = appSettings.getApiKey();
    rq.session_id = appSettings.getSessId();
    rq.access_token = appSettings.getRequestToken();
    rq.data = new MarketWatchListRq.Data();
    rq.data.acctId = appSettings.getClientId();
    return rq;
  }
 
}
